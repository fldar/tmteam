@extends('layouts.member')

@section('content')

    @if($errors->any())
        @php toast()->error('Falha ao cadastrar frequência.', 'Error'); @endphp
    @endif

    <section class="section">
        <div class="section-header">
            <h1>Nova Frequência</h1>
        </div>

        <div class="section-body mt-4">

            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <div class="card">
                        {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['teachers.frequencies.store', 'classroom' => $classroom->id]]) !!}
                        <div class="card-header">
                            <h4>
                                <i class="far fa-edit lga"></i>
                                Detalhes
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group mb-2">
                                        {!! Form::hidden('classroom_id', $classroom->id) !!}
                                        {!! Form::label('date', 'Data', ['class' => 'label-required']) !!}
                                        {!! Form::text('date', old('date'), ['class' => 'form-control js-datetimepicker']) !!}
                                        @if($errors->has('date'))
                                            <span class="text-danger">{{ $errors->first('date') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group mb-2">
                                        {!! Form::label('registration_id', 'Aluno', ['class' => 'label-required']) !!}
                                        {!! Form::select('registration_id[]', $students, old('registration_id'), ['multiple' => 'multiple', 'class' => 'form-control', 'title' => 'Selecione']) !!}
                                        @if($errors->has('registration_id'))
                                            <span class="text-danger">{{ $errors->first('registration_id') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group mb-2">
                                        {!! Form::label('remark', 'Observações', ['class' => '']) !!}
                                        {!! Form::textarea('remark', old('remark'), ['class' => 'form-control']) !!}
                                        @if($errors->has('remark'))
                                            <span class="text-danger">{{ $errors->first('remark') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <a href="{{ route('teachers.frequencies.index', ['classroom' => $classroom->id]) }}"
                               class="btn btn-danger btn-lg btn-icon mr-1"
                               title="Voltar">
                                <i class="fas fa-angle-left"></i> Voltar</a>
                            <button type="submit"
                                    class="btn btn-icon icon-left btn-success btn-lg">
                                <i class="fas fa-check"></i> Salvar
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>

@endsection
