@extends('layouts.member')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Frequencias</h1>
        </div>

        <div class="row mt-4">
            <div class="col-lg-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => ['teachers.frequencies.index', 'classroom' => $classroom->id], 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-6 col-md-8 col-sm-12 mb-1 form-group">
                                {!! Form::text('date', Request::get('date'), ['class' => 'form-control js-datepicker']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-1 form-group">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-1 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>

                            <div class="col-lg-1 col-md-1 col-sm-12 mb-1 form-group">
                                <a href="{{ route('teachers.frequencies.create', ['classroom' => $classroom->id]) }}"
                                   class="btn btn-icon btn-block btn-info"
                                   title="Adicionar frequência manual">
                                    <i class="fas fa-plus lg-icon"></i>
                                </a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-md-12 col-lg-12">
                    {!! Form::open(['method' => 'PUT',
                                    'novalidate',
                                    'role' => 'form',
                                    'class' => 'form',
                                    'route' => ['teachers.frequencies.update', 'classroom' => $classroom->id]
                                    ]) !!}
                    <div class="card">
                        <div class="card-header">
                            <b>Lista de presenças registradas</b>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped overflow mb-0">
                                <tbody>
                                <tr class="tr-a border">
                                    <th>#</th>
                                    <th>Alunos</th>
                                    <th class="text-right">Presente?</th>
                                </tr>
                                @forelse($frequencies as $key => $frequency)
                                    <tr class="tr-item border">
                                        <td class="td-item">{{ $loop->iteration }}</td>
                                        <td class="td-item"><img
                                                    class="border-100"
                                                    src="{{ $frequency->registration->student->user->avatarUrl }}"
                                                    alt="{{ $frequency->registration->student->user->nickname }}"
                                                    width="40"> {{  $frequency->registration->student->user->name }}</td>
                                        <td class="td-item text-right">
                                            <label class="mt-2 mr-2">
                                                {!! Form::checkbox("reviewed[$frequency->id]", true, true, ['class' => 'custom-switch-input', 'id' => 'reviewed', 'disabled' => (bool) $frequency->reviewed]) !!}
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="tr-item border">
                                        <td colspan="3" class="td-item text-center">
                                            Nenhum registro encontrado
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="form-group mb-0">
                                <button type="submit"
                                        class="btn btn-lg btn-success btn-icon float-right"
                                        title="Atualizar dados"><i
                                            class="fas fa-check"></i>
                                    Salvar
                                </button>
                                <a href="{{ route('teachers.dashboard.index') }}"
                                   class="btn btn-danger btn-lg btn-icon float-right mr-2"
                                   title="Voltar"><i
                                            class="fas fa-angle-left"></i>
                                    Voltar</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

@endsection
