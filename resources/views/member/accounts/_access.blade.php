<div class="row">
    <div class="col-lg-12 mt-0 mb-2">
        <div class="section-title mt-2">Acesso</div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group mb-4">
            {!! Form::label('user[password]', 'Senha', ['class' => '']) !!}
            {!! Form::password('user[password]', ['class' => 'form-control']) !!}
            @if($errors->has('user.password'))
                <span class="text-danger">{{ $errors->first('user.password') }}</span>
            @endif
        </div>
    </div>

    <div class="col-sm-12 col-md-6">
        <div class="form-group mb-4">
            {!! Form::label('user[password_confirmation]', 'Confirmar senha', ['class' => '']) !!}
            {!! Form::password('user[password_confirmation]', ['class' => 'form-control']) !!}
            @if($errors->has('user.password_confirmation'))
                <span class="text-danger">{{ $errors->first('user.password_confirmation') }}</span>
            @endif
        </div>
    </div>
</div>
