<div class="row">
    <div class="col-lg-12 mt-0 mb-2">
        <div class="section-title mt-2">Arquivos</div>
    </div>
    <div class="col-sm-12 col-md-6">
        {!! Form::label('user[avatar]', "Foto do $model", []) !!}
        <div class="file-upload-wrapper">
            {{ Form::file('user[avatar]', ['class' => "file-upload-field"]) }}
            @if($errors->has('user.avatar'))
                <span class="text-danger">{{ $errors->first('user.avatar') }}</span>
            @endif
        </div>
    </div>

    @if(isset($model->fisic))
        {!! Form::label('fisic', "Atestado médico", []) !!}
        <div class="file-upload-wrapper">
            {{ Form::file('fisic', ['class' => 'file-upload-field']) }}
            @if($errors->has('fisic'))
                <span class="text-danger">{{ $errors->first('fisic') }}</span>
            @endif
        </div>
    @endif
</div>


