<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4>Quadro de avisos</h4>
            @can('add_messages')
                <a href="{{ route('teachers.messages.create') }}"
                   class="btn btn-info btn-lg btn-icon float-right mr-2"
                   title="Voltar"><i class="fas fa-plus"></i> Mensagem</a>
            @endcan
        </div>
        <div class="card-body">
            <div class="list-group">
                @forelse($messages as $key => $message)
                    <a class="list-group-item list-group-item-action p-2"
                       data-toggle="collapse"
                       href="#collapse-{{ $key }}"
                       role="button" aria-expanded="false"
                       aria-controls="collapse-{{ $key }}">
                        <div class="card m-0">
                            <div class="card-header"
                                 style="background: #e8e8e8"
                                 id="heading-{{ $key }}">
                                [{{ ucfirst($message->created_at->diffForHumans()) }}]&ensp;<b>{{ $message->subject }}</b>
                            </div>

                            <div id="collapse-{{ $key }}"
                                 class="collapse"
                                 aria-labelledby="heading-{{ $key }}"
                                 data-parent=".list-group-item">
                                <div class="card-body overflow">
                                    {!! $message->body !!}
                                </div>
                            </div>
                        </div>
                    </a>
                @empty
                    <div>Aguardando novos avisos ;)</div>
                @endforelse
            </div>
        </div>
    </div>
</div>

