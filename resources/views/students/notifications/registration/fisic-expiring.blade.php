@component('mail::message')
# Olá {{ $user->nickname }},

Atencão, o atestado médico anexado expira em {{ $daysToExpire }} dias. Anexe
um novo para evitar o bloqueio de sua matrícula.

@component('mail::button', ['url' => $url, 'color' => 'green'])
    Acesso ao sistema
@endcomponent

Atenciosamente,<br>
{{ config('app.name') }}
@endcomponent
