@component('mail::message')
# Olá {{ $user->nickname }},

Sua matrícula foi temporariamente cancelada. Por favor contate-nos para
regularizar a sua situação para retornar as aulas o mais rápido possível.

Atenciosamente,<br>
{{ config('app.name') }}, Oss.
@endcomponent
