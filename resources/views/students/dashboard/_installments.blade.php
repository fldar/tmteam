<div class="col-lg-6 col-sm-12">
    @if(count($installments))
        <div class="card">
            <div class="card-header">
                <h4>Mensalidades</h4>
            </div>
            <div class="card-body mb-0 p-2">
                <ul class="list-group">
                    @foreach($installments as $installment)
                        <li class="list-group-item" data-id="{{ $installment->id }}">
                            {{ $installment->due_date->format('d/m/Y') }} - {{ money_parse_by_decimal($installment->amount, 'BRL') }} {!! installmentPaymentBadge($installment) !!}
                            <span class="float-right ">
                                @if(!empty($installment->charge_link) && empty($installment->payment_date))
                                    <a href="{{ $installment->charge_link }}" target="_blank">
                                        <i class="fas fa-file-invoice"></i>
                                    </a>
                                @else
                                    <i class="fas fa-ban"></i>
                                @endif
                            </span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
</div>