<div class="col-lg-6 col-md-6 col-sm-12 col-12">
    <div class="card card-statistic-1">
        <div class="card-icon bg-success">
            <i class="fas fa-file-medical-alt"></i>
        </div>
        <div class="card-wrap">
            <div class="card-header">
                <h4>Laudo de aptidão</h4>
            </div>
            <div class="card-body text-black-50">
                @if($student->registration->fisic)
                    @if($student->registration->fisic_validation_status == 3)
                        Expira {{ $student->registration->fisic_expiration }}
                    @elseif($student->registration->fisic_validation_status == 2)
                        Inválido
                    @else
                        Aguardando aprovação
                    @endif
                @else
                    Aguardando envio
                @endif
            </div>
        </div>
    </div>
</div>