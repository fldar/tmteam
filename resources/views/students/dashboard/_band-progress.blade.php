<div class="col-lg-6 col-md-6 col-sm-12">
    @if($graduation)
        <div class="card card-statistic-1">
            <div class="card-icon bg-success">
                <i class="fas fa-ribbon"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Progressão da faixa ({{ $graduation->band->name }})</h4>
                </div>
                <div class="card-body pt-2">
                    <div class="row">
                        @foreach($graduation->band->degrees as $degree)
                            @php $achievement = $achievements->firstWhere('degree_id', $degree->id);
                                 $lessons_taken = $achievement->lessons_taken ?? 0;
                                 $percent = $lessons_taken ? ($achievement->lessons_taken/$degree->lessons * 100) : 0;
                            @endphp
                            <div class="col-lg col-xs-12 m-0 p-0" style="margin-right: 2px !important;">
                                <div class="progress border-right border-light rounded-0 position-relative">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                                         role="progressbar"
                                         style="width: {{ number_format($percent, 0) }}%">
                                    </div>
                                    <span class="justify-content-center d-flex position-absolute w-100 text-black-50">
                                        {{ number_format($percent, 0) }}%
                                    </span>
                                </div>
                                <h6 class="text-black-50 mt-1">
                                    @if($loop->first)
                                        <small style="margin-left: -3px"><i class="fas fa-flag-checkered"></i></small>
                                    @elseif($loop->last)
                                        <small style="margin-left: -5px">{{ $degree->name }}</small>
                                        <small class="float-right mt-1"><i class="fas fa-ribbon"></i></small>
                                    @else
                                        <small style="margin-left: -5px">{{ $degree->name }}</small>
                                    @endif
                                </h6>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>