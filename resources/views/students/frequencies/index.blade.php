@extends('layouts.member')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <h1>Histórico</h1>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Histórico de frequências</h4>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-md mb-0">
                                <thead>
                                    <tr class="tr-item tr-a">
                                        <th>Turma</th>
                                        <th>Data</th>
                                        <th>Validado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($frequencies as $frequency)
                                        <tr class="tr-item td-border">
                                            <td class="td-item">{{ $frequency->classroom->name }}</td>
                                            <td class="td-item">{{ $frequency->date->format('d/m/Y H:i') }}</td>
                                            <td class="td-item">{!! isFrequencyReviewedBadge($frequency->reviewed) !!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr class="tr-item td-border">
                                        <td class="td-item">
                                            <b>Total: {!! $frequencies->total() !!}</b>
                                        </td>
                                        <td colspan="2">
                                            <div class="float-right">{!! $frequencies->render() !!}</div>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

