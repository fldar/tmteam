@component('mail::message')
# Olá {{ $user->nickname }},

Em nome de toda a equipe da {{ config('app.name') }} gostaríamos de te dar as boas-vindas!

Acesse o sistema e finalize o seu acesso.

@component('mail::button', ['url' => $url, 'color' => 'green'])
  Acesso ao sistema
@endcomponent

Atenciosamente,<br>
{{ config('app.name') }}
@endcomponent
