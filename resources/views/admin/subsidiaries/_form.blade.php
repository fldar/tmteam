@if(session()->has('gateway_messages'))
    <div class="col-12 mb-4">
        <div class="bg-warning text-white p-4 rounded text-dark">
            <div class="h5">Ops, houve uma falha</div>
            <ul>
                @foreach(session()->get('gateway_messages') as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="fas fa-building lga"></i>
                Unidade
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 mt-0 mb-2">
                    <div class="section-title mt-2">Dados do estabelecimento</div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('name', 'Nome', ['class' => 'label-required']) !!}
                        {!! Form::text('name', $subsidiary->name ?? old('name'), ['class' => 'form-control']) !!}
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('phone', 'Telefone', ['class' => 'label-required']) !!}
                        {!! Form::text('phone', $subsidiary->phone ?? old('phone'), ['class' => 'form-control js-mask-phone']) !!}
                        @if($errors->has('phone'))
                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4"
                         data-toggle="tooltip"
                         data-placement="top"
                         data-original-title="O usuário deve posuir nome, CPF e aniversário preenchidos para ser elegível">
                        {!! Form::label('manager_id', 'Responsável', ['class' => 'label-required']) !!}
                        {!! Form::select('manager_id', $users, $subsidiary->manager_id ?? old('manager_id'), ['class' => 'form-control', 'title' => 'Selecione', "data-live-search" => "true"]) !!}
                        @if($errors->has('manager_id'))
                            <span class="text-danger">{{ $errors->first('manager_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('email', 'E-mail', ['class' => 'label-required']) !!}
                        {!! Form::text('email', $subsidiary->email ?? old('email'), ['class' => 'form-control']) !!}
                        @if($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('document_type', 'Tipo de inscrição', ['class' => 'label-required']) !!}
                        {!! Form::select('document_type', ['CPF' => 'CPF', 'CNPJ' => 'CNPJ'], $subsidiary->cnpj ? 'CNPJ' : 'CPF', ['class' => 'form-control']) !!}
                        @if($errors->has('document_type'))
                            <span class="text-danger">{{ $errors->first('document_type') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('business_area', 'Área de atuação', ['class' => 'label-required']) !!}
                        {!! Form::select('business_area', $businessAreas, $subsidiary->business_area ?? old('business_area'), ['class' => 'form-control', 'title' => 'Selecione', "data-live-search" => "true"]) !!}
                        @if($errors->has('business_area'))
                            <span class="text-danger">{{ $errors->first('business_area') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('lines_of_business', 'Linha de negócio', ['class' => 'label-required']) !!}
                        {!! Form::text('lines_of_business', $subsidiary->lines_of_business ?? old('lines_of_business'), ['class' => 'form-control']) !!}
                        @if($errors->has('lines_of_business'))
                            <span class="text-danger">{{ $errors->first('lines_of_business') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 company_field" style="display: {{ strlen($subsidiary->cnpj) >= 14 ? 'block' : 'none'}}">
                    <div class="form-group mb-4">
                        {!! Form::label('cnpj', 'Documento', ['class' => 'label-required']) !!}
                        {!! Form::text('cnpj', $subsidiary->cnpj ?? old('cnpj'), ['class' => 'form-control js-mask-cnpj']) !!}
                        @if($errors->has('cnpj'))
                            <span class="text-danger">{{ $errors->first('cnpj') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 company_field" style="display: {{ strlen($subsidiary->cnpj) >= 14 ? 'block' : 'none'}}">
                    <div class="form-group mb-4">
                        {!! Form::label('fantasy_name', 'Fantasia', ['class' => 'label-required']) !!}
                        {!! Form::text('fantasy_name', $subsidiary->fantasy_name ?? old('fantasy_name'), ['class' => 'form-control']) !!}
                        @if($errors->has('fantasy_name'))
                            <span class="text-danger">{{ $errors->first('fantasy_name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 company_field" style="display: {{ strlen($subsidiary->cnpj) >= 14 ? 'block' : 'none'}}">
                    <div class="form-group mb-4">
                        {!! Form::label('company_type', 'Tipo de empresa', ['class' => 'label-required']) !!}
                        {!! Form::select('company_type', $companyTypes, $subsidiary->company_type ?? old('company_type'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('company_type'))
                            <span class="text-danger">{{ $errors->first('company_type') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 mt-0 mb-2">
                    <div class="section-title mt-2">Endereço</div>
                </div>

                <div class="col-sm-12 col-md-3">
                    <div class="form-group mb-4">
                        {!! Form::label('cep', 'CEP', ['class' => 'label-required']) !!}
                        {!! Form::text('cep', $subsidiary->cep ?? old('cep'), ['class' => 'form-control js-mask-cep cep']) !!}
                        @if($errors->has('cep'))
                            <span class="text-danger">{{ $errors->first('cep') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-9">
                    <div class="form-group mb-4">
                        {!! Form::label('street', 'Rua', ['class' => 'label-required']) !!}
                        {!! Form::text('street', $subsidiary->street ?? old('street'), ['class' => 'form-control rua']) !!}
                        @if($errors->has('street'))
                            <span class="text-danger">{{ $errors->first('street') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-5">
                    <div class="form-group mb-4">
                        {!! Form::label('neighborhood', 'Bairro', ['class' => 'label-required']) !!}
                        {!! Form::text('neighborhood', $subsidiary->neighborhood ?? old('neighborhood'), ['class' => 'form-control bairro']) !!}
                        @if($errors->has('neighborhood'))
                            <span class="text-danger">{{ $errors->first('neighborhood') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-2">
                    <div class="form-group mb-2">
                        {!! Form::label('number', 'Número', ['class' => 'label-required']) !!}
                        {!! Form::text('number', $subsidiary->number ?? old('number'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                        @if($errors->has('number'))
                            <span class="text-danger">{{ $errors->first('number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <div class="form-group mb-4">
                        {!! Form::label('complement', 'Complemento', ['class' => '']) !!}
                        {!! Form::text('complement', $subsidiary->complement ?? old('complement'), ['class' => 'form-control']) !!}
                        @if($errors->has('complement'))
                            <span class="text-danger">{{ $errors->first('complement') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-3">
                    <div class="form-group mb-2">
                        {!! Form::label('city', 'Cidade', ['class' => 'label-required']) !!}
                        {!! Form::text('city', $subsidiary->city ?? old('city'), ['class' => 'form-control cidade']) !!}
                        @if($errors->has('city'))
                            <span class="text-danger">{{ $errors->first('city') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-2">
                    <div class="form-group mb-2">
                        {!! Form::label('state', 'Estado', ['class' => 'label-required']) !!}
                        {!! Form::text('state', $subsidiary->state ?? old('state'), ['class' => 'form-control uf']) !!}
                        @if($errors->has('state'))
                            <span class="text-danger">{{ $errors->first('state') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 mt-0 mb-2">
                    <div class="section-title mt-2">Dados bancários</div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('account_holder_name', 'Nome do titular', ['class' => 'label-required']) !!}
                        {!! Form::text('account_holder_name', $subsidiary->account_holder_name ?? old('account_holder_name'), ['class' => 'form-control']) !!}
                        @if($errors->has('account_holder_name'))
                            <span class="text-danger">{{ $errors->first('account_holder_name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('account_holder_document', 'Documento do titular', ['class' => 'label-required']) !!}
                        <div class="input-group">
                            <select id="account_holder_document_type"
                                    class="form-control js-mask-type"
                                    data-target="#account_holder_document">
                                <option value="1"
                                        data-mask="000.000.000-00" {{ strlen($subsidiary->account_holder_document) == 11 ? 'selected' : '' }}>
                                    CPF
                                </option>
                                <option value="2"
                                        data-mask="00.000.000/0000-00" {{ strlen($subsidiary->account_holder_document) == 14 ? 'selected' : '' }}>
                                    CNPJ
                                </option>
                            </select>
                            {!! Form::text('account_holder_document', $subsidiary->account_holder_document ?? old('account_holder_document'), ['class' => 'form-control']) !!}
                            @if($errors->has('account_holder_document'))
                                <span class="text-danger">{{ $errors->first('account_holder_document') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('bank_number', 'Banco', ['class' => 'label-required']) !!}
                        {!! Form::select('bank_number', $banks, $subsidiary->bank_number ?? old('bank_number'), ['class' => 'form-control', 'title' => 'Selecione', "data-live-search" => "true"]) !!}
                        @if($errors->has('bank_number'))
                            <span class="text-danger">{{ $errors->first('bank_number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('agency_number', 'Número da agência', ['class' => 'label-required']) !!}
                        {!! Form::text('agency_number', $subsidiary->agency_number ?? old('agency_number'), ['class' => 'form-control']) !!}
                        @if($errors->has('agency_number'))
                            <span class="text-danger">{{ $errors->first('agency_number') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('account_type', 'Tipo de conta', ['class' => 'label-required']) !!}
                        {!! Form::select('account_type', ['CHECKING' => 'Corrente', 'SAVINGS' => 'Poupança'], $subsidiary->account_type ?? old('account_type'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('account_type'))
                            <span class="text-danger">{{ $errors->first('account_type') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('account_number', 'Número da conta', ['class' => 'label-required']) !!}
                        {!! Form::text('account_number', $subsidiary->account_number ?? old('account_number'), ['class' => 'form-control']) !!}
                        @if($errors->has('account_number'))
                            <span class="text-danger">{{ $errors->first('account_number') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('account_complement_number', 'Operação') !!}
                        {!! Form::text('account_complement_number', $subsidiary->account_complement_number ?? old('account_complement_number'), ['class' => 'form-control']) !!}
                        <smal>Para contas da Caixa Econômica Federal</smal>
                        @if($errors->has('account_complement_number'))
                            <span class="text-danger">{{ $errors->first('account_complement_number') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group mb-4">
                        {!! Form::label('resource_token', 'Gateway token', ['class' => 'label-required']) !!}
                        {!! Form::text('resource_token',  $subsidiary->resource_token ?? old('resource_token'), ['class' => 'form-control', 'disabled' => !empty($subsidiary)]) !!}
                        @if($errors->has('resource_token'))
                            <span class="text-danger">{{ $errors->first('resource_token') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-sm-12 text-right mb-2">
    <button type="submit"
            class="btn btn-icon icon-left btn-success btn-lg"><i
                class="fas fa-check"></i> Salvar
    </button>
</div>

@push('scripts')
    <script type="text/javascript">

        $('#document_type').on('change', function(){
            let value = $(this).val();

            $('.company_field').toggle(value === 'CNPJ');
        });
    </script>
@endpush


