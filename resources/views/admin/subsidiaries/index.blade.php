@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Unidades</h1>
            @can('add_subsidiaries')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.subsidiaries.create') }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"> <i class="fas fa-plus"></i> Adicionar</a>
                </div>
            @endcan

            {!! Breadcrumbs::render('subsidiaries.index') !!}
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-12">
                    <h6 class="mb-3">Listagem de unidades cadastradas</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-item td-border">
                                            <th>Nome</th>
                                            <th>Responsável</th>
                                            <th>Telefone</th>
                                            <th></th>
                                        </tr>
                                        @forelse($results as $result)
                                            <tr class="tr-item td-border">
                                                <td class="td-item">{{ $result->name }}</td>
                                                <td class="td-item">{{ $result->manager->nickname }}</td>
                                                <td class="td-item">{{ maskPhone($result->phone) }}</td>
                                                <td class="no-wrap text-right">
                                                    @can('edit_subsidiaries')
                                                        <a href="{{ route('admin.subsidiaries.edit', ['id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Editar">
                                                            <i class="icon far fa-edit lg"></i></a>

                                                        @if(!empty($result->resource_token))
                                                            <a href="{{ route('admin.subsidiaries.documents.index', ['id' => $result->id]) }}"
                                                               data-toggle="tooltip"
                                                               data-placement="top"
                                                               title=""
                                                               data-original-title="Documentos">
                                                                <i class="icon far fa-list-alt lg"></i></a>
                                                        @endif
                                                    @endcan

                                                    @can('delete_subsidiaries')
                                                        <a href="#"
                                                           class="js-confirm-delete"
                                                           data-link="{{ route('admin.subsidiaries.destroy', ['id' => $result->id]) }}"
                                                           data-title="{{ $result->name }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Excluir">
                                                            <i class="icon far fa-trash-alt lg"></i></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="4"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $results->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="3">
                                                <div class="float-right">{!! $results->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
