@if($errors->any())
    @php toast()->error('Falha ao cadastrar responsável.', 'Error'); @endphp
@endif

<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-user-circle lga"></i>
                Dados do responsável
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 mt-0 mb-2">
                    <div class="section-title mt-2">Dados do pessoais</div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::hidden('user_id', $guardian->user_id ?? '') !!}
                        {!! Form::hidden('user[active]', $guardian->user->active ?? 0) !!}
                        {!! Form::label('user[cpf]', 'CPF', ['class' => '']) !!}
                        {!! Form::text('user[cpf]', old('user[cpf]'), ['class' => 'form-control js-mask-cpf', 'data-link' => route('admin.persons.show', [''])]) !!}
                        @if($errors->has('user.cpf'))
                            <span class="text-danger">{{ $errors->first('user.cpf') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-8">
                    <div class="form-group mb-4">
                        {!! Form::label('user[name]', 'Nome', ['class' => '']) !!}
                        {!! Form::text('user[name]', old('user[name]'), ['class' => 'form-control']) !!}
                        {!! Form::hidden('user[nickname]', old('user[nickname]')) !!}
                        @if($errors->has('user.name'))
                            <span class="text-danger">{{ $errors->first('user.name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('user[email]', 'E-mail', ['class' => '']) !!}
                        {!! Form::text('user[email]', old('user[email]'), ['class' => 'form-control']) !!}
                        @if($errors->has('user.email'))
                            <span class="text-danger">{{ $errors->first('user.email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('user[sex]', 'Sexo', ['class' => '']) !!}
                        {!! Form::select('user[sex]', [1 => 'Masculino', 2 => 'Feminino'], old('user[sex]'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('user.sex'))
                            <span class="text-danger">{{ $errors->first('user.sex') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('user[phone]', 'Telefone', ['class' => '']) !!}
                        {!! Form::text('user[phone]', old('user[phone]'), ['class' => 'form-control js-mask-phone']) !!}
                        @if($errors->has('user.phone'))
                            <span class="text-danger">{{ $errors->first('user.phone') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('user[birthdate]', 'Data de Nascimento', ['class' => '']) !!}
                        {!! Form::text('user[birthdate]', old('user[birthdate]'), ['class' => 'form-control js-datepicker birthdate']) !!}
                        @if($errors->has('user.birthdate'))
                            <span class="text-danger">{{ $errors->first('user.birthdate') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('user[age]', 'Idade', ['class' => '']) !!}
                        {!! Form::number('user[age]', old('user[age]'), ['class' => 'form-control js-age']) !!}
                        @if($errors->has('user.age'))
                            <span class="text-danger">{{ $errors->first('user.age') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('user[avatar]', 'Foto do responsável', []) !!}
                        <div class="file-upload-wrapper">
                            {{ Form::file('user[avatar]', ['class' => 'file-upload-field']) }}
                        </div>
                        @if($errors->has('user.avatar'))
                            <span class="text-danger">{{ $errors->first('user.avatar') }}</span>
                        @endif
                    </div>
                </div>

                @can('view_subsidiaries')
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-4">
                            {!! Form::label('user[subsidiary_id]', 'Unidade', ['class' => 'label-required']) !!}
                            {!! Form::select('user[subsidiary_id]', $subsidiaries, old('user[subsidiary_id]'), ['class' => 'form-control']) !!}
                            @if($errors->has('user.subsidiary_id'))
                                <span class="text-danger">{{ $errors->first('user.subsidiary_id') }}</span>
                            @endif
                        </div>
                    </div>
                @else
                    {!! Form::hidden('user[subsidiary_id]', $guardian->user->subsidiary_id ?? Auth::user()->subsidiary_id) !!}
                @endcan
            </div>
            <div class="row">
                <div class="col-lg-12 mt-0 mb-2">
                    <div class="section-title mt-2">Endereço</div>
                </div>
                <div class="col-sm-12 col-md-3">
                    <div class="form-group mb-4">
                        {!! Form::label('user[cep]', 'CEP', ['class' => '']) !!}
                        {!! Form::text('user[cep]', old('user[cep]'), ['class' => 'form-control js-mask-cep cep']) !!}
                        @if($errors->has('user.cep'))
                            <span class="text-danger">{{ $errors->first('user.cep') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="form-group mb-4">
                        {!! Form::label('user[street]', 'Rua', ['class' => '']) !!}
                        {!! Form::text('user[street]', old('user[street]'), ['class' => 'form-control rua']) !!}
                        @if($errors->has('user.street'))
                            <span class="text-danger">{{ $errors->first('user.street') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <div class="form-group mb-4">
                        {!! Form::label('user[neighborhood]', 'Bairro', ['class' => '']) !!}
                        {!! Form::text('user[neighborhood]', old('user[neighborhood]'), ['class' => 'form-control bairro']) !!}
                        @if($errors->has('user.neighborhood'))
                            <span class="text-danger">{{ $errors->first('user.neighborhood') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group mb-2">
                        {!! Form::label('user[number]', 'Número', ['class' => '']) !!}
                        {!! Form::text('user[number]', old('user[number]'), ['class' => 'form-control numero']) !!}
                        @if($errors->has('user.number'))
                            <span class="text-danger">{{ $errors->first('user.number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <div class="form-group mb-4">
                        {!! Form::label('user[complement]', 'Complemento', ['class' => '']) !!}
                        {!! Form::text('user[complement]', old('user[complement]'), ['class' => 'form-control complemento']) !!}
                        @if($errors->has('user.complement'))
                            <span class="text-danger">{{ $errors->first('user.complement') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-3">
                    <div class="form-group mb-2">
                        {!! Form::label('user[city]', 'Cidade', ['class' => '']) !!}
                        {!! Form::text('user[city]', old('user[city]'), ['class' => 'form-control cidade']) !!}
                        @if($errors->has('user.city'))
                            <span class="text-danger">{{ $errors->first('user.city') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group mb-2">
                        {!! Form::label('user[state]', 'Estado', ['class' => '']) !!}
                        {!! Form::text('user[state]', old('user[state]'), ['class' => 'form-control uf']) !!}
                        @if($errors->has('user.state'))
                            <span class="text-danger">{{ $errors->first('user.state') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-sm-12 text-right mb-2">
    <button type="submit"
            class="btn btn-icon icon-left btn-success btn-lg"><i
                class="fas fa-check"></i> Salvar
    </button>
</div>


@push('scripts')
    <script>
        $(document).ready(function () {

            $('input[name="user[cpf]"]').blur(function () {
                let cpf = $(this).val().replace(/\D/g, '');
                let url = $(this).data('link');

                $.ajax({
                    type: "GET",
                    url: url + `/${cpf}`,
                    dataType: 'json',
                    success: function (response) {

                        $('input[name="user_id"]').val(response.data.id);
                        $('input[name="user[name]"]').val(response.data.name);
                        $('input[name="user[email]"]').val(response.data.email);
                        $('input[name="user[phone]"]').val(response.data.phone);
                        $('input[name="user[birthdate]"]').val(response.data.birthdate);
                        $('select[name="user[sex]"]').val(response.data.sex);
                        $('input[name="user[age]"]').val(response.data.age);

                        $('.cep').val(response.data.cep);
                        $('.rua').val(response.data.street);
                        $('.complemento').val(response.data.complement);
                        $('.bairro').val(response.data.neighborhood);
                        $('.numero').val(response.data.number);
                        $('.cidade').val(response.data.city);
                        $('.uf').val(response.data.state);

                        let nickname = (response.data.name.split(" "))[0];
                        $('input[name="user[nickname]"]').val(nickname);
                    }
                });
            });

            $('input[name="user[name]"]').blur(function () {
                let nickname = ($(this).val().split(" "))[0];

                $('input[name="user[nickname]"]').val(nickname);
            });

        });
    </script>
@endpush
