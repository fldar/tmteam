@extends('layouts.default')

@section('content')

    {!! Form::model($guardian, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'enctype' =>"multipart/form-data", 'route' => ['admin.guardians.update', 'id' => $guardian->id]]) !!}
    <section class="section">
        <div class="section-header mb-0">
            <div class="section-header-back">
                <a href="{{ route('admin.guardians.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar responsável</h1>
            {!! Breadcrumbs::render('guardians.edit', $guardian) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.guardians._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
