<div class="modal fade"
     tabindex="-1"
     role="dialog"
     id="installmentsModal">
    <div class="modal-dialog"
         role="document">
            <div class="modal-content">
                {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.monthly-charge.store', 'method' => 'POST']) !!}
                <div class="modal-header">
                    <h5 class="modal-title">Gerar parcelas</h5>
                    <button type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group mb-2">
                                {!! Form::label('competence', 'Competência', ['class' => 'label-required']) !!}
                                {!! Form::text('competence', '', ['class' => 'form-control js-monthpicker', 'required' => true]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal">Cancelar
                    </button>
                    <button type="submit"
                            class="btn btn-success">Enviar
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
    </div>
</div>



