@extends('layouts.default')

@section('content')

    {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => 'admin.users.store',  'enctype' => 'multipart/form-data']) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.users.index') }}" class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Novo Usuário</h1>
            {!! Breadcrumbs::render('users.create') !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            @include('admin.users._form')
        </div>
    </section>
    {!! Form::close() !!}

@endsection
