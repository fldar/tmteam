@extends('layouts.default')

@section('content')

    {!! Form::model($user, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form',  'enctype' => 'multipart/form-data', 'route' => ['admin.users.update', 'id' => $user->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.users.index') }}" class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Usuário</h1>
            {!! Breadcrumbs::render('users.edit', $user) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            @include('admin.users._form')
        </div>
    </section>
    {!! Form::close() !!}

@endsection
