@extends('layouts.default')

@section('content')

    {!! Form::model($ageCategory, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.age-categories.update', 'id' => $ageCategory->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.age-categories.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Categoria</h1>
            {!! Breadcrumbs::render('ages_categories.edit', $ageCategory) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.age-categories._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
