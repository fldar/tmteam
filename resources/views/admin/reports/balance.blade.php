@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-4">
            <h1>Caixa</h1>
            {!! Breadcrumbs::render('reports.balance') !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.reports.balance', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 mb-1 form-group">
                                {!! Form::text('competence', Request::get('competence'), ['class' => 'form-control js-monthpicker', 'title' => 'Competência']) !!}
                            </div>
                            @can('view_subsidiaries')
                                <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                    {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade']) !!}
                                </div>
                            @endcan
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group ml-auto">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de receitas X despesas agrupados
                        por mês</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                    <tr class="tr-item td-border">
                                        <th></th>
                                        <th colspan="4" class="text-success">
                                            Receitas
                                        </th>
                                        <th colspan="4" class="text-danger">
                                            Despesas
                                        </th>
                                        <th></th>
                                    </tr>
                                    <tr class="tr-a">
                                        <th>Competência</th>
                                        <th>Esperado</th>
                                        <th>Descontos</th>
                                        <th>Taxas</th>
                                        <th>Recebido</th>
                                        <th>Gasto</th>
                                        <th>Descontos</th>
                                        <th>Taxas</th>
                                        <th>Pago</th>
                                        <th>Resultado</th>
                                    </tr>
                                    @forelse($reports as $report)
                                        <tr class="tr-item td-border">
                                            @php $balance = $report->total_received - $report->total_paid @endphp
                                            <td class="td-item">{{ "$report->competence_month/$report->competence_year" }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal((string) $report->total_charged,  'BRL') }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal((string) $report->total_charge_discount, 'BRL') }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal((string) $report->total_charge_fees, 'BRL') }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal((string) $report->total_received, 'BRL') }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal((string) $report->total_spent, 'BRL') }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal((string) $report->total_spent_discount, 'BRL') }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal((string) $report->total_spent_fees, 'BRL') }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal((string) $report->total_paid, 'BRL') }}</td>
                                            <td class="{{ $balance >= 0 ? 'text-success' : 'text-danger' }}">
                                                {{ money_parse_by_decimal((string) $balance, 'BRL') }}
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="tr-item td-border">
                                            <td colspan="10"
                                                class="text-center">Nenhum
                                                registro encontrado
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $reports->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="9">
                                                <div class="float-right">{!! $reports->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

