@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-4">
            <h1>Receitas por classe</h1>
            {!! Breadcrumbs::render('reports.balance') !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.reports.class-billing', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 mb-0 form-group">
                                {!! Form::select('classroom', $classrooms, Request::get('classroom'), ['class' => 'form-control', 'title' => 'Turma']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-1 form-group">
                                {!! Form::select('competence_month', months(), Request::get('competence_month'), ['class' => 'form-control', 'title' => 'Competência']) !!}
                            </div>
                            @can('view_subsidiaries')
                                <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                    {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade']) !!}
                                </div>
                            @endcan
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de receitas por classe
                        por mês</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                    <tr class="tr-a">
                                        <th>Turma</th>
                                        <th>Competência</th>
                                        <th>Valor esperado</th>
                                        <th>Descontos</th>
                                        <th>Taxas</th>
                                        <th>Valor recebido</th>
                                        <th>Pendente</th>
                                    </tr>
                                    @forelse($reports as $report)
                                        <tr class="tr-item td-border">
                                            <td class="td-item">{{ $report->classroom }}</td>
                                            <td class="td-item">{{ "$report->competence_month/$report->competence_year" }}</td>
                                            <td class="td-item">{{ number_format($report->expected, 2, ',', '.') }}</td>
                                            <td class="td-item">{{ number_format($report->discount, 2, ',', '.') }}</td>
                                            <td class="td-item">{{ number_format($report->charge_fee, 2, ',', '.') }}</td>
                                            <td class="td-item">{{ number_format($report->received, 2, ',', '.') }}</td>
                                            <td class="td-item">{{ number_format($report->pendent, 2, ',', '.') }}</td>
                                        </tr>
                                    @empty
                                        <tr class="tr-item td-border">
                                            <td colspan="10"
                                                class="text-center">Nenhum registro encontrado
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

