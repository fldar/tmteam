<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <icon class="fas fa-wallet lga"></icon>
                Financeiro por período
            </h4>
        </div>
        <div class="card-body">
            <div class="row justify-content-md-center mb-5">
                <div class="col-md-4 col-sm-12 mb-0 input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                    </div>
                    {{ Form::text('date_range', '', ['class' => 'form-control js-daterangepicker']) }}
                </div>
                <div class="col-md-4 col-sm-12 mb-0 form-group">
                    {{ Form::select('classroom_id[]', $classrooms, null, ['class' => 'form-control', 'title' => 'Turma', 'multiple' => 'multiple']) }}
                </div>
                @can('view_subsidiaries')
                    <div class="col-md-3 col-sm-12 mb-0 form-group">
                        {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade ']) !!}
                    </div>
                @endcan
                <div class="col-lg-1">
                    <button type="button"
                            id="filter-billing"
                            class="btn btn-primary form-control">
                        <icon class="fa fa-filter"></icon>
                    </button>
                </div>
            </div>

            <hr>

            <div class="card-title text-center"><h4>Resumo</h4></div>

            <div class="row d-flex justify-content-between mt-4">
                <div class="col col-lg-4 font-weight-bold">
                    <div class="card-wrap">
                        <div class="card-header justify-content-center">
                            <h4>Faturamento esperado</h4>
                        </div>
                        <div class="card-body text-center"
                             id="expected-incoming">
                            R$ 0,00
                        </div>
                    </div>
                </div>
                <div class="col col-lg-4 text-center font-weight-bold">
                    <div class="card-wrap">
                        <div class="card-header justify-content-center">
                            <h4>Faturamento real</h4>
                        </div>
                        <div class="card-body text-center"
                             id="real-incoming">
                            R$ 0,00
                        </div>
                    </div>
                </div>
                <div class="col col-lg-4 text-center font-weight-bold">
                    <div class="card-wrap">
                        <div class="card-header justify-content-center">
                            <h4>Conversão</h4>
                        </div>
                        <div class="card-body text-center"
                             id="conversion">
                            R$ 0,00
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-lg-center mt-4">
                <div class="col col-lg-4 text-center font-weight-bold">
                    <div class="card-wrap">
                        <div class="card-header justify-content-center">
                            <h4>Valor médio</h4>
                        </div>
                        <div class="card-body text-center"
                             id="medium-ticket">
                            R$ 0,00
                        </div>
                    </div>
                </div>
                <div class="col col-lg-4 text-center font-weight-bold">
                    <div class="card-wrap">
                        <div class="card-header justify-content-center">
                            <h4>Cobranças pagas</h4>
                        </div>
                        <div class="card-body text-center"
                             id="paid-charges">
                            0
                        </div>
                    </div>
                </div>
                <div class="col col-lg-4 text-center font-weight-bold">
                    <div class="card-wrap">
                        <div class="card-header justify-content-center">
                            <h4>Total de cobranças</h4>
                        </div>
                        <div class="card-body text-center"
                             id="total-charges">
                            0
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <div class="row justify-content-lg-center mb-5">
                <div class="col-lg-6 text-center">
                    <div class="card-title">
                        <h4>Expectativa de faturamento</h4>
                    </div>
                    <div class="canvas-wrapper"
                         style="position: relative; height: 80vh;">
                        <canvas id="chart-expected-billing"></canvas>
                    </div>
                </div>

                <div class="col-lg-6 text-center">
                    <div class="card-title">
                        <h4>Alunos por status</h4>
                    </div>
                    <div class="canvas-wrapper"
                         style="position: relative; height: 80vh;">
                        <canvas id="chart-student-status"></canvas>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

