@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Acadêmico</h1>
            {!! Breadcrumbs::render('academic') !!}
        </div>
        <div class="section-body mt-4">
            <div class="row">
                @include('admin.dashboard.academic._statistics')
            </div>
        </div>
    </section>

@endsection
