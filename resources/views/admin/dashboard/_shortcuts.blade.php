<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <icon class="fas fa-external-link-alt lga"></icon>
                Atalhos
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-3 col-sm-6 text-center mb-2">
                    <a href="{{ route('admin.students.create') }}"
                       class="btn btn-light p-3 w-100">
                        <i class="lg-icon far fa-address-book mb-3"></i>
                        <h6>Matrícula</h6>
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 text-center mb-2">
                    <a href="{{ route('admin.students.index') }}"
                       class="btn btn-light p-3 w-100">
                        <i class="lg-icon fas fa-user-ninja mb-3"></i>
                        <h6>Alunos</h6>
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 text-center mb-2">
                    <a href="{{ route('admin.classrooms.index') }}"
                       class="btn btn-light p-3 w-100">
                        <i class="lg-icon fas fa-clipboard mb-3"></i>
                        <h6>Turmas</h6>
                    </a>
                </div>

                @can('edit_settings')
                    <div class="col-lg-3 col-sm-6 text-center mb-2">
                        <a href="{{ route('admin.settings.edit') }}"
                           class="btn btn-light p-3 w-100">
                            <i class="lg-icon fas fa-cogs mb-3"></i>
                            <h6>Configurações</h6>
                        </a>
                    </div>
                @endcan
            </div>
        </div>
    </div>
</div>

