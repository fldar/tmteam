<!-- Modal -->
<div class="modal fade"
     id="guardian-modal"
     tabindex="-1"
     role="dialog">
    <div class="modal-dialog modal-lg"
         role="document">
        <div class="modal-content">
            {!! Form::open(['novalidate', 'role' => 'form', 'class' => 'form', 'id' => 'form-guardian', "enctype" => "multipart/form-data"]) !!}
            @method(empty($student->guardian) ? "POST" : "PUT")
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLabel">Responsável</h5>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 mt-0 mb-2">
                        <div class="section-title mt-2">Dados do pessoais</div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-4">
                            {!! Form::hidden('user_id', $student->guardian->user->id ?? old('guardian[user][id]'),  ['id' => 'guardian[user_id]']) !!}
                            {!! Form::label('user[cpf]', 'CPF', ['class' => '']) !!}
                            {!! Form::text('user[cpf]',
                                            $student->guardian->user->cpf ?? old('guardian[user][cpf]'),
                                            ['id' => 'guardian-cpf',
                                             'class' => 'form-control js-mask-cpf',
                                             'data-link' => route('admin.persons.show', [''])]) !!}
                            <span class="text-danger"
                                  id="user.cpf"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group mb-4">
                            {!! Form::label('user[name]', 'Nome', ['class' => '']) !!}
                            {!! Form::text('user[name]',
                                            $student->guardian->user->name ?? old('guardian[user][name]'),
                                            ['id' => 'guardian-name','class' => 'form-control']) !!}
                            {!! Form::hidden('user[nickname]',
                                             $student->guardian->user->nickname ?? old('guardian[user][nickname]'),
                                             ['id' => 'guardian-nickname']) !!}
                            <span class="text-danger"
                                  id="user.name"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-4">
                            {!! Form::label('user[email]', 'E-mail', ['class' => '']) !!}
                            {!! Form::text('user[email]',
                                           $student->guardian->user->email ?? old('guardian[user][email]'),
                                           ['id' => 'guardian[user][email]', 'class' => 'form-control']) !!}
                            <span class="text-danger"
                                  id="user.email"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-2">
                            {!! Form::label('user[sex]', 'Sexo', ['class' => '']) !!}
                            {!! Form::select('user[sex]',
                                             [1 => 'Masculino', 2 => 'Feminino'],
                                             $student->guardian->user->sex ?? old('guardian[user][sex]'),
                                             ['id' => 'guardian[user][sex]','class' => 'form-control', 'title' => 'Selecione']) !!}
                            <span class="text-danger"
                                  id="user.sex"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-2">
                            {!! Form::label('user[phone]', 'Telefone', ['class' => '']) !!}
                            {!! Form::text('user[phone]',
                                           $student->guardian->user->phone ?? old('guardian[user][phone]'),
                                           ['id' => 'guardian[user][phone]', 'class' => 'form-control js-mask-phone']) !!}
                            <span class="text-danger"
                                  id="user.phone"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-2">
                            {!! Form::label('user[birthdate]', 'Data de Nascimento', ['class' => '']) !!}
                            {!! Form::text('user[birthdate]',
                                           $student->guardian->user->birthdateBr ?? old('guardian[user][birthdate]'),
                                           ['id' => 'guardian[user][birthdate]', 'class' => 'form-control js-datepicker birthdate']) !!}
                            <span class="text-danger"
                                  id="user.birthdate"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-4">
                            {!! Form::label('user[age]', 'Idade', ['class' => '']) !!}
                            {!! Form::number('user[age]',
                                             $student->guardian->user->age ?? old('guardian[user][age]'),
                                             ['id' => 'guardian[user][age]', 'class' => 'form-control js-age']) !!}
                            <span class="text-danger"
                                  id="user.age"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group mb-4">
                            {!! Form::label('user[avatar]', 'Foto do responsável', []) !!}
                            <div class="file-upload-wrapper">
                                {{ Form::file('user[avatar]', ['class' => 'file-upload-field', 'id' => 'guardian[user][avatar]']) }}
                            </div>
                            <span class="text-danger" id="user.avatar"></span>
                        </div>
                    </div>
                    @can('view_subsidiaries')
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group mb-4">
                                {!! Form::label('user[subsidiary_id]', 'Unidade', ['class' => 'label-required']) !!}
                                {!! Form::select('user[subsidiary_id]', $subsidiaries->pluck('name', 'id'),
                                                 $student->user->subsidiary_id ?? old('guardian[user][subsidiary_id]'),
                                                 ['id' => 'guardian[user][subsidiary_id]','class' => 'form-control subsidiary']) !!}
                                <span class="text-danger"
                                      id="user.subsidiary_id"></span>
                            </div>
                        </div>
                    @else
                        {!! Form::hidden('user[subsidiary_id]', $student->user->subsidiary_id ?? Auth::user()->subsidiary_id) !!}
                    @endcan
                </div>
                <div class="row">
                    <div class="col-lg-12 mt-0 mb-2">
                        <div class="section-title mt-2">Endereço</div>
                    </div>
                    <div class="col-sm-12 col-md-3">
                        <div class="form-group mb-4">
                            {!! Form::label('user[cep]', 'CEP', ['class' => '']) !!}
                            {!! Form::text('user[cep]',
                                           $student->guardian->user->cep ?? old('guardian[user][cep]'),
                                           ['id' => 'guardian[user][cep]', 'class' => 'form-control js-mask-cep cep']) !!}
                            <span class="text-danger"
                                  id="user.cep"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <div class="form-group mb-4">
                            {!! Form::label('user[street]', 'Rua', ['class' => '']) !!}
                            {!! Form::text('user[street]',
                                           $student->guardian->user->street ?? old('guardian[user][street]'),
                                           ['id' => 'guardian[user][street]','class' => 'form-control rua']) !!}
                            <span class="text-danger"
                                  id="user.street"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="form-group mb-4">
                            {!! Form::label('user[neighborhood]', 'Bairro', ['class' => '']) !!}
                            {!! Form::text('user[neighborhood]',
                                           $student->guardian->user->neighborhood ?? old('guardian[user][neighborhood]'),
                                           ['id' => 'guardian[user][neighborhood]', 'class' => 'form-control bairro']) !!}
                            <span class="text-danger"
                                  id="user.neighborhood"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group mb-2">
                            {!! Form::label('user[number]', 'Número', ['class' => '']) !!}
                            {!! Form::text('user[number]', 
                                           $student->guardian->user->number ?? old('guardian[user][number]'),
                                           ['id' => 'guardian[user][number]', 'class' => 'form-control numero']) !!}
                            <span class="text-danger"
                                  id="user.number"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="form-group mb-4">
                            {!! Form::label('user[complement]', 'Complemento', ['class' => '']) !!}
                            {!! Form::text('user[complement]',
                                           $student->guardian->user->complement ?? old('guardian[user][complement]'),
                                            ['id' => 'guardian[user][complement]', 'class' => 'form-control complemento']) !!}
                            <span class="text-danger"
                                  id="user.complement"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3">
                        <div class="form-group mb-2">
                            {!! Form::label('user[city]', 'Cidade', ['class' => '']) !!}
                            {!! Form::text('user[city]',
                                           $student->guardian->user->city ?? old('guardian[user][city]'),
                                           ['id' => 'guardian[user][city]', 'class' => 'form-control cidade']) !!}
                            <span class="text-danger"
                                  id="user.city"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group mb-2">
                            {!! Form::label('user[state]', 'Estado', ['class' => '']) !!}
                            {!! Form::text('user[state]',
                                           $student->guardian->user->state ?? old('guardian[user][state]'),
                                           ['id' => 'guardian[user][state]', 'class' => 'form-control uf']) !!}
                            <span class="text-danger"
                                  id="user.state"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal">Close
                </button>
                <button type="submit"
                        class="btn btn-icon icon-left btn-success btn"><i
                            class="fas fa-check"></i> Salvar
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {

            $('#guardian-cpf').blur(function () {
                let cpf = $(this).val().replace(/\D/g, '');
                let url = $(this).data('link');
                let form = $(this).closest('form');

                $.ajax({
                    type: "GET",
                    url: url + `/${cpf}`,
                    dataType: 'json',
                    success: function (response) {

                        $('input[name="guardian_id"]').val(response.data.guardian_id);
                        form.find('input[name="user_id"]').val(response.data.id);
                        form.find('input[name="user[name]"]').val(response.data.name);
                        form.find('input[name="user[email]"]').val(response.data.email);
                        form.find('input[name="user[phone]"]').val(response.data.phone);
                        form.find('select[name="user[sex]"]').val(response.data.sex);
                        form.find('input[name="user[age]"]').val(response.data.age);

                        let birthdate = form.find('input[name="user[birthdate]"]');
                        birthdate[0]._flatpickr.setDate(response.data.birthdate, true, 'Y-m-d');

                        $('.cep').val(response.data.cep);
                        $('.rua').val(response.data.street);
                        $('.complemento').val(response.data.complement);
                        $('.bairro').val(response.data.neighborhood);
                        $('.numero').val(response.data.number);
                        $('.cidade').val(response.data.city);
                        $('.uf').val(response.data.state);

                        let nickname = (response.data.name.split(" "))[0];
                        form.find('input[name="user[nickname]"]').val(nickname);
                    }
                });
            });

            $('#guardian-name').blur(function () {
                let nickname = ($(this).val().split(" "))[0];

                $('#guardian-nickname').val(nickname);
            });

            $('#form-guardian').submit(function () {
                event.preventDefault();

                let formData = new FormData(this);
                let guardianId = $('input[name="guardian_id"]');
                let guardianName = $('input[name="guardian_name"]');
                let url = APP_URL + '/admin/guardians';

                if (guardianId.val() > 0) {
                    url = APP_URL + '/admin/guardians/' + guardianId.val();
                }

                $.ajax({
                    headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        guardianId.val(response.guardian.id);
                        guardianName.val(response.guardian.user.name);

                        $('#guardian-modal').modal('hide');

                        $('.cep').val(response.guardian.user.cep);
                        $('.rua').val(response.guardian.user.street);
                        $('.complemento').val(response.guardian.user.complement);
                        $('.bairro').val(response.guardian.user.neighborhood);
                        $('.numero').val(response.guardian.user.number);
                        $('.cidade').val(response.guardian.user.city);
                        $('.uf').val(response.guardian.user.state);
                    },
                    error: function (response) {
                        let errors = response.responseJSON.errors;

                        for (let error in errors) {
                            $(`span[id="${error}"]`).text(errors[error][0]);
                        }
                    }
                });
            });
        });
    </script>
@endpush


