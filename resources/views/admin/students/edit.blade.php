@extends('layouts.default')

@section('content')

    {!! Form::model($student, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'enctype' => 'multipart/form-data', 'route' => ['admin.students.update', 'id' => $student->id]]) !!}
    <section class="section">
        <div class="section-header mb-0">
            <div class="section-header-back">
                <a href="{{ route('admin.students.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Aluno</h1>
            {!! Breadcrumbs::render('students.edit', $student) !!}
        </div>

        <div class="section-options">
            <div class="text-right mb-2">
                <label class="mt-2 mr-2">
                    {!! Form::hidden('experimental', 0) !!}
                    {!! Form::checkbox('experimental', true, $student->experimental ?? false, ['class' => 'custom-switch-input', 'id' => 'experimental']) !!}
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Aula Experimental?</span>
                </label>

            </div>
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.students._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

    @include('admin.students._form-guardian')

@endsection
