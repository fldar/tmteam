@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <div class="section-header-back">
                <a href="{{ route('admin.students.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Pendências</h1>
            {!! Breadcrumbs::render('students_issues.index', $registration) !!}
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de pendências do aluno - {{ $registration->student->user->name }}</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Descrição</th>
                                            <th>Data</th>
                                        </tr>
                                        @forelse($records as $record)
                                            <tr class="tr-item td-border">
                                                <td class="td-item">{{ $record->description }}</td>
                                                <td class="td-item">{{ $record->created_at->format('d/m/Y') }}</td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="2"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $records->count() !!}</b>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

