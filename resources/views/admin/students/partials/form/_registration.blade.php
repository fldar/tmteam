<div class="row">
    {!! Form::text('registration[subsidiary_id]', $student->registration->subsidiary_id ?? auth()->user()->subsidiary_id, ['class' => 'subsidiary', 'hidden' => true]) !!}
    <div class="col-sm-12 col-lg-8">
        <div class="form-group mb-4">
            {!! Form::label("registration[classrooms][]", 'Turmas', ['class' => 'label-required']) !!}
            {!! Form::select("registration[classrooms][]",
                             $classrooms->pluck('name', 'id'),
                             empty($student->registration) ? old('registration[classrooms]') : $student->registration->classrooms->pluck('id'),
                             ['class' => 'form-control classroom', 'multiple' => true, 'title' => 'Selecione', 'onchange' => 'calcRegistrationValues()'],
                             classroomsToDataAttr($classrooms)) !!}
            @if($errors->has('registration.classrooms'))
                <span class="text-danger">{{ $errors->first('registration.classrooms') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 mt-0 mb-2">
        <div class="section-title mt-2">Financeiro</div>
    </div>
    <div class="col-sm-12 col-lg-4">
        <div class="form-group mb-4">
            {!! Form::label('registration[acquisition]', 'Valor da matrícula', ['class' => '']) !!}
            {!! Form::number('registration[acquisition]', $student->registration->acquisition ?? old('registration[amount]'), ['class' => 'form-control ',  'step' => '0.01', 'placeholder' => '0,00']) !!}
            @if($errors->has('registration.acquisition'))
                <span class="text-danger">{{ $errors->first('registration.acquisition') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-lg-4">
        <div class="form-group mb-4">
            {!! Form::label('registration[amount]', 'Valor da mensalidade', ['class' => '']) !!}
            {!! Form::number('registration[amount]', $student->registration->amount ?? old('registration[amount]'), ['class' => 'form-control ',  'step' => '0.01', 'placeholder' => '0,00']) !!}
            @if($errors->has('registration.amount'))
                <span class="text-danger">{{ $errors->first('registration.amount') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('registration[effective_date]', 'Data de início da vingência', ['class' => '']) !!}
            {!! Form::text('registration[effective_date]', $student->registration->effective_date ?? date('Y-m-d', time()) , ['class' => 'form-control js-datepicker']) !!}
            @if($errors->has('registration.effective_date'))
                <span class="text-danger">{{ $errors->first('registration.effective_date') }}</span>
            @endif
        </div>
    </div>

    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('registration[advance_discount]', 'Desconto até vencimento', ['class' => '']) !!}
            {!! Form::number('registration[advance_discount]', $student->registration->advance_discount ?? old('registration[advance_discount]'), ['class' => 'form-control ',  'step' => '0.01', 'placeholder' => '0,00']) !!}
            @if($errors->has('registration.advance_discount'))
                <span class="text-danger">{{ $errors->first('registration.advance_discount') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('registration[payment_type]', 'Tipo de Pagamento', ['class' => '']) !!}
            {!! Form::select('registration[payment_type]', enumOptions('payment_type'), $student->registration->payment_type ?? '', ['class' => 'form-control', 'title' => 'Selecione']) !!}
            @if($errors->has('registration.payment_type'))
                <span class="text-danger">{{ $errors->first('registration.payment_type') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('registration[payment_day]', 'Dia de pagamento', ['class' => '']) !!}
            {!! Form::number('registration[payment_day]', $student->registration->payment_day ?? date('j', time()), ['class' => 'form-control', 'min' => 1, 'max' => 31]) !!}
            @if($errors->has('registration.payment_day'))
                <span class="text-danger">{{ $errors->first('registration.payment_day') }}</span>
            @endif
        </div>
    </div>
</div>
