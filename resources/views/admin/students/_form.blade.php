@if($errors->any())
    @php toast()->error('Falha ao cadastrar aluno.', 'Error'); @endphp
@endif

<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-user-circle lga"></i>
                Ficha Técnica
            </h4>
            <button type="button"
                    class="btn btn-link btn-secondary ml-auto"
                    data-toggle="collapse"
                    data-target="#form-student">
                <span aria-hidden="true"><i class="fas fa-minus"></i></span>
            </button>
        </div>
        <div class="card-body collapse show"
             id="form-student">
            @include('admin.students.partials.form._students')
            @include('admin.students.partials.form._address')
            @include('admin.students.partials.form._medias')
        </div>
    </div>
</div>

<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="fas fa-tasks lga"></i>
                Matrícula
            </h4>
            <button type="button"
                    class="btn btn-link btn-secondary ml-auto"
                    data-toggle="collapse"
                    data-target="#form-subscritption">
                <span aria-hidden="true"><i class="fas fa-minus"></i></span>
            </button>
        </div>
        <div class="card-body collapse show"
             id="form-subscritption">
            @include('admin.students.partials.form._registration')
        </div>
    </div>
</div>

<div class="col-lg-12 col-sm-12 text-right mb-2">
    <button type="submit"
            class="btn btn-icon icon-left btn-success btn-lg">
        <i class="fas fa-check"></i> Salvar
    </button>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {

            let subsidiaryField = $('input[name="user[subsidiary_id]"]');
            let subsidiarySelect = $('select[name="user[subsidiary_id]"]');

            if(subsidiaryField){
                filterClassroom(subsidiaryField.val());
            }

            if(subsidiarySelect.length > 0){
                filterClassroom(subsidiarySelect.val());
            }

            $('#student-cpf').blur(function () {
                let cpf = $(this).val().replace(/\D/g, '');
                let url = $(this).data('link');
                let form = $(this).closest('form');

                $.ajax({
                    type: "GET",
                    url: url + `/${cpf}`,
                    dataType: 'json',
                    success: function (response) {
                        if (response.data.student_id > 0) {
                            $('.cpf').val('');

                            swal(
                                'Estudante já existe',
                                'Foi encontrado um registro com o cpf informado!',
                                'error'
                            );
                        } else {

                            form.find('input[name="user_id"]').val(response.data.id);
                            form.find('input[name="user[name]"]').val(response.data.name);
                            form.find('input[name="user[nickname]"]').val(response.data.nickname);
                            form.find('input[name="user[email]"]').val(response.data.email);
                            form.find('input[name="user[phone]"]').val(response.data.phone);
                            form.find('select[name="user[sex]"]').val(response.data.sex);
                            form.find('input[name="user[age]"]').val(response.data.age);

                            let birthdate = form.find('input[name="user[birthdate]"]');
                            birthdate[0]._flatpickr.setDate(response.data.birthdate, true, 'Y-m-d');

                            form.find('input[name="user[cep]"]').val(response.data.cep);
                            form.find('input[name="user[street]"]').val(response.data.street);
                            form.find('input[name="user[complement]"]').val(response.data.complement);
                            form.find('input[name="user[neighborhood]"]').val(response.data.neighborhood);
                            form.find('input[name="user[number]"]').val(response.data.number);
                            form.find('input[name="user[city]"]').val(response.data.city);
                            form.find('input[name="user[state]"]').val(response.data.state);
                        }
                    }
                });
            });

            $('.clear-guardian').click(function () {
                $('input[name="guardian_id"]').val('');
                $('input[name="guardian_name"]').val('Não');
            });

            $('select[name="registration[payment_type]"]').on('change', function (){

                let selected = parseInt($(this).find('option:selected').val());

                if(selected === 4){
                    $('input[name="registration[acquisition]"]').val('0.00');
                    $('input[name="registration[amount]').val('0.00');
                    $('input[name="registration[advance_discount]"]').val('0.00');
                }
            });

            $('.subsidiary').on('change', function (){
               let value = $(this).val();

               $('.subsidiary').val(value);
               $('.subsidiary').selectpicker('val', value);
               filterClassroomBySelect(this);
            });
        });

        function calcRegistrationValues() {
            let value = 0.00;

            $('select.classroom>option:selected').each(function (index, item) {
                if ($(item).val() > 0) {
                    value += parseFloat($(item).data('value'));
                }
            });

            $('input[name="registration[acquisition]"]').val(value);
            $('input[name="registration[amount]"]').val(value);
        }

        function filterClassroomBySelect(select) {
            let option = $(select).find('option:selected')[0];
            let subsidiary = $(option).val();

            filterClassroom(subsidiary, true);
        }

        function filterClassroom(subsidiary, clearAll = false) {
            $('.classroom > option').each(function () {

                let subsidiary_id = $(this).data('subsidiary');

                if(subsidiary_id){
                    $(this).css('display', subsidiary_id == subsidiary ? 'block' : 'none');
                    $(this).prop('disabled', subsidiary_id == subsidiary ? false : true);
                }
            });

            if(clearAll){
                $('.classroom').selectpicker('deselectAll');
            }

            $('.classroom').selectpicker('refresh');
        }
    </script>
@endpush
