@extends('layouts.default')

@section('content')
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.students.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Anexos</h1>
            {!! Breadcrumbs::render('students_attachments.show', $attachment) !!}
        </div>

        <div class="clearfix"></div>
        <div class="section-body mt-4">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                <i class="fas fa-images"></i>
                                Validar {{ $attachment->human_readable_type }}
                            </h4>
                        </div>
                        {!! Form::model($attachment, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.students.attachments.update', 'id' => $attachment->id]]) !!}
                        <div class="card-body text-center">
                            <div class="row mb-4">
                                <div class="col-12">
                                    <div>
                                        <b>Aluno:</b> {{ $attachment->model->student->user->name }}
                                    </div>
                                    <img style="max-width:600px;"
                                         src="{{ $attachment->getFullUrl() }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit"
                                            name="validation" value="2"
                                            class="btn btn-icon btn-danger btn-lg">
                                        <i class="fas fa-times"></i>
                                    </button>
                                    <button type="submit"
                                            name="validation" value="3"
                                            class="btn btn-icon btn-success btn-lg">
                                        <i class="fas fa-check"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <a href="{{ route('admin.students.index') }}"
                               class="btn btn-danger btn-lg btn-icon mr-1"
                               title="Voltar"><i class="fas fa-angle-left"></i>
                                Voltar</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
