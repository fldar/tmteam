@extends('layouts.default')

@section('content')

    {!! Form::open(['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.students.graduations.update', 'registration' => $registration->id, 'id' => $graduation->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.students.installments.index', ['registration' => $registration->id]) }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar graduação</h1>
            {!! Breadcrumbs::render('students_graduations.edit', $registration, $graduation) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.students.graduations._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection