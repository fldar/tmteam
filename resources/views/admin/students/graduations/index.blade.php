@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <div class="section-header-back">
                <a href="{{ route('admin.students.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Graduações</h1>
            @can('add_graduations')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.students.graduations.create', ['registration' => $registration]) }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"><i class="fas fa-plus"></i>
                        Adicionar</a>
                </div>
            @endcan
            {!! Breadcrumbs::render('students_graduations.index', $registration) !!}
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de Graduações cadastradas</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Modalidade</th>
                                            <th>Faixa</th>
                                            <th>Grau</th>
                                            <th>Aulas</th>
                                            <th>Data</th>
                                            <th>Certificado</th>
                                            <th></th>
                                        </tr>
                                        @foreach($results as $result)
                                            <tr class="tr-item td-border">
                                                <td class="td-item">{{ $result->modality->name }}</td>
                                                <td class="td-item">{{ $result->band->name }}</td>
                                                <td class="td-item">{{ $result->degree->name }}</td>
                                                <td class="td-item">{{ $result->lessons_taken }}</td>
                                                <td class="td-item">{{ $result->graduated_on->format('d/m/Y') }}</td>
                                                <td class="td-item text-center">
                                                    @if($result->degree->name == '0')
                                                        <a href="{{ route('admin.students.graduations.print', ['registration' => $registration->id, 'id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Certificado" target="_blank">
                                                            <i class="icon fas fa-print lg"></i></a>
                                                    @else
                                                        <i class="icon fas fa-ban lg"></i>
                                                    @endif
                                                </td>
                                                <td class="td-item">
                                                    @can('edit_graduations')
                                                        <a href="{{ route('admin.students.graduations.edit', ['registration' => $registration->id, 'id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Editar">
                                                            <i class="icon far fa-edit lg"></i></a>
                                                    @endcan

                                                    @can('delete_graduations')
                                                        <a href="#"
                                                           class="js-confirm-delete"
                                                           data-link="{{ route('admin.students.graduations.destroy', ['registration' => $registration->id, 'id' => $result->id]) }}"
                                                           data-title="Graduação"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Excluir">
                                                            <i class="icon far fa-trash-alt lg"></i></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border  bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $results->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="6">
                                                <div class="float-right">{!! $results->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

