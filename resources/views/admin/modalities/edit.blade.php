@extends('layouts.default')

@section('content')

    {!! Form::model($modality, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.modalities.update', 'id' => $modality->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.modalities.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Modalidade</h1>
            {!! Breadcrumbs::render('modalities.edit', $modality) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.modalities._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
