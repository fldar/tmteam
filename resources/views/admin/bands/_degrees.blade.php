<div class="row">
    <div class="col-lg-12 mt-2 mb-2">
        <div class="section-title mt-2">Graus e regras de faixa
        </div>
    </div>

    <div class="col-lg-12 degrees-group">
        @for($i = 0; $i <= 8; $i++)

            @php
                $hidden = empty($band->degrees[$i]) &&
                    !$errors->has("degrees.$i.name") &&
                    !$errors->has("degrees.$i.lessons") &&
                    !$errors->has("degrees.$i.minimum_period") &&
                    $i > 0;
            @endphp

            <div class="row degrees-fields" data-index="{{ $i }}" style="display: {{ $hidden ? 'none' : 'flex' }}">

                {!! Form::hidden("degrees[$i][id]", $this->degrees[$i]->id ?? old("degrees[$i][id]"), ['class' => 'degree-id', 'disabled' => $hidden]) !!}
                {!! Form::hidden("degrees[$i][_destroy]", '', ['class' => 'degree-destroy', 'disabled' => $hidden]) !!}

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label("degrees[$i][name]", 'Grau', ['class' => 'label-required']) !!}
                        {!! Form::text("degrees[$i][name]",
                                        $band->degrees[$i]->name ?? $i,
                                        ['class' => 'form-control degree-name', 'readonly' => true,'disabled' => $hidden]) !!}
                        @if($errors->has("degrees.$i.name"))
                            <span class="text-danger">
                                {{ $errors->first("degrees.$i.name") }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-3">
                    <div class="form-group mb-2">
                        {!! Form::label("degrees[$i][lessons]", 'Aulas', ['class' => 'label-required']) !!}
                        {!! Form::number("degrees[$i][lessons]",
                                        $band->degrees[$i]->lessons ?? old("degrees[$i][lessons]"),
                                        ['class' => 'form-control degree-name', 'disabled' => $hidden]) !!}
                        @if($errors->has("degrees.$i.lessons"))
                            <span class="text-danger">
                                {{ $errors->first("degrees.$i.lessons") }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-3">
                    <div class="form-group mb-2">
                        {!! Form::label("degrees[$i][minimum_period]", 'Período mínimo (dias)', ['class' => 'label-required']) !!}
                        {!! Form::number("degrees[$i][minimum_period]",
                                        $band->degrees[$i]->minimum_period ?? old("degrees[$i][minimum_period]"),
                                        ['class' => 'form-control degree-name', 'disabled' => $hidden]) !!}
                        @if($errors->has("degrees.$i.minimum_period"))
                            <span class="text-danger">
                                {{ $errors->first("degrees.$i.minimum_period") }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-2 col-md-1 align-self-center action-buttons">
                    <br>
                    <button class="btn btn-lg btn-success add-degree"
                            type="button">
                        <span class="fas fa-plus"></span>
                    </button>
                </div>

                @if($i > 0)
                    <div class="col-sm-2 col-md-1 align-self-center action-buttons">
                        <br>
                        <button class="btn btn-lg btn-danger remove-degree"
                                type="button">
                            <span class="fas fa-minus"></span>
                        </button>
                    </div>
                @endif

            </div>
        @endfor
    </div>

</div>
