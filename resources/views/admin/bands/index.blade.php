@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Faixas</h1>

            @can('add_bands')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.bands.create') }}"
                       class="btn btn-success btn-lg mr-2"
                       title="Adicionar">
                        <i class="fas fa-plus"></i> Adicionar</a>
                </div>
            @endcan

            {!! Breadcrumbs::render('bands.index') !!}
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de faixas cadastradas</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>#</th>
                                            <th>Faixa</th>
                                            <th>Modalidade</th>
                                            <th></th>
                                        </tr>
                                        @forelse($bands as $band)
                                            <tr class="tr-item td-border">
                                                <td class="td-item">
                                                    <span class="faixa border" style="background-color: {{ $band->color }}"></span>
                                                </td>
                                                <td class="td-item">{{ $band->name }}</td>
                                                <td class="td-item">{{ $band->modality->name }}</td>
                                                <td class="td-item text-right">

                                                    @can('edit_bands')
                                                        <a href="{{ route('admin.bands.edit', ['id' => $band->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Editar">
                                                            <i class="icon far fa-edit lg"></i></a>
                                                    @endcan

                                                    @can('delete_bands')
                                                        <a href="#"
                                                           class="js-confirm-delete"
                                                           data-link="{{ route('admin.bands.destroy', ['id' => $band->id]) }}"
                                                           data-title="{{ $band->full_name }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Excluir">
                                                            <i
                                                                    class="icon far fa-trash-alt lg"></i></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="4"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border  bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $bands->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="3">
                                                <div class="float-right">{!! $bands->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
