@if($errors->any())
    @php toast()->error('Falha ao cadastrar faixa.', 'Error'); @endphp
@endif

<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-edit lga"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 mt-0 mb-2">
                    <div class="section-title mt-2">Dados cadastrais</div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('name', 'Nome', ['class' => 'label-required']) !!}
                        {!! Form::text('name', $band->name ?? old('name'), ['class' => 'form-control']) !!}
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('color', 'Cor', ['class' => 'label-required']) !!}
                        {!! Form::color('color', $band->color ?? old('color'), ['class' => 'form-control']) !!}
                        @if($errors->has('color'))
                            <span class="text-danger">{{ $errors->first('color') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('modality_id', 'Modalidade', ['class' => 'label-required']) !!}
                        {!! Form::select('modality_id', $modalities, $band->modality_id ?? old('modality_id'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('modality_id'))
                            <span class="text-danger">{{ $errors->first('modality_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('next_id', 'Próxima faixa', ['class' => '']) !!}
                        {!! Form::select('next_id', $bands->pluck('name', 'id'),
                                                    $band->next_id ?? old('next_id'),
                                                    ['class' => 'form-control', 'title' => 'Selecione'],
                                                    bandsToDataAttribute($bands)) !!}
                        @if($errors->has('next_id'))
                            <span class="text-danger">{{ $errors->first('next_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div class="form-group mb-2">
                        {!! Form::label('age_categories', 'Categorias de idade', ['class' => 'label-required']) !!}
                        {!! Form::select('age_categories[]', $ageCategories,
                                                             isset($band->ageCategories) ? $band->ageCategories->pluck('id') : old('age_categories'),
                                                             ['class' => 'form-control js-box js-select-target', 'multiple']) !!}
                        @if($errors->has('age_categories'))
                            <span class="text-danger">{{ $errors->first('age_categories') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            @include('admin.bands._degrees')
        </div>
        <div class="card-footer text-right">
            <button type="submit"
                    class="btn btn-icon icon-left btn-success btn-lg">
                <i class="fas fa-check"></i> Salvar
            </button>
        </div>
    </div>
</div>

@push('scripts')
    <script type="application/javascript">
        filterBands();

        $('#modality_id').on('change', function () {
            filterBands(true);
        });

        function filterBands(clearSelected = false){
            var field = $('#modality_id');
            var modality = parseInt(field.val());

            $('#next_id>option').each(function () {

                var band_modality = parseInt($(this).data('modality'));

                if(!isNaN(band_modality)){
                    $(this).toggle(band_modality === modality);
                }

                if(clearSelected){
                    this.selected = false;
                }
            });

            setTimeout(function (){
                $('#next_id').selectpicker('refresh');
            }, 200);
        }
    </script>
@endpush
