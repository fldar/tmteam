@extends('layouts.default')

@section('content')

    {!! Form::model($teacher, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'enctype' =>"multipart/form-data", 'route' => ['admin.teachers.update', 'id' => $teacher->id]]) !!}
    <section class="section">
        <div class="section-header mb-0">
            <div class="section-header-back">
                <a href="{{ route('admin.teachers.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Professor</h1>
            {!! Breadcrumbs::render('teachers.edit', $teacher) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.teachers._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
