@if($errors->any())

    @php toast()->error('Falha ao cadastrar professor.', 'Error'); @endphp
@endif

<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-user-circle lga"></i>
                Dados do Professor
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 mt-0 mb-2">
                    <div class="section-title mt-2">Dados pessoais</div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::hidden('user_id', $teacher->user_id ?? '') !!}
                        {!! Form::label('user[cpf]', 'CPF', ['class' => 'label-required']) !!}
                        {!! Form::text('user[cpf]',
                                        $teacher->user->cpf ?? old('user.cpf'),
                                        ['class' => 'form-control js-mask-cpf cpf',
                                         'data-link' => route('admin.persons.show', ['']),
                                         'readonly' => !empty($teacher->id)]) !!}
                        @if($errors->has('user.cpf'))
                            <span class="text-danger">{{ $errors->first('user.cpf') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-8">
                    <div class="form-group mb-4">
                        {!! Form::label('user[name]', 'Nome', ['class' => 'label-required']) !!}
                        {!! Form::text('user[name]', $teacher->user->name ?? old('user[name]'), ['class' => 'form-control']) !!}
                        @if($errors->has('user.name'))
                            <span class="text-danger">{{ $errors->first('user.name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('user[nickname]', 'Apelido', ['class' => 'label-required']) !!}
                        {!! Form::text('user[nickname]', $teacher->user->nickname ?? old('user.nickname'), ['class' => 'form-control']) !!}
                        @if($errors->has('user.nickname'))
                            <span class="text-danger">{{ $errors->first('user.nickname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('user[email]', 'E-mail', ['class' => 'label-required']) !!}
                        {!! Form::text('user[email]', $teacher->user->email ?? old('user.email'), ['class' => 'form-control']) !!}
                        @if($errors->has('user.email'))
                            <span class="text-danger">{{ $errors->first('user.email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('user[sex]', 'Sexo', ['class' => '']) !!}
                        {!! Form::select('user[sex]', [1 => 'Masculino', 2 => 'Feminino'], $teacher->user->sex ?? old('user.sex'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('user.sex'))
                            <span class="text-danger">{{ $errors->first('user.sex') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('user[phone]', 'Telefone', ['class' => 'label-required']) !!}
                        {!! Form::text('user[phone]',  $teacher->user->phone ?? old('user.phone'), ['class' => 'form-control js-mask-phone']) !!}
                        @if($errors->has('user.phone'))
                            <span class="text-danger">{{ $errors->first('user.phone') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('user[birthdate]', 'Data de Nascimento', ['class' => '']) !!}
                        {!! Form::text('user[birthdate]', !empty($teacher) ? $teacher->user->birthdate : old('user.birthdate'), ['class' => 'form-control js-datepicker']) !!}
                        @if($errors->has('user.birthdate'))
                            <span class="text-danger">{{ $errors->first('user.birthdate') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('user[avatar]', 'Foto do usuário', ['class' => '']) !!}
                        <div class="file-upload-wrapper">
                            {{ Form::file('user[avatar]', ['class' => "file-upload-field"]) }}
                        </div>
                        @if($errors->has('user.avatar'))
                            <span class="text-danger">{{ $errors->first('user.avatar') }}</span>
                        @endif
                    </div>
                </div>

                @can('view_subsidiaries')
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-2">
                            {!! Form::label('user[subsidiary_id]', 'Unidade', ['class' => 'label-required']) !!}
                            {!! Form::select('user[subsidiary_id]', $subsidiaries, $teacher->user->subsidiary_id ?? old('user[subsidiary_id]'), ['class' => 'form-control']) !!}
                            @if($errors->has('user.subsidiary_id'))
                                <span class="text-danger">{{ $errors->first('user.subsidiary_id') }}</span>
                            @endif
                        </div>
                    </div>
                @else
                    {!! Form::hidden('subsidiary_id', Auth::user()->subsidiary_id) !!}
                @endcan
            </div>
            <div class="row">
                <div class="col-lg-12 mt-0 mb-2">
                    <div class="section-title mt-2">Endereço</div>
                </div>
                <div class="col-sm-12 col-md-3">
                    <div class="form-group mb-4">
                        {!! Form::label('user[cep]', 'CEP', ['class' => '']) !!}
                        {!! Form::text('user[cep]', $teacher->user->cep ?? old('user[cep]'), ['class' => 'form-control js-mask-cep cep']) !!}
                        @if($errors->has('user.cep'))
                            <span class="text-danger">{{ $errors->first('user.cep') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="form-group mb-4">
                        {!! Form::label('user[street]', 'Rua', ['class' => '']) !!}
                        {!! Form::text('user[street]', $teacher->user->street ?? old('user[street]'), ['class' => 'form-control rua']) !!}
                        @if($errors->has('user.street'))
                            <span class="text-danger">{{ $errors->first('user.street') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <div class="form-group mb-4">
                        {!! Form::label('user[neighborhood]', 'Bairro', ['class' => '']) !!}
                        {!! Form::text('user[neighborhood]', $teacher->user->neighborhood ?? old('user[neighborhood]'), ['class' => 'form-control bairro']) !!}
                        @if($errors->has('user.neighborhood'))
                            <span class="text-danger">{{ $errors->first('user.neighborhood') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group mb-2">
                        {!! Form::label('user[number]', 'Número', ['class' => '']) !!}
                        {!! Form::text('user[number]', $teacher->user->number ?? old('user[number]'), ['class' => 'form-control']) !!}
                        @if($errors->has('user.number'))
                            <span class="text-danger">{{ $errors->first('user.number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <div class="form-group mb-4">
                        {!! Form::label('user[complement]', 'Complemento', ['class' => '']) !!}
                        {!! Form::text('user[complement]', $teacher->user->complement ?? old('user[complement]'), ['class' => 'form-control']) !!}
                        @if($errors->has('user.complement'))
                            <span class="text-danger">{{ $errors->first('user.complement') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-3">
                    <div class="form-group mb-2">
                        {!! Form::label('user[city]', 'Cidade', ['class' => '']) !!}
                        {!! Form::text('user[city]', $teacher->user->city ?? old('user[city]'), ['class' => 'form-control cidade']) !!}
                        @if($errors->has('user.city'))
                            <span class="text-danger">{{ $errors->first('user.city') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group mb-2">
                        {!! Form::label('user[state]', 'Estado', ['class' => '']) !!}
                        {!! Form::text('user[state]', $teacher->user->state ?? old('user[state]'), ['class' => 'form-control uf']) !!}
                        @if($errors->has('user.state'))
                            <span class="text-danger">{{ $errors->first('user.state') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-sm-12">

    <div class="card">
        <div class="card-header">
            <h4>
                <i class="fas fa-dumbbell  lga"></i>
                Modalidades
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group mb-2">
                        {!! Form::label('modalities', 'Modalidades', ['class' => 'label-required']) !!}
                        {!! Form::select('modalities[]', $modalities, isset($teacher->modalities) ? $teacher->modalities->pluck('id') : old('modalities'), ['class' => 'form-control js-box js-select-target', 'multiple']) !!}
                        @if($errors->has('modalities'))
                            <span class="text-danger">{{ $errors->first('modalities') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="col-lg-12 col-sm-12 text-right mb-2">
    <button type="submit"
            class="btn btn-icon icon-left btn-success btn-lg">
        <i class="fas fa-check"></i> Salvar
    </button>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {

            $('.cpf').blur(function () {
                let cpf = $(this).val().replace(/\D/g, '');
                let url = $(this).data('link');

                $.ajax({
                    type: "GET",
                    url: url + `/${cpf}`,
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (response) {

                        if (response.data.teacher_id > 0) {
                            $('.cpf').val('');

                            swal(
                                'Professor já existe',
                                'Foi encontrado um registro com o cpf informado!',
                                'error'
                            );

                            return false;
                        }

                        $('input[name="user_id"]').val(response.data.id);
                        $('input[name="user[name]"]').val(response.data.name);
                        $('input[name="user[nickname]"]').val(response.data.nickname);
                        $('input[name="user[email]"]').val(response.data.email);
                        $('input[name="user[phone]"]').val(response.data.phone);
                        $('input[name="user[birthdate]"]').val(response.data.birthdate);
                        $('select[name="user[sex]"]').val(response.data.sex);
                        $('input[name="user[age]"]').val(response.data.age);

                        $('input[name="user[cep]"]').val(response.data.cep);
                        $('input[name="user[street]"]').val(response.data.street);
                        $('input[name="user[complement]"]').val(response.data.complement);
                        $('input[name="user[neighborhood]"]').val(response.data.neighborhood);
                        $('input[name="user[number]"]').val(response.data.number);
                        $('input[name="user[city]"]').val(response.data.city);
                        $('input[name="user[state]"]').val(response.data.state);
                    }
                });
            });
        });
    </script>
@endpush
