@extends('layouts.default')

@section('content')

    {!! Form::model($balanceRecord, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.balance-records.update', 'id' => $balanceRecord->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.balance-records.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar lançamento</h1>
            {!! Breadcrumbs::render('balance_records.edit', $balanceRecord) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.balance-records._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
