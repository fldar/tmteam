@can($tree['permission'])

    <li class="dropdown {{ activeTree($tree) ? 'active' : '' }}">
        <a href="#"
           class="nav-link has-dropdown"
           data-toggle="dropdown">
            <i class="fas {{ $tree['icon'] }}"></i>
            <span>{{ $tree['name'] }}</span>
        </a>
        <ul class="dropdown-menu"
            style="display: none;">
            @foreach($tree['options'] as $option)
                @include('admin.partials.option', ['item' => $option])
            @endforeach
        </ul>
    </li>


@endcan