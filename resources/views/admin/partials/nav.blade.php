<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
    <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar"
                   class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

    </form>
    <ul class="navbar-nav navbar-right">

        @auth()
            @include('layouts.common._notifications')

            <li class="dropdown">
                <a href="#" data-toggle="dropdown"
                   class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                    <img alt="image" src="{{ auth()->user()->avatarUrl }}"
                         class="rounded-circle mr-1">
                    <div class="d-sm-none d-lg-inline-block">
                        Olá, {{ auth()->user()->nickname }}</div>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    @include('layouts.common._roles')
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('auth.session.logout') }}"
                       class="dropdown-item has-icon text-danger">
                        <i class="fas fa-sign-out-alt"></i> Sair
                    </a>
                </div>
            </li>
        @endauth
    </ul>
</nav>
