@extends('layouts.print')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <h1>Alunos qualificados</h1>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de Graduações cadastradas</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Aluno</th>
                                            <th>Modalidade</th>
                                            <th>Faixa</th>
                                            <th>Grau</th>
                                            <th>Aulas</th>
                                            <th>Graduação</th>
                                        </tr>
                                        @php $total = 0; @endphp
                                        @foreach($results as $graduation)
                                            @php $total++; @endphp
                                            <tr class="tr-item td-border">
                                                <td class="td-item no-wrap">
                                                    <img class="border-100"
                                                         src="{{ $graduation->registration->student->user->avatarUrl }}"
                                                         alt="{{ $graduation->name }}"
                                                         title="{{ $graduation->name }}"
                                                         width="40"> {{ $graduation->name }}
                                                </td>
                                                <td class="td-item">{{ $graduation->modality }}</td>
                                                <td class="td-item">{{ $graduation->band }}</td>
                                                <td class="td-item">{{ $graduation->degree }}</td>
                                                <td class="td-item">{{ $graduation->lessons }}</td>
                                                <td class="td-item">{{ $graduation->graduated_on->diffForHumans() }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-2 pt-2 text-muted">
                                    <b>Total: {!! $total !!}</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

