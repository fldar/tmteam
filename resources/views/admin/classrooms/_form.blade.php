<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-edit lga"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('name', 'Nome', ['class' => 'label-required']) !!}
                        {!! Form::text('name', $classroom->name ?? old('name'), ['class' => 'form-control']) !!}
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('modality_id', 'Modalidade', ['class' => 'label-required']) !!}
                        {!! Form::select('modality_id', $modalities, $classroom->modality_id ?? old('modality_id'), ['class' => 'form-control']) !!}
                        @if($errors->has('modality_id'))
                            <span class="text-danger">{{ $errors->first('modality_id') }}</span>
                        @endif
                    </div>
                </div>

                @can('view_subsidiaries')
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-4">
                            {!! Form::label('subsidiary_id', 'Unidade', ['class' => 'label-required']) !!}
                            {!! Form::select('subsidiary_id', $subsidiaries, $classroom->subsidiary_id ?? old('subsidiary_id'), ['class' => 'form-control']) !!}
                            @if($errors->has('subsidiary_id'))
                                <span class="text-danger">{{ $errors->first('subsidiary_id') }}</span>
                            @endif
                        </div>
                    </div>
                @else
                    {!! Form::hidden('subsidiary_id', $classroom->subsidiary_id ?? auth()->user()->subsidiary_id) !!}
                @endcan

                <div class="col-sm-12 col-md-12">
                    <div class="form-group mb-4">
                        {!! Form::label('teachers', 'Professores', ['class' => 'label-required']) !!}
                        {!! Form::select('teachers[]', $teachers, isset($classroom->teachers) ? $classroom->teachers->pluck('id') : old('teachers'), ['class' => 'form-control js-box', 'multiple']) !!}
                        @if($errors->has('teachers'))
                            <span class="text-danger">{{ $errors->first('teachers') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit"
                    class="btn btn-icon icon-left btn-success btn-lg">
                <i class="fas fa-check"></i> Salvar
            </button>
        </div>
    </div>
</div>
