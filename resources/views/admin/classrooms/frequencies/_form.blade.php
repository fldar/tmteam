@if($errors->any())
    @php toast()->error('Falha ao cadastrar frequência.', 'Error'); @endphp
@endif

<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-edit lga"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::hidden('classroom_id', $classroom->id) !!}
                        {!! Form::label('date', 'Data', ['class' => 'label-required']) !!}
                        {!! Form::text('date', isset($frequency->date) ? $frequency->date->format('Y-m-d H:i') : old('date'), ['class' => 'form-control js-datetimepicker']) !!}
                        @if($errors->has('date'))
                            <span class="text-danger">{{ $errors->first('date') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('registration_id', 'Aluno', ['class' => 'label-required']) !!}
                        {!! Form::select('registration_id[]', $students, old('registration_id'), ['multiple' => 'multiple', 'class' => 'form-control', 'title' => 'Selecione', 'data-live-search' => "true"]) !!}
                        @if($errors->has('registration_id'))
                            <span class="text-danger">{{ $errors->first('registration_id') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('reviewed', 'Revisado', ['class' => 'label-required']) !!}
                        {!! Form::select('reviewed', ['0' => 'Não', '1' => 'Sim'], $frequency->reviewed ?? old('reviewed'), ['class' => 'form-control']) !!}
                        @if($errors->has('registration_id'))
                            <span class="text-danger">{{ $errors->first('registration_id') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-12">
                    <div class="form-group mb-2">
                        {!! Form::label('remark', 'Observações', ['class' => '']) !!}
                        {!! Form::textarea('remark', $frequency->remark ?? old('remark'), ['class' => 'form-control']) !!}
                        @if($errors->has('remark'))
                            <span class="text-danger">{{ $errors->first('remark') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit"
                    class="btn btn-icon icon-left btn-success btn-lg">
                <i class="fas fa-check"></i> Salvar
            </button>
        </div>
    </div>
</div>

