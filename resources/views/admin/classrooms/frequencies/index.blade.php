@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <div class="section-header-back">
                <a href="{{ route('admin.classrooms.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $classroom->name }} - Frequências</h1>
            @can('add_classrooms')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.classrooms.frequencies.create', ['classroom_id' => $classroom->id]) }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"><i class="fas fa-plus"></i>
                        Adicionar</a>
                </div>
            @endcan
            {!! Breadcrumbs::render('classrooms_frequencies.index', $classroom) !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => ['admin.classrooms.frequencies.index', 'classroom_id' => $classroom->id], 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 mb-0 form-group">
                                {!! Form::select('registration_id', $students, Request::get('registration_id'), ['class' => 'form-control', 'title' => 'Aluno', 'data-live-search' => 'true']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::text('date', Request::get('date'), ['class' => 'form-control js-datepicker', 'placeholder' => 'Data']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('status', [0 => 'Não', 1 => 'Sim'], Request::get('status'), ['class' => 'form-control', 'title' => 'Revisado ']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de frequências registradas</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Aluno</th>
                                            <th>Horário</th>
                                            <th>Revisado?</th>
                                            <th></th>
                                        </tr>
                                        @forelse($results as $result)
                                            <tr class="tr-item td-border">
                                                <td class="td-item">{{ $result->registration->student->user->name }}</td>
                                                <td class="td-item">{{ $result->date->format('d/m/Y H:i') }}</td>
                                                <td class="td-item">{!! isFrequencyReviewedBadge($result->reviewed) !!}</td>
                                                <td class="td-item text-right">

                                                    @can('edit_classrooms_submodules')
                                                        <a href="{{ route('admin.classrooms.frequencies.edit', ['classroom_id' => $classroom->id, 'id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Editar">
                                                            <i class="icon far fa-edit lg"></i></a>
                                                    @endcan
                                                    @can('delete_classrooms_submodules')
                                                        <a href="#"
                                                           class="js-confirm-delete"
                                                           data-link="{{ route('admin.classrooms.frequencies.destroy', ['classroom_id' => $classroom->id, 'id' => $result->id]) }}"
                                                           data-title="A frequência"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Excluir">
                                                            <i class="icon far fa-trash-alt lg"></i></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="4"
                                                    class="text-center">Nenhum
                                                    registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border  bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $results->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="3">
                                                <div class="float-right">{!! $results->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

