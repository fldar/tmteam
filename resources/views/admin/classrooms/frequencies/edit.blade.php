@extends('layouts.default')

@section('content')

    {!! Form::model($frequency, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.classrooms.frequencies.update', 'classroom_id' => $classroom->id, 'id' => $frequency->id]]) !!}
    <section class="section">
        <div class="section-header mb-0">
            <div class="section-header-back">
                <a href="{{ route('admin.classrooms.frequencies.index', ['classroom_id' => $classroom->id]) }}"
                   class="btn btn-icon" title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Turma - {{ $classroom->name }} - Frequência</h1>
            {!! Breadcrumbs::render('classrooms_frequencies.edit', $classroom, $frequency) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.classrooms.frequencies._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
