@extends('layouts.default')

@section('content')

    {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.classrooms.frequencies.store', 'classroom_id' => $classroom->id]]) !!}
    <section class="section">
        <div class="section-header mb-0">
            <div class="section-header-back">
                <a href="{{ route('admin.classrooms.frequencies.index', ['classroom_id' => $classroom->id]) }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Turma - {{ $classroom->name }} - Nova Frequência</h1>
            {!! Breadcrumbs::render('classrooms_frequencies.create', $classroom) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.classrooms.frequencies._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
