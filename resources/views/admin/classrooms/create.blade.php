@extends('layouts.default')

@section('content')

    {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => 'admin.classrooms.store']) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.classrooms.index') }}"
                   class="btn btn-icon" title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Nova Turma</h1>
            {!! Breadcrumbs::render('classrooms.create') !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.classrooms._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
