@extends('layouts.default')

@section('content')

    {!! Form::model($origin, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.origins.update', 'id' => $origin->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.origins.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Origem</h1>
            {!! Breadcrumbs::render('students_origins.edit', $origin) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.origins._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
