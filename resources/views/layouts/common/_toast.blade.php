@if(Session::has('toasts'))
	<script type="text/javascript">
		@foreach(Session::get('toasts') as $toast)
		iziToast.{{ $toast['level'] }} ({
			title: "{{ $toast['title'] }}",
			message: "{{ $toast['message'] }}",
			position: 'topRight'
        });
		@endforeach
	</script>
@endif
