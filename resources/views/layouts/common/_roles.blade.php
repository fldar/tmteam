<div class="dropdown-title" style="padding: 1px 20px;">
    <b>Visões</b>
</div>
<div class="dropdown-divider"></div>
@foreach(auth()->user()->roles as $role)
    @switch($role->name)
        @case('root')
        @case('gestor')
        <a href="{{ route('admin.dashboard.index') }}"
           class="dropdown-item has-icon"><i
                    class="far fa-circle"></i>{{ $role->details }}
        </a>
        @break
        @case('aluno')
        <a href="{{ route('students.dashboard.index') }}"
           class="dropdown-item has-icon"><i
                    class="far fa-circle"></i>{{ $role->details }}
        </a>
        @break
        @case('professor')
        <a href="{{ route('teachers.dashboard.index') }}"
           class="dropdown-item has-icon"><i
                    class="far fa-circle"></i>{{ $role->details }}
        </a>
        @break
        @case('responsavel')
        <a href="{{ route('guardians.dashboard.index') }}"
           class="dropdown-item has-icon"><i
                    class="far fa-circle"></i>{{ $role->details }}
        </a>
        @break
    @endswitch
@endforeach
