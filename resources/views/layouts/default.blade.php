<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#2c4e87">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEO::generate() !!}

    @include('layouts.common._favicons')

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ mix('css/global.css') }}">
</head>
<body class="sidebar-gone">
<div id="app">
    <div class="main-wrapper">
        @include('admin.partials.nav')
        @include('admin.partials.sidebar')

        <div class="main-content">
            @yield('content')
        </div>
        @include('admin.partials.footer')
    </div>

    <!-- General JS Libraies -->
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Template JS File -->
    <script src="{{ mix('js/global.js') }}"></script>

    @include('layouts.common._toast')
    @stack('scripts')
</div>
</body>
</html>
