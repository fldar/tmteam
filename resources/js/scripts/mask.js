$(function () {
    $('.js-mask-date').mask('00/00/0000');

    $('.js-mask-cnpj').mask('00.000.000/0000-00', {reverse: true});

    $('.js-mask-cpf').mask('000.000.000-00', {reverse: true});

    $('.js-mask-time').mask('00:00:00');

    $('.js-mask-datetime').mask('00/00/0000 00:00:00');

    $('.js-mask-hour').mask('00:00');

    $('.js-mask-date-cart').mask('00/00');

    $('.js-mask-month').mask('00');

    $('.js-mask-year').mask('00');

    $('.js-mask-monthyear').mask('00/0000');

    $('.js-mask-size').mask('0000,00', {reverse: true});

    $('.js-mask-money').mask('000.000.000.000.000,00', {reverse: true});

    $('.js-mask-percent').mask('##0,00%', {reverse: true});

    $('.js-mask-phone').mask('(00) 00000-0000');

    $('.js-mask-cep').mask('00000-000');

    $('.js-mask-state-registration').mask('00.000.000-0', {reverse: true});

    $('.js-mask-type').on('change', maskByType);

    maskByType();
});

function maskByType() {

    $('select.js-mask-type').each(function (){
        let selected   = $(this).find('option:selected');
        let mask       = selected.data('mask');
        let target     = $(this).data('target');

        $(target).unmask();
        $(target).mask(mask, {reverse: true});
    });
}




