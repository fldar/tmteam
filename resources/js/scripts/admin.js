const APP_URL = window.location.origin;

$(function () {
    $("select.form-control").not('.js-box').selectpicker({
        style: 'btn btn-light form-control',
        actionsBox: true,
        deselectAllText: 'Nenhum',
        selectAllText: 'Todos'
    });

    $("form").on("change", ".file-upload-field", function () {
        $(this).parent(".file-upload-wrapper")
            .attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
    });

    $('.js-confirm-delete').on('click', function (e) {
        e.preventDefault();

        swal({
            title: 'Você tem certeza?',
            text: $(this).data('title') + " será excluído(a).",
            type: 'error',
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            showCancelButton: true,
            confirmButtonColor: '#3f51b5',
            cancelButtonColor: '#ff4081',
        }).then((result) => {

            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    url: $(this).data('link'),
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (data) {
                        swal('Excluído com sucesso', data.message, 'success');

                        if (data.route) {
                            setTimeout(function () {
                                window.location.href = data.route;
                            }, 500);

                        }
                    },
                    error: function (data) {
                        swal('Falha na solicitação', data.responseJSON.error, 'error');
                    }
                });
            } else if (result.dismiss === 'cancel') {
                swal('Ação cancelada', 'O registro não foi excluido', 'warning');
            }
        })
    });

    $(".js-clear").on("click", function () {
        $('.form')[0].reset();
    });

    $(".js-box").bootstrapDualListbox({
        filterPlaceHolder: "Filtrar",
        infoText: false
    });

    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

    if ($(".js-redactor").length) {
        $('.js-redactor').summernote({
            lang: 'pt-BR',
            height: 300,
            tabsize: 2,
            maximumImageFileSize: 2097152
        });
    }
});

function randomColor() {
    return ("#" + Math.floor(Math.random() * 16777215).toString(16)).padEnd(7, '');
}

function getHexContrast(hex) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }

    return (parseInt(hex, 16) > 0xffffff / 2) ? 'black' : 'white';
}


