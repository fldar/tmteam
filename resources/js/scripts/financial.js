$(function () {
    const financial = document.getElementById('chart-financial');
    const billing = document.getElementById('chart-expected-billing');
    const students = document.getElementById('chart-student-status');

    if(financial != null ){
        var year_dash = new Chart(financial, {
            type: 'line',
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                plugins: { datalabels: false }
            },
            data: {
                labels: lastTwelveMonths(),
                datasets: []
            }
        });
    }

    if(billing != null ){
        var billing_dash = pieChart(billing, {});
    }

    if(students != null ){
        var student_dash = pieChart(students, {});
    }

    $('#income-card').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: APP_URL + "/admin/financial/income",
            type: "GET",
            success: function(response){
                var data = {
                    label: 'Receitas',
                    data: response,
                    backgroundColor: 'rgba(16,192,37,0.2)',
                    borderColor: 'rgb(17,192,34)',
                    borderWidth: 1
                };

                year_dash.data.datasets.push(data);
                year_dash.update();
            }
        });
    });

    $('#outcome-card').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: APP_URL + "/admin/financial/outcome",
            type: "GET",
            success: function(response){
                var data = {
                    label: 'Despesas',
                    data: response,
                    backgroundColor: 'rgba(192,50,60,0.2)',
                    borderColor: 'rgb(192,41,38)',
                    borderWidth: 1
                };

                year_dash.data.datasets.push(data);
                year_dash.update();
            }
        });
    });

    $('#registration-card').on('click', function (e) {

        $.ajax({
            url: APP_URL + "/admin/financial/registrations",
            type: "GET",
            success: function(response){
                var data = {
                    label: 'Matrículas',
                    data: response,
                    backgroundColor: 'rgba(185,192,33,0.2)',
                    borderColor: 'rgb(192,169,39)',
                    borderWidth: 1
                };

                year_dash.data.datasets.push(data);
                year_dash.update();
            }
        });
    });

    $('#active-students-card').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: APP_URL + "/admin/financial/active-students",
            type: "GET",
            success: function(response){
                var data = {
                    label: 'Estudantes',
                    data: response,
                    backgroundColor: 'rgba(49,146,192,0.2)',
                    borderColor: 'rgb(60,130,192)',
                    borderWidth: 1
                };

                year_dash.data.datasets.push(data);
                year_dash.update();
            }
        });
    });

    $('#filter-billing').on('click', function() {

        let dates = $('input[name="date_range"]').val();
        let classroom = $('select[name="classroom_id[]"]').val();
        let subsidiaryElement = $('select[name="subsidiary_id"]');
        let subsidiary = subsidiaryElement === undefined ? null : subsidiaryElement.val();

        updateResume(dates, classroom, subsidiary);
        updateExpectedBilling(billing_dash, dates, classroom, subsidiary);
        updateStudentStatus(student_dash, dates, classroom, subsidiary);

    });
});

function lastTwelveMonths() {
    var curDate = new Date();
    var curMonth = curDate.getMonth();
    var curYear = curDate.getFullYear() - 2000;
    var lastYear = curYear - 1;
    var lastMonts = [];

    var months = [
        'Jan',
        'Fev',
        'Mar',
        'Abr',
        'Mai',
        'Jun',
        'Jul',
        'Ago',
        'Set',
        'Out',
        'Nov',
        'Dez'
    ];

    for (var i = curMonth; i > curMonth - 12; i--) {
        lastMonts.push(
            i >= 0 ?
                months[i] + `/${curYear}` :
                months[months.length + i] + `/${lastYear}`
        )
    }

    return lastMonts;
}

function updateResume(dates, classroom, subsidiary = null){

    $.ajax({
        url: APP_URL + "/admin/financial/billing-summary",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function(response){
            $('#expected-incoming').html("R$ " + response.expected_incoming.toFixed(2));
            $('#real-incoming').html("R$ " + response.real_incoming.toFixed(2));
            $('#conversion').html(response.conversion.toFixed(2) + " %");
            $('#medium-ticket').html("R$ " + response.medium_ticket.toFixed(2));
            $('#paid-charges').html(response.paid_charges);
            $('#total-charges').html(response.total_charges);
        }
    });
}

function updateExpectedBilling(pie_dash, dates, classroom, subsidiary = null){

    $.ajax({
        url: APP_URL + "/admin/financial/billing-per-payment-type",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function(response){
            pie_dash.data.datasets.splice(0, 1);

            pie_dash.data.labels = [
                'Boleto',
                'Dinheiro',
                'Cartão de crédito'
            ];
            pie_dash.data.datasets.push({
                data: response,
                backgroundColor: [
                    '#e3ad0b',
                    '#28a745',
                    '#dc3545',
                ]
            });
            pie_dash.update();
        }
    });
}

function updateStudentStatus(student_dash, dates, classroom, subsidiary = null){

    $.ajax({
        url: APP_URL + "/admin/financial/students-by-status",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function(response){
            student_dash.data.datasets.splice(0, 1);

            student_dash.data.labels = [
                'Bolsistas',
                'Regular',
                'Pendentes',
                'Cancelados'
            ];
            student_dash.data.datasets.push({
                data: response,
                backgroundColor: [
                    '#17a2b8',
                    '#28a745',
                    '#e3ad0b',
                    '#dc3545',
                ]
            });
            student_dash.update();
        }
    });
}






