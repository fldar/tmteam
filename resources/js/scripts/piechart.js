function pieChart(el, data) {
    return new Chart(el, {
        type: 'outlabeledPie',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                enabled: false
            },
            plugins: {
                zoomOutPercentage: 55,
                legend: true,
                outlabels: {
                    text: '%v - %p',
                    color: 'white',
                    stretch: 45,
                    font: {
                        resizable: true,
                        minSize: 10,
                        maxSize: 14
                    }
                }
            }
        }
    });
}