$(function () {
    const bands = document.getElementById('chart-student-bands');
    const categories = document.getElementById('chart-age-category');
    const sex = document.getElementById('chart-student-sex');

    if (bands != null) {
        var bands_dash = pieChart(bands, {});
    }

    if (categories != null) {
        var categories_dash = pieChart(categories, {});
    }

    if (sex != null) {
        var sex_dash = pieChart(sex, {});
    }

    $('#filter-academic').on('click', function () {

        let dates = $('input[name="date_range"]').val();
        let classroom = $('select[name="classroom_id[]"]').val();
        let subsidiaryElement = $('select[name="subsidiary_id"]');
        let subsidiary = subsidiaryElement === undefined ? null : subsidiaryElement.val();

        updateStudentsPerSex(sex_dash, dates, classroom, subsidiary);
        updateStudentsPerAgeCategories(categories_dash, dates, classroom, subsidiary);
        updateStudentsPerBands(bands_dash, dates, classroom, subsidiary);
    });
});

function updateStudentsPerSex(sex_dash, dates, classroom, subsidiary = null) {

    $.ajax({
        url: APP_URL + "/admin/academic/students-per-sex",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function (response) {
            sex_dash.data.datasets.splice(0, 1);

            sex_dash.data.labels = ['Masculino', 'Feminino'];
            sex_dash.data.datasets.push({
                data: response,
                backgroundColor: [
                    '#2d4abd',
                    '#e02fdd',
                ]
            });
            sex_dash.update();
        }
    });
}

function updateStudentsPerAgeCategories(categories_dash, dates, classroom, subsidiary = null) {

    $.ajax({
        url: APP_URL + "/admin/academic/students-per-age-categories",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function (response) {

            let labels = Object.keys(response);
            let data = Object.values(response);
            let colors = data.map(() => randomColor());

            categories_dash.data.labels = labels;
            categories_dash.data.datasets.splice(0, 1);
            categories_dash.data.datasets.push({
                data: data,
                backgroundColor: colors
            });

            categories_dash.options.plugins.outlabels.color =
                colors.map((color) => getHexContrast(color));

            categories_dash.update();
        }
    });
}

function updateStudentsPerBands(bands_dash, dates, classroom, subsidiary = null) {

    $.ajax({
        url: APP_URL + "/admin/academic/students-per-bands",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function (response) {

            let labels = Object.keys(response);
            let data = Object.values(response);

            let students = data.map((item) => item.students);
            let colors = data.map((item) => item.color);

            bands_dash.data.labels = labels;

            bands_dash.data.datasets.splice(0, 1);
            bands_dash.data.datasets.push({
                data: students,
                backgroundColor: colors
            });

            bands_dash.options.plugins.outlabels.color =
                colors.map((color) => getHexContrast(color));

            bands_dash.update();
        }
    });
}