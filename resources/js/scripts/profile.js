$(function () {

    $('.birthdate').on('focusout', function () {
        let date = $(this).val().split(/\/|\-/);
        let birthDate = new Date(date[2], date[1], date[0]);
        let otherDate = new Date();
        let years = (otherDate.getFullYear() - birthDate.getFullYear());

        fetchAgeCategory(years);

        if (otherDate.getMonth() < birthDate.getMonth() ||
            otherDate.getMonth() == birthDate.getMonth() &&
            otherDate.getDate() < birthDate.getDate()) {
            years--;
        }

        let form =  $(this).closest('form');
        form.find('.js-age').val(years);

        showResponsible(years, form);
    });
});

function fetchAgeCategory(years) {
    if($('.age-category') !== undefined){
        let option = $('.age-category>option').filter(function () {
            return ($(this).data('age-begin')) <= years &&
                ($(this).data('age-finish')) >= years;
        });

        $('.age-category').selectpicker('val', option.val());
    }
}

function showResponsible(years, form) {
    let field = $(form).find('#guardian-field');

    if(field !== undefined){
        if(years < 18) {
            field.show(); return;
        }
        field.hide();
    }
}