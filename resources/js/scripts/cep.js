$(".cep").on('blur', function()
{
    let value = $(this).val();

    $.get("https://viacep.com.br/ws/"+ value +"/json/", function(data)
    {
        $(".rua").val('');
        $(".bairro").val('');
        $(".cidade").val('');
        $(".uf").val('');

        if (data.sucesso != "0")
        {
            $(".cep").val(value);
            $(".bairro").val(data.bairro);
            $(".complemento").val(data.complemento);
            $(".cidade").val(data.localidade);
            $(".rua").val(data.logradouro);
            $(".uf").val(data.uf);
        }
    }, 'json');
});

$('.numero').change(function() {
    $('.numero').val($(this).val());
});

$('.complemento').change(function() {
    $('.complemento').val($(this).val());
});