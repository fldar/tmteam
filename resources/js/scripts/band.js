var lines = $('.degrees-fields:not(:hidden)');

lines.each(function (index, line) {
    if(index < lines.length -1){
        $(line).find('.action-buttons').hide();
    }

    if(index === $('.degrees-fields').length -1){
        $(line).find('.add-degree').first().parent().hide();
    }
});

$('.add-degree').click(function() {

    var line = $(this).closest('.degrees-fields');
    var idx = parseInt(line.data('index'));
    var lines = $('.degrees-fields');
    var nextLine = $(lines[idx + 1]);

    line.find('.action-buttons').hide();

    if(lines.length === (parseInt(nextLine.data('index')) + 1)){
        nextLine.find('.add-degree').first().parent().hide();
    }

    nextLine.find('.degree-name').prop('disabled', false);
    nextLine.find('.degree-rule').prop('disabled', false);

    nextLine.show();
});

$('.remove-degree').click(function(){

    var line = $(this).closest('.degrees-fields');
    var idx = parseInt(line.data('index'));
    var lines = $('.degrees-fields');

    line.hide();
    $(lines[idx - 1]).find('.action-buttons').show();

    if(line.find('.degree-id').first().val() > 0){
        line.find('.degree-destroy').first().val('1');
    } else {
        line.find('.degree-name').prop('disabled', true);
        line.find('.degree-rule').prop('disabled', true);
    }

});




