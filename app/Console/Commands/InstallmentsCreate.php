<?php

namespace App\Console\Commands;

use App\Core\Services\Financial\MonthlyCharge;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InstallmentsCreate extends Command
{

    protected $service;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'installments:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create next month installments';

    /**
     * Create a new command instance.
     *
     * @param \App\Core\Services\Financial\MonthlyCharge $service
     */
    public function __construct(MonthlyCharge $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $competence = Carbon::now()->addMonth()->format('Y-m');
        $this->service->generate($competence);
    }
}
