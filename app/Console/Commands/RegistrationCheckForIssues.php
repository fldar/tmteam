<?php

namespace App\Console\Commands;

use App\Core\Models\TrainingCenter\Registration;
use App\Core\Services\TrainingCenter\RegistrationService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RegistrationCheckForIssues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registration:check-issues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run system verification for issues on registrations';

    protected $registrationService;

    /**
     * Create a new command instance.
     *
     * @param \App\Core\Services\TrainingCenter\RegistrationService $service
     */
    public function __construct(RegistrationService $service)
    {
        parent::__construct();

        $this->registrationService = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function handle()
    {
        $startMsg = 'Iniciando verificação diária' . PHP_EOL;
        $finishMsg = 'Verificação finalizada com sucesso' . PHP_EOL;

        $this->info($startMsg);
        Log::channel('system')->info($startMsg);

        $registrations = Registration::all();

        $bar = $this->output->createProgressBar(count($registrations));

        foreach ($registrations as $registration){
            $this->registrationService->checkForIssues($registration);
            $bar->advance();
        }

        $bar->finish();

        $this->info(PHP_EOL . $finishMsg);
        Log::channel('system')->info($finishMsg);
    }
}
