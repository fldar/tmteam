<?php

namespace App\Core\Models\Auth;

use App\Core\Models\Concerns\Filterable;
use Spatie\Permission\Models\Permission as SpatiePermission;

/**
 * Class Permission
 *
 * @package App\Core\Models\Auth
 * @property string $name
 * @property string $details
 * @property string $guard_name
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Auth\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Permission permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Permission role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Permission extends SpatiePermission
{

    use Filterable;

    protected $fillable = [
        'name',
        'details',
        'guard_name'
    ];
}
