<?php


namespace App\Core\Models\TrainingCenter;

use App\Core\Models\Concerns\SubsidiaryScope;
use App\Core\Models\Subsidiary;
use App\Enums\PaymentTypesEnum;
use App\Enums\RegistrationStatusEnum;
use App\Events\RegistrationHasBeenCanceled;
use App\Core\Models\Financial\BalanceRecord;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * App\Core\Models\TrainingCenter\Registration
 *
 * @property int $id
 * @property string $code
 * @property \Illuminate\Support\Carbon $effective_date
 * @property float $acquisition
 * @property float $amount
 * @property float $advance_discount
 * @property int $payment_type
 * @property int $payment_day
 * @property int $status
 * @property int $student_id
 * @property \Illuminate\Support\Carbon|null $skip_checking_from
 * @property \Illuminate\Support\Carbon|null $skip_checking_until
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $subsidiary_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Classroom[] $classrooms
 * @property-read int|null $classrooms_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Frequency[] $frequencies
 * @property-read int|null $frequencies_count
 * @property-read mixed $financier
 * @property-read mixed $fisic
 * @property-read mixed $fisic_expiration
 * @property-read mixed $fisic_validation_status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Graduation[] $graduations
 * @property-read int|null $graduations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Financial\BalanceRecord[] $installments
 * @property-read int|null $installments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\RegistrationIssue[] $issues
 * @property-read int|null $issues_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \App\Core\Models\TrainingCenter\Student $student
 * @property-read \App\Core\Models\Subsidiary $subsidiary
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration currentMonth()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration frequenciesOfWeek()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration lastSixInstallments()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration notCancelled()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereAcquisition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereAdvanceDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereEffectiveDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration wherePaymentDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereSkipCheckingFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereSkipCheckingUntil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereSubsidiaryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Registration whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Registration extends Model implements AuditableInterface, HasMedia
{
    use AuditableTrait, HasMediaTrait;

    protected $fillable = [
        'code',
        'effective_date',
        'acquisition',
        'amount',
        'advance_discount',
        'payment_type',
        'payment_day',
        'status',
        'student_id',
        'subsidiary_id',
        'skip_checking_from',
        'skip_checking_until',
    ];

    protected $dates = [
        'effective_date',
        'created_at',
        'skip_checking_from',
        'skip_checking_until'
    ];

    protected $auditInclude = [
        'code',
        'effective_date',
        'acquisition',
        'amount',
        'advance_discount',
        'payment_type',
        'payment_day',
        'status',
        'student_id'
    ];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);

        static::creating(function ($model) {
            $model->configs();
        });
    }

    protected function configs()
    {
        $this->attributes['code'] = $this->student->code;
        $this->attributes['status'] = $this->type == 2 ? 2 : 1;
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('fisic')
             ->singleFile();
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

    public function classrooms()
    {
        return $this->belongsToMany(Classroom::class,
            'registrations_classrooms',
            'registration_id',
            'classroom_id'
        );
    }

    public function graduations()
    {
        return $this->hasMany(Graduation::class, 'registration_id');
    }

    public function frequencies()
    {
        return $this->hasMany(Frequency::class, 'registration_id');
    }

    public function installments()
    {
        return $this->morphMany(BalanceRecord::class, 'chargeable');
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'subsidiary_id');
    }

    public function issues()
    {
        return $this->hasMany(RegistrationIssue::class, 'registration_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeLastSixInstallments()
    {
        return $this->installments()
                    ->orderBy('due_date', 'DESC')
                    ->take(6);
    }

    public function scopeFrequenciesOfWeek()
    {
        $monday = Carbon::now()->startOfWeek()->format('Y-m-d');
        $sunday = Carbon::now()->endOfWeek()->format('Y-m-d');

        return $this->frequencies()
                    ->whereBetween('date', [$monday, $sunday])
                    ->whereReviewed(true)
                    ->orderBy('date', 'DESC');
    }

    public function scopeCurrentMonth($query)
    {
        return $query->where('effective_date', 'like', date('Y-m') . "%");
    }

    public function scopeNotCancelled($query)
    {
        return $query->where('status', '<>', RegistrationStatusEnum::CANCELLED)
                     ->where('payment_type', '<>', PaymentTypesEnum::SCHOLARSHIP);
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    public function setStatusAttribute($value)
    {
        if ($value == RegistrationStatusEnum::CANCELLED) {
            event(new RegistrationHasBeenCanceled($this));
        }

        $this->attributes['status'] = $value;
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getFinancierAttribute()
    {
        return $this->student->guardian ?? $this->student;
    }

    public function getFisicAttribute()
    {
        return $this->getFirstMedia('fisic');
    }

    public function getFisicValidationStatusAttribute()
    {
        $media = $this->getFirstMedia('fisic');

        return $media ? $media->getCustomProperty('validation') : null;
    }

    public function getFisicExpirationAttribute()
    {
        $media = $this->getFirstMedia('fisic');

        return $media ? $media->created_at->addYear()->diffForHumans() : null;
    }

    /*
    |--------------------------------------------------------------------------
    | Class methods
    |--------------------------------------------------------------------------
    */

    public function dontCheck()
    {
        $from = $this->skip_checking_from;
        $until = $this->skip_checking_until;

        if (empty($from) || empty($until)) {
            return false;
        }

        return Carbon::now()->between($from, $until);
    }

    public function currentGraduation($modalityId = null)
    {
        $query = $this->graduations();

        if($modalityId){
            $query->where('modality_id', $modalityId);
        }

        return $query->latest('graduated_on')->first();
    }

}
