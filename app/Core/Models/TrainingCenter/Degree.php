<?php

namespace App\Core\Models\TrainingCenter;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Degree
 *
 * @package App\Core\Models\TrainingCenter
 * @property string $name
 * @property int $band_id
 * @property Band $band
 * @property string $lessons
 * @property string $minimum_period
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Graduation[] $graduations
 * @property-read int|null $graduations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree whereBandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree whereLessons($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree whereMinimumPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Degree whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Degree extends Model
{

    protected $fillable = [
        'name',
        'band_id',
        'lessons',
        'minimum_period'
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function band()
    {
        return $this->belongsTo(Band::class, 'band_id');
    }

    public function graduations()
    {
        return $this->hasMany(Graduation::class, 'degree_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Instance methods
    |--------------------------------------------------------------------------
    */

    public function next()
    {
        return $this->band->degrees()
                          ->where('id', '>', $this->id)
                          ->orderBy('id', 'ASC')
                          ->first();
    }

}
