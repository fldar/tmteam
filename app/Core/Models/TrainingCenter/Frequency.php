<?php

namespace App\Core\Models\TrainingCenter;

use App\Core\Models\Concerns\Filterable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * Class RegistrationFrequency
 *
 * @package App\Core\Models\TrainingCenter
 * @property int $id
 * @property int $registration_id
 * @property Registration $registration
 * @property int $graduation_id
 * @property Graduation $graduation
 * @property \DateTime $date
 * @property boolean $reviewed
 * @property string $remark
 * @property int $classroom_id
 * @property Classroom $classroom
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $send_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency whereGraduationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency whereRegistrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency whereReviewed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Frequency whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Frequency extends Model implements AuditableInterface
{
    use AuditableTrait, Filterable;

    protected $table = 'registrations_frequencies';

    protected $fillable = [
        'classroom_id',
        'registration_id',
        'graduation_id',
        'date',
        'reviewed',
        'remark'
    ];

    protected $casts = [
        'reviewed' => 'boolean',
        'date'     => 'datetime'
    ];

    protected $auditInclude = [
        'classroom_id',
        'registration_id',
        'date',
        'reviewed',
        'remark'
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function registration()
    {
        return $this->belongsTo(Registration::class, 'registration_id');
    }

    public function classroom()
    {
        return $this->belongsTo(Classroom::class, 'classroom_id');
    }

    public function graduation()
    {
        return $this->belongsTo(Graduation::class, 'graduation_id');
    }

    /*
    |--------------------------------------------------------------------------
    |  Accessors
    |--------------------------------------------------------------------------
    */
    public function getSendDateAttribute()
    {
        return $this->date ?
            $this->date->format('d/m/Y H:i') :
            $this->created_at->format('d/m/Y H:i');
    }
}
