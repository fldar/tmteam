<?php

namespace App\Core\Models\TrainingCenter;

use App\Core\Models\Concerns\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;

/**
 * Class Modality
 *
 * @package App\Core\Models\TrainingCenter
 * @property string $name
 * @property string $price
 * @property boolean $active_graduation
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Band[] $bands
 * @property-read int|null $bands_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Classroom[] $classroom
 * @property-read int|null $classroom_count
 * @property-read mixed $discount_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Teacher[] $teachers
 * @property-read int|null $teachers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality asc()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality whereActiveGraduation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $subsidiary_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Modality whereSubsidiaryId($value)
 */
class Modality extends Model implements AuditableInterface
{
    use AuditableTrait, Filterable;

    protected $table = 'modalities';

    protected $fillable = [
        'name',
        'price',
        'active_graduation',
    ];

    protected $casts = [
        'active_graduation' => 'boolean'
    ];

    protected $auditInclude = [
        'name',
        'price',
        'active_graduation',
    ];

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */
    public function scopeAsc($query)
    {
        return $query->orderBy('name', 'ASC');
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function bands()
    {
        return $this->hasMany(Band::class, 'modality_id');
    }

    public function classroom()
    {
        return $this->hasMany(Classroom::class, 'modality_id');
    }

    public function teachers()
    {
        return $this->belongsToMany(Teacher::class,
            'teachers_modalities',
            'modality_id',
            'teacher_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */
    public function setNameAttribute($value)
    {
        if (empty($value)) return;

        $this->attributes[ 'name' ] = Str::title($value);
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */
    public function getdiscountNameAttribute()
    {
        return $this->discount ? "{$this->discount} %" : '-';
    }

    /*
    |--------------------------------------------------------------------------
    | Class methods
    |--------------------------------------------------------------------------
    */
    public static function options()
    {
        return self::select()->asc()->pluck('name', 'id');
    }
}
