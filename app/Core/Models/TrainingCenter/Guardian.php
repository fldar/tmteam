<?php

namespace App\Core\Models\TrainingCenter;

use App\Core\Models\Auth\User;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Person;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Guardian
 *
 * @package App\Core\Models\TrainingCenter
 * @property int $user_id
 * @property User $user
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $photo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Student[] $students
 * @property-read int|null $students_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian byActive()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian byAsc()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian wherePersonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Guardian whereUserId($value)
 * @mixin \Eloquent
 */
class Guardian extends Model implements AuditableInterface
{
    use AuditableTrait, Filterable;

    protected $table = 'guardians';

    protected $fillable = [
        'user_id'
    ];

    protected $auditInclude = [
        'user_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */
    public function scopebyAsc($query)
    {
        return $query->orderBy('first_name', 'ASC');
    }

    public function scopebyActive($query)
    {
        return $query->where('active', true);
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'guardian_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    |  Accessors
    |--------------------------------------------------------------------------
    */
    public function getPhotoAttribute()
    {
        $image = $this->getMedia('user')->first();
        return isset($image) ? $image->getUrl('photo') : asset('img/avatar-default.png');
    }
}
