<?php

namespace App\Core\Models\TrainingCenter;

use App\Events\StudentIsNowQualified;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;

/**
 * Class Graduation
 *
 * @package App\Core\Models\TrainingCenter
 * @property int $registration_id
 * @property Registration $registration
 * @property int $teacher_id
 * @property Teacher $teacher
 * @property int $band_id
 * @property Band $band
 * @property int $modality_id
 * @property Modality $modality
 * @property int $degree_id
 * @property Degree $degree
 * @property int $lessons_taken
 * @property \DateTime $graduated_on
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereBandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereDegreeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereGraduatedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereLessonsTaken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereModalityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereRegistrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Graduation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Graduation extends Model implements AuditableInterface
{
    use AuditableTrait;

    protected $fillable = [
        'registration_id',
        'teacher_id',
        'band_id',
        'modality_id',
        'degree_id',
        'lessons_taken',
        'graduated_on'
    ];

    protected $auditInclude = [
        'registration_id',
        'teacher_id',
        'band_id',
        'classroom_id',
        'degree_id',
        'lessons_taken',
        'graduated_on'
    ];

    protected $casts = [
        'graduated_on' => 'date'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            if ($model->qualified()) {
                event(new StudentIsNowQualified($model->registration, $model));
            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function registration()
    {
        return $this->belongsTo(Registration::class, 'registration_id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id');
    }

    public function modality()
    {
        return $this->belongsTo(Modality::class, 'modality_id');
    }

    public function band()
    {
        return $this->belongsTo(Band::class, 'band_id');
    }

    public function degree()
    {
        return $this->belongsTo(Degree::class, 'degree_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Instance methods
    |--------------------------------------------------------------------------
    */

    public function nextDegree()
    {
        return $this->degree->next() ??
            $this->band->next->degrees()->first() ??
            null;
    }

    /**
     * @return bool
     */
    public function qualified()
    {
        return !empty($this->graduated_on) &&
            $this->is($this->registration->currentGraduation()) &&
            $this->lessons_taken >= $this->degree->lessons &&
            $this->graduated_on->diffInDays(Carbon::now()) >= $this->degree->minimum_period;
    }
}
