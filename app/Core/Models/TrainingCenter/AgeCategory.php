<?php

namespace App\Core\Models\TrainingCenter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;

/**
 * Class AgeCategory
 *
 * @package App\Core\Models\TrainingCenter
 * @property string $name
 * @property int $age_finish
 * @property int $age_begin
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $subsidiary_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Band[] $bands
 * @property-read int|null $bands_count
 * @property-read mixed $age_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Student[] $students
 * @property-read int|null $students_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory asc()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory whereAgeBegin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory whereAgeFinish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\AgeCategory whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class AgeCategory extends Model implements AuditableInterface
{
    use AuditableTrait;

    protected $table = 'ages_categories';

    protected $fillable = [
        'name',
        'age_finish',
        'age_begin',
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    protected $auditInclude = [
        'name',
        'age_finish',
        'age_begin',
    ];

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */
    public function scopeAsc($query)
    {
        return $query->orderBy('name', 'ASC');
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function bands()
    {
        return $this->belongsToMany(Band::class,
            'bands_ages_categories',
            'age_category_id',
            'band_id'
        );
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'age_category_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */
    public function setNameAttribute($value)
    {
        if (empty($value)) return;

        $this->attributes[ 'name' ] = Str::title($value);
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */
    public function getAgeNameAttribute()
    {
        return "{$this->age_begin} anos até {$this->age_finish}";
    }
}
