<?php

namespace App\Core\Models\TrainingCenter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;

/**
 * Class Band
 *
 * @package App\Core\Models\TrainingCenter
 * @property string $name
 * @property string $color
 * @property int $modality_id
 * @property Modality $modality
 * @property int $next_id
 * @property Band $next
 * @property int $id
 * @property int $age_category_id
 * @property int $age_finish
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\AgeCategory[] $ageCategories
 * @property-read int|null $age_categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Degree[] $degrees
 * @property-read int|null $degrees_count
 * @property-read mixed $color_class
 * @property-read mixed $color_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Registration[] $registration
 * @property-read int|null $registration_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band asc()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band whereAgeCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band whereAgeFinish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band whereModalityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band whereNextId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Band whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Band extends Model implements AuditableInterface
{
    use AuditableTrait;

    protected $fillable = [
        'name',
        'color',
        'modality_id',
        'next_id',
    ];

    protected $auditInclude = [
        'name',
        'color',
        'modality_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeAsc($query)
    {
        return $query->orderBy('name', 'ASC');
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function registration()
    {
        return $this->belongsToMany(Registration::class,
            'registrations_bands',
            'band_id',
            'registration_id'
        )->withPivot(['graduation']);
    }

    public function ageCategories()
    {
        return $this->belongsToMany(AgeCategory::class,
            'bands_ages_categories',
            'band_id',
            'age_category_id'
        );
    }

    public function modality()
    {
        return $this->belongsTo(Modality::class, 'modality_id');
    }

    public function degrees()
    {
        return $this->hasMany(Degree::class, 'band_id');
    }

    public function next()
    {
        return $this->hasOne(Band::class, 'next_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    public function setNameAttribute($value)
    {
        if (empty($value)) return;

        $this->attributes['name'] = Str::title($value);
    }

    /*
    |--------------------------------------------------------------------------
    | Class methods
    |--------------------------------------------------------------------------
    */
    public static function options()
    {
        return self::select()->asc()->pluck('name', 'id');
    }
}
