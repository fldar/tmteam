<?php


namespace App\Core\Models\Report;


use Illuminate\Database\Eloquent\Collection;

class StudentsPerSexCollection extends Collection
{

    public function resume() : \Illuminate\Support\Collection
    {
        return $this->unique('registration_id')
                    ->groupBy('sex')->map(function ($item, &$key){
            return $key = $item->count();
        });
    }
}