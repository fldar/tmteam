<?php

namespace App\Core\Models\Report;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\Core\Models\Report\Balance
 *
 * @property float|null $total_charged
 * @property float|null $total_received
 * @property float|null $total_charge_discount
 * @property float|null $total_charge_fees
 * @property float|null $total_spent
 * @property float|null $total_paid
 * @property float|null $total_spent_discount
 * @property float|null $total_spent_fees
 * @property int|null $competence_month
 * @property int|null $competence_year
 * @property int $subsidiary_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance currentMonth()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance lastYear()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereCompetenceMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereCompetenceYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereSubsidiaryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereTotalChargeDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereTotalChargeFees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereTotalCharged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereTotalPaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereTotalReceived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereTotalSpent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereTotalSpentDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Balance whereTotalSpentFees($value)
 * @mixin \Eloquent
 */
class Balance extends Model
{

    use Filterable;

    protected $table = 'balance_report';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('subsidiary', function (Builder $builder) {
            $user = Auth::user();
            $builder->where('subsidiary_id', $user->subsidiary_id);
        });

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderByDesc('competence_year')
                    ->orderByDesc('competence_month');
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeLastYear(Builder $query)
    {
        $end   = Carbon::now()->day(1)->format('Y-m-d');
        $begin = Carbon::now()->day(1)->subYear(1)->format('Y-m-d');

        return $query->whereRaw(
            "STR_TO_DATE(CONCAT(competence_year, '-', competence_month, '-', '1'), '%Y-%m-%d') <= '$end'"
        )->whereRaw(
            "STR_TO_DATE(CONCAT(competence_year, '-', competence_month, '-', '1'), '%Y-%m-%d') >= '$begin'"
        )->take(12);
    }

    public function scopeCurrentMonth($query)
    {
        return $query->where([
            'competence_month' => date('m'),
            'competence_year'  => date('Y')
        ]);
    }

}
