<?php

namespace App\Core\Models\Report;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\Overdue
 *
 * @property string $student_name
 * @property string $student_email
 * @property string|null $overdues
 * @property int $subsidiary_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Overdue filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Overdue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Overdue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Overdue query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Overdue whereOverdues($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Overdue whereStudentEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Overdue whereStudentName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Overdue whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class Overdue extends Model
{

    use Filterable;

    protected $table = 'overdue_report';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }
}
