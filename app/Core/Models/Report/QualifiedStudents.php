<?php


namespace App\Core\Models\Report;

use App\Core\Models\Concerns\SubsidiaryScope;
use App\Core\Models\TrainingCenter\Registration;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\QualifiedStudents
 *
 * @property int $id
 * @property int $registration_id
 * @property string $name
 * @property string $nickname
 * @property string $modality
 * @property string $band
 * @property string $degree
 * @property string $lessons
 * @property \Illuminate\Support\Carbon $graduated_on
 * @property int $subsidiary_id
 * @property-read \App\Core\Models\TrainingCenter\Registration $registration
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereBand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereDegree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereGraduatedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereLessons($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereModality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereRegistrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\QualifiedStudents whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class QualifiedStudents extends Model
{
    protected $table = 'qualified_students_report';

    protected $casts = [
        'graduated_on' => 'date'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function registration()
    {
        return $this->belongsTo(Registration::class, 'registration_id');
    }

}