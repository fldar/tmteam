<?php


namespace App\Core\Models\Report;


use Illuminate\Database\Eloquent\Collection;

class BillingPerPaymentTypeCollection extends Collection
{

    public function resume()
    {
        return $this->groupBy('payment_type')->map(function ($item, &$key){
            return $key = $item->sum('expected_value');
        });
    }
}