<?php


namespace App\Core\Models\Report;


use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\BillingPerClass
 *
 * @property int $id
 * @property string $classroom
 * @property float|null $expected
 * @property float|null $received
 * @property float|null $discount
 * @property float|null $charge_fee
 * @property int|null $competence_month
 * @property int|null $competence_year
 * @property int $subsidiary_id
 * @property-read mixed $pendent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass whereChargeFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass whereClassroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass whereCompetenceMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass whereCompetenceYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass whereExpected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass whereReceived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerClass whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class BillingPerClass extends Model
{

    use Filterable;

    protected $table = 'billing_per_class_report';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getPendentAttribute()
    {
        $result = $this->expected - ($this->received + $this->discount + $this->charge_fee);

        return $result > 0 ? $result : 0;
    }
}