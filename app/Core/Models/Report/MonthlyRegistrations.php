<?php

namespace App\Core\Models\Report;

use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\MonthlyRegistrations
 *
 * @property int $registries
 * @property int|null $competence_month
 * @property int|null $competence_year
 * @property int $subsidiary_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\MonthlyRegistrations newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\MonthlyRegistrations newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\MonthlyRegistrations query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\MonthlyRegistrations whereCompetenceMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\MonthlyRegistrations whereCompetenceYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\MonthlyRegistrations whereRegistries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\MonthlyRegistrations whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class MonthlyRegistrations extends Model
{
    protected $table = 'monthly_registrations_report';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }
}
