<?php


namespace App\Core\Models\Report;


use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\StudentsPerBands
 *
 * @property string|null $date
 * @property int $registration_id
 * @property int $classroom_id
 * @property int $graduation_id
 * @property int $band_id
 * @property string $band
 * @property string|null $color
 * @property int $subsidiary_id
 * @method static \App\Core\Models\Report\StudentsPerBandsCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands filter(\App\Core\Filters\Filter $filter)
 * @method static \App\Core\Models\Report\StudentsPerBandsCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands whereBand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands whereBandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands whereGraduationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands whereRegistrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerBands whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class StudentsPerBands extends Model
{

    use Filterable;

    protected $table = 'students_per_bands_report';

    public function newCollection(array $models = [])
    {
        return new StudentsPerBandsCollection($models);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }

}