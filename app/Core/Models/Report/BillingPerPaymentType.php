<?php


namespace App\Core\Models\Report;


use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\BillingPerPaymentType
 *
 * @property string $date
 * @property float|null $expected_value
 * @property int $payment_type
 * @property int $classroom_id
 * @property string $classroom
 * @property int $subsidiary_id
 * @method static \App\Core\Models\Report\BillingPerPaymentTypeCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType filter(\App\Core\Filters\Filter $filter)
 * @method static \App\Core\Models\Report\BillingPerPaymentTypeCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType whereClassroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType whereExpectedValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingPerPaymentType whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class BillingPerPaymentType extends Model
{

    use Filterable;

    protected $table = 'expected_billing_per_payment_type_report';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }

    /**
     * @inheritdoc
     */
    public function newCollection(array $models = [])
    {
        return new BillingPerPaymentTypeCollection($models);
    }
}