<?php


namespace App\Core\Models\Report;


use App\Enums\RegistrationStatusEnum;
use Illuminate\Database\Eloquent\Collection;

class StudentsPerStatusCollection extends Collection
{

    public function resume() : \Illuminate\Support\Collection
    {
        return $this->groupBy('status')->map(function ($item, &$key){
            return $key = $item->count();
        });
    }

}