<?php


namespace App\Core\Models\Report;


use Illuminate\Database\Eloquent\Collection;

class StudentsPerBandsCollection extends Collection
{

    public function resume()
    {
        return $this->groupBy('registration_id')
                    ->map(function (Collection $item) {
                        return $item->sortBy('date')->last();
                    })->groupBy('band')
                    ->map(function ($item, &$key) {
                        return $key = [
                            'students' => $item->count(),
                            'color'    => $item->first()->color
                        ];
                    });
    }

}