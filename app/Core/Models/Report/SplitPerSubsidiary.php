<?php

namespace App\Core\Models\Report;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\SplitPerSubsidiary
 *
 * @property float|null $expected
 * @property float|null $received
 * @property int|null $competence_month
 * @property int|null $competence_year
 * @property int $subsidiary_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary currentMonth()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary whereCompetenceMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary whereCompetenceYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary whereExpected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary whereReceived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\SplitPerSubsidiary whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class SplitPerSubsidiary extends Model
{

    use Filterable;

    protected $table = 'split_per_subsidiary_report';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderByDesc('competence_month')
                    ->orderByDesc('competence_year');
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeCurrentMonth($query)
    {
        return $query->where([
            'competence_month' => date('m'),
            'competence_year'  => date('Y')
        ]);
    }

}
