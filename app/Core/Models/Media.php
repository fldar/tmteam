<?php

namespace App\Core\Models;

use App\Events\MediaHasBeenValidated;
use Spatie\MediaLibrary\Models\Media as BaseMedia;

/**
 * Class Media
 *
 * @package App\Core\Models
 * @property string $human_readable_type
 * @property string $validation_status
 * @property int $id
 * @property string $model_type
 * @property int $model_id
 * @property string $collection_name
 * @property string $name
 * @property string $file_name
 * @property string|null $mime_type
 * @property string $disk
 * @property int $size
 * @property array $manipulations
 * @property array $custom_properties
 * @property array $responsive_images
 * @property int|null $order_column
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $extension
 * @property-read string $human_readable_size
 * @property-read string $type
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $model
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\MediaLibrary\Models\Media ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereCollectionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereCustomProperties($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereDisk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereManipulations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereModelType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereOrderColumn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereResponsiveImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Media whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Media extends BaseMedia
{
    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */
    public function getHumanReadableTypeAttribute()
    {
        switch ($this->collection_name) {
            case 'fisic':
                return 'atestado médico';
            case 'avatar':
                return 'avatar';
            default :
                return 'anexo';
        }
    }

    public function getValidationStatusAttribute()
    {
        return $this->getCustomProperty('validation');
    }

    /*
    |--------------------------------------------------------------------------
    | Class methods
    |--------------------------------------------------------------------------
    */

    /**
     * Validates attached media
     *
     * @param $validationStatus
     */
    public function validateAttachment($validationStatus)
    {
        $this->setCustomProperty('validation', $validationStatus);

        event(new MediaHasBeenValidated($this));
    }
}
