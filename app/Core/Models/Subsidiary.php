<?php

namespace App\Core\Models;

use App\Core\Models\Auth\User;
use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\TrainingCenter\Registration;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;


/**
 * App\Core\Models\Subsidiary
 *
 * @property int $id
 * @property string $name
 * @property string|null $fantasy_name
 * @property string|null $cnpj
 * @property string|null $company_type
 * @property string|null $business_area
 * @property string|null $lines_of_business
 * @property string|null $bank_number
 * @property string|null $agency_number
 * @property string|null $account_number
 * @property string|null $account_complement_number
 * @property string|null $account_type
 * @property string|null $account_holder_name
 * @property string|null $account_holder_document
 * @property string $phone
 * @property string $email
 * @property string $cep
 * @property string $street
 * @property string $number
 * @property string $neighborhood
 * @property string $city
 * @property string $state
 * @property string|null $complement
 * @property int $manager_id
 * @property string|null $gateway_id
 * @property string|null $gateway_status
 * @property string|null $resource_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Financial\BalanceRecord[] $balanceRecords
 * @property-read int|null $balance_records_count
 * @property-read \App\Core\Models\Auth\User $manager
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Registration[] $registrations
 * @property-read int|null $registrations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Auth\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereAccountComplementNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereAccountHolderDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereAccountHolderName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereAccountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereAgencyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereBankNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereBusinessArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereCep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereCnpj($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereCompanyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereComplement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereFantasyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereGatewayId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereLinesOfBusiness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereNeighborhood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereResourceToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Subsidiary whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Subsidiary extends Model implements AuditableInterface
{

    use AuditableTrait, Filterable;

    protected $fillable = [
        'name',
        'fantasy_name',
        'cnpj',
        'company_type',
        'business_area',
        'lines_of_business',
        'manager_id',
        'phone',
        'email',
        'cep',
        'street',
        'neighborhood',
        'number',
        'city',
        'state',
        'bank_number',
        'agency_number',
        'account_number',
        'account_complement_number',
        'account_type',
        'account_holder_name',
        'account_holder_document',
        'resource_token'
    ];

    protected $auditInclude = [
        'name',
        'cnpj',
        'company_type',
        'business_area',
        'lines_of_business',
        'manager_id',
        'phone',
        'email',
        'cep',
        'street',
        'neighborhood',
        'number',
        'city',
        'state',
        'bank_number',
        'agency_number',
        'account_number',
        'account_complement_number',
        'account_type',
        'account_type',
        'account_holder_name',
        'account_holder_document',
        'resource_token'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'subsidiary_id');
    }

    public function registrations()
    {
        return $this->hasMany(Registration::class, 'subsidiary_id');
    }

    public function balanceRecords()
    {
        return $this->hasMany(BalanceRecord::class, 'subsidiary_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    public function setNameAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['name'] = Str::title($value);
    }

    public function setCnpjAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['cnpj'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setEmailAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['email'] = Str::lower($value);
    }

    public function setPhoneAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['phone'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setAccountHolderDocumentAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['account_holder_document'] = preg_replace('/[^0-9]/', '', $value);
    }
}
