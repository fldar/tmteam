<?php

namespace App\Core\Models;

use Illuminate\Notifications\DatabaseNotification;

/**
 * Class Notification
 *
 * @package App\Core\Models
 * @property array $data
 * @property-read \DateTime $read_at
 * @property string $id
 * @property string $type
 * @property string $notifiable_type
 * @property int $notifiable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $notifiable
 * @method static \Illuminate\Notifications\DatabaseNotificationCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Notifications\DatabaseNotificationCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification whereNotifiableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification whereNotifiableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification whereReadAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Notification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Notification extends DatabaseNotification
{
}
