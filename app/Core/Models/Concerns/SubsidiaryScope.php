<?php


namespace App\Core\Models\Concerns;

use App\Core\Models\Subsidiary;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class SubsidiaryScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
       $user = Auth::user();
       $column = $this->columnName($model);

        if($user && $user->cannot('view_subsidiaries')){
            $builder->where($column, $user->subsidiary_id);
        }
    }

    private function columnName(Model $model)
    {
        return $model instanceof Subsidiary ? 'id' : 'subsidiary_id';
    }
}