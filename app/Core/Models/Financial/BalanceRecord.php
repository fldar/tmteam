<?php

namespace App\Core\Models\Financial;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use App\Core\Models\Subsidiary;
use App\Enums\BalanceRecordStatusEnum;
use App\Enums\BalanceRecordTypeEnum;
use App\Enums\PaymentTypesEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * App\Core\Models\Financial\BalanceRecord
 *
 * @property int $id
 * @property string $description
 * @property int $type
 * @property float $amount
 * @property float|null $amount_paid
 * @property float|null $discount_amount
 * @property int $discount_days
 * @property \Illuminate\Support\Carbon $due_date
 * @property \Illuminate\Support\Carbon|null $payment_date
 * @property int $payment_type
 * @property bool $canceled
 * @property string|null $charge_code
 * @property string|null $charge_link
 * @property float|null $charge_fee
 * @property string|null $chargeable_type
 * @property int|null $chargeable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $subsidiary_id
 * @property string|null $gateway_id
 * @property float $split_amount
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $chargeable
 * @property-read \App\Core\Models\Subsidiary $subsidiary
 * @property-read mixed $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord inTime()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord invoice()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereAmountPaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereCanceled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereChargeCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereChargeFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereChargeLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereChargeableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereChargeableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereDiscountAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereDiscountDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereGatewayId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord wherePaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereSplitAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereSubsidiaryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Financial\BalanceRecord whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BalanceRecord extends Model implements AuditableInterface
{
    use AuditableTrait, Filterable;

    protected $fillable = [
        'description',
        'type',
        'amount',
        'amount_paid',
        'discount_amount',
        'discount_days',
        'due_date',
        'payment_date',
        'payment_type',
        'canceled',
        'charge_code',
        'charge_link',
        'charge_fee',
        'subsidiary_id',
        'gateway_id'
    ];

    protected $casts = [
        'due_date'     => 'date',
        'payment_date' => 'date',
        'canceled'     => 'boolean'
    ];

    protected $auditInclude = [
        'description',
        'type',
        'amount',
        'amount_paid',
        'discount_amount',
        'discount_days',
        'due_date',
        'chargeable_type',
        'chargeable_id',
        'payment_date',
        'payment_type',
        'canceled',
        'charge_code',
        'charge_link',
        'charge_fee',
        'subsidiary_id',
        'gateway_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);

        static::creating(function($model){
            if(empty($model->subsidiary_id)){
                $model->subsidiary_id = Auth::user()->subsidiary_id;
            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function chargeable()
    {
        return $this->morphTo();
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'subsidiary_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeInvoice($query)
    {
        return $query->where([
            'type'         => BalanceRecordTypeEnum::RECIPE,
            'payment_type' => PaymentTypesEnum::INVOICE
        ])->whereNotNull('chargeable_id');
    }

    public function scopeInTime($query)
    {
        return $query->where('due_date', '>=', date('Y-m-d', time()));
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getStatusAttribute()
    {
        if (!empty($this->canceled)) {
            return BalanceRecordStatusEnum::CANCELED;
        }

        if (!empty($this->payment_date)) {
            return BalanceRecordStatusEnum::PAID;
        }

        if (!empty($this->charge_code)) {
            return BalanceRecordStatusEnum::BILLED;
        }

        $today = Carbon::now()->setTime(0, 0, 0);

        if ($this->due_date < $today && empty($this->payment_date)) {
            return BalanceRecordStatusEnum::UNPAID;
        }

        return BalanceRecordStatusEnum::CREATED;
    }

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public function paid()
    {
        return $this->status === BalanceRecordStatusEnum::PAID;
    }

    public function isInvoice()
    {
        return $this->chargeable_id !== null;
    }
}
