<?php


namespace App\Core\Filters;


use Illuminate\Database\Eloquent\Builder;

class OriginFilter extends Filter
{

    /**
     * Filter the student records by the given name value.
     *
     * @param string $value
     * @return Builder
     */
    public function name($value)
    {
        return $this->builder->where('name', 'like', "%{$value}%");
    }
}