<?php


namespace App\Core\Filters;


use Illuminate\Database\Eloquent\Builder;

class SplitReportFilter extends Filter
{

    /**
     * Default filters data.
     *
     * @return array
     */
    public function defaultFilters(): array
    {
        return [
            'competence_year' => date('Y', time())
        ];
    }

    /**
     * Filter the billing per class records by the given competence month value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function competence(string $value)
    {
        [$year, $month] = explode('-', $value);

        return $this->builder->where('competence_month', $month)
                             ->where('competence_year', $year);
    }

    /**
     * Filter the user records by the given subsidiary id value.
     *
     * @param string $value
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->builder->where('subsidiary_id', $value);
    }
}