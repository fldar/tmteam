<?php


namespace App\Core\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class BalanceFilter extends Filter
{

    /**
     * Default filters data.
     *
     * @return array
     */
    public function defaultFilters(): array
    {
        return [
            'competence_year' => date('Y', time()),
            'subsidiary_id' => Auth::user()->subsidiary_id
        ];
    }

    /**
     * Filter the billing per class records by the given competence year and month value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function competence(string $value)
    {
        [$year, $month] = explode('-', $value);

        return $this->builder->where('competence_month', $month)
                             ->where('competence_year', $year);
    }

    /**
     * Filter the billing per class records by the given competence year value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function competenceYear(string $value)
    {
        return $this->builder->where('competence_year', $value);
    }

    /**
     * Filter the user records by the given subsidiary id value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->builder->where('subsidiary_id', $value);
    }
}
