<?php


namespace App\Core\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

abstract class Filter
{
    protected Request $request;

    protected Builder $builder;

    /**
     * Initialize a new filter instance.
     *
     * @param Request $request
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply the filters to the builder.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        $filters       = empty($this->filters()) ? $this->defaultFilters() : $this->filters();

        foreach ($filters as $name => $value) {

            $method = Str::camel($name);

            if (!method_exists($this, $method) || blank($value)) {
                continue;
            }

            $this->$method($value);
        }

        return $this->builder;
    }

    /**
     * Get all request filters data.
     *
     * @return array
     */
    public function filters(): array
    {
        return $this->request->all();
    }

    /**
     * Default filters data.
     *
     * @return array
     */
    public function defaultFilters(): array
    {
        return [];
    }

    /**
     * Merge filters on request before they are applied
     *
     * @param array $filters
     */
    public function addFilters(array $filters): void
    {
        $this->request->merge($filters);
    }
}