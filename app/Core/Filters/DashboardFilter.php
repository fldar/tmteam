<?php


namespace App\Core\Filters;

use Illuminate\Database\Eloquent\Builder;

class DashboardFilter extends Filter
{
    /**
     * Filter the billing summary records between the date range value.
     *
     * @param string $value
     * @return Builder
     */
    public function dateRange(string $value): Builder
    {
        list($begin, $end) = explode(' - ', $value);

        return $this->builder->where('date', '>=', $begin)
                             ->where('date', '<=', $end);
    }

    /**
     * Filter the billing summary records between the date range value.
     *
     * @param array $value
     * @return Builder
     */
    public function classroomId(array $value)
    {
        return $this->builder->whereIn('classroom_id', $value);
    }

    /**
     * Filter the student records by the given subsidiary id value.
     *
     * @param string $value
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->builder->where('subsidiary_id', $value);
    }
}