<?php


namespace App\Core\DTOs;


use Illuminate\Http\Request;
use Jetimob\Juno\Lib\Model\ChargeResource;
use Jetimob\Juno\Lib\Model\Payment;

/**
 * Class BalanceRecordData
 * @package App\Core\DTOs
 */
class BalanceRecordData extends DataTransferObject
{

    /**
     * @var int|null
     */
    public ?int $id;

    /**
     * @var string|null
     */
    public ?string $description;

    /**
     * @var string|null
     */
    public ?string $type;

    /**
     * @var float|null
     */
    public ?float $amount;

    /**
     * @var float|null
     */
    public ?float $amount_paid;

    /**
     * @var int|null
     */
    public ?int $discount_amount;

    /**
     * @var int|null
     */
    public ?int $discount_days;

    /**
     * @var \DateTime|null
     */
    public ?\DateTime $due_date;

    /**
     * @var \DateTime|null
     */
    public ?\DateTime $payment_date;

    /**
     * @var int|null
     */
    public ?int $payment_type;

    /**
     * @var bool|null
     */
    public ?bool $canceled;

    /**
     * @var string|null
     */
    public ?string $charge_code;

    /**
     * @var string|null
     */
    public ?string $charge_link;

    /**
     * @var string|null
     */
    public ?string $charge_fee;

    /**
     * @var string|null
     */
    public ?string $chargeable_type;

    /**
     * @var int|null
     */
    public ?int $chargeable_id;

    /**
     * @var int|null
     */
    public ?int $subsidiary_id;

    /**
     * @var string|null
     */
    public ?string $gateway_id;


    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Core\DTOs\BalanceRecordData
     */
    public static function fromRequest(Request $request): self
    {
        return new self([
            'id'              => strict_cast('int', $request->input('id')),
            'description'     => $request->input('description'),
            'type'            => strict_cast('int', $request->input('type')),
            'amount'          => strict_cast('float', $request->input('amount')),
            'amount_paid'     => strict_cast('float', $request->input('amount_paid')),
            'discount_amount' => strict_cast('float', $request->input('discount_amount')),
            'discount_days'   => strict_cast('int', $request->input('discount_days')),
            'due_date'        => strict_cast('date', $request->input('due_date')),
            'payment_date'    => strict_cast('date', $request->input('payment_date')),
            'payment_type'    => strict_cast('int', $request->input('payment_type')),
            'canceled'        => strict_cast('bool', $request->input('canceled', false)),
            'chargeable_id'   => strict_cast('int', $request->input('chargeable_id')),
            'subsidiary_id'   => strict_cast('int', $request->input('subsidiary_id'))
        ]);
    }

    /**
     * @param \Jetimob\Juno\Lib\Model\ChargeResource $resource
     *
     * @return \App\Core\DTOs\BalanceRecordData
     */
    public static function fromChargeResource(ChargeResource $resource): self
    {
        return new self([
            'charge_code' => (string)$resource->code,
            'charge_link' => $resource->link,
            'gateway_id'  => $resource->id
        ]);
    }

    public static function fromPayment(Payment $payment) : self
    {
        return new self([
            'amount_paid'  => $payment->amount,
            'payment_date' => $payment->date,
            'charge_fee'   => $payment->fee,
        ]);
    }
}