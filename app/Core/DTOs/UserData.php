<?php


namespace App\Core\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;

/**
 * Class UserData
 * @package App\Core\DTOs
 */
class UserData extends DataTransferObject
{
    /**
     * @var int|null
     */
    public ?int $id;

    /**
     * @var string|null
     */
    public ?string $name;

    /**
     * @var string|null
     */
    public ?string $nickname;

    /**
     * @var string|null
     */
    public ?string $cpf;

    /**
     * @var string|null
     */
    public ?string $email;

    /**
     * @var string|null
     */
    public ?string $phone;

    /**
     * @var \DateTime|null
     */
    public ?\DateTime $birthdate;

    /**
     * @var int|null
     */
    public ?int $sex;

    /**
     * @var string|null
     */
    public ?string $cep;

    /**
     * @var string|null
     */
    public ?string $street;

    /**
     * @var string|null
     */
    public ?string $number;

    /**
     * @var string|null
     */
    public ?string $neighborhood;

    /**
     * @var string|null
     */
    public ?string $complement;

    /**
     * @var string|null
     */
    public ?string $city;

    /**
     * @var string|null
     */
    public ?string $state;

    /**
     * @var string|null
     */
    public ?string $password;

    /**
     * @var bool|null
     */
    public ?bool  $receive_messages;

    /**
     * @var bool|null
     */
    public ?bool $active;

    /**
     * @var int|null
     */
    public ?int $subsidiary_id;

    /**
     * @var \Illuminate\Http\UploadedFile|null
     */
    public ?UploadedFile $avatar;

    /**
     * @var array|null
     */
    public ?array $roles;

    /**
     * @param \Illuminate\Foundation\Http\FormRequest $request
     *
     * @return static
     */
    public static function fromRequest(FormRequest $request): self
    {
        $data = collect($request->validated());

        return new self([
            'id'               => strict_cast('int', $data->get('id')),
            'name'             => $data->get('name'),
            'nickname'         => $data->get('nickname'),
            'cpf'              => $data->get('cpf'),
            'email'            => $data->get('email'),
            'phone'            => $data->get('phone'),
            'birthdate'        => strict_cast('date', $data->get('birthdate')),
            'sex'              => strict_cast('int', $data->get('sex')),
            'cep'              => $data->get('cep'),
            'street'           => $data->get('street'),
            'number'           => $data->get('number'),
            'neighborhood'     => $data->get('neighborhood'),
            'complement'       => $data->get('complement'),
            'city'             => $data->get('city'),
            'state'            => $data->get('state'),
            'password'         => $data->get('password'),
            'receive_messages' => strict_cast('bool', $data->get('receive_messages', 1)),
            'active'           => strict_cast('bool', $data->get('active')),
            'subsidiary_id'    => strict_cast('int', $data->get('subsidiary_id')),
            'avatar'           => $data->get('avatar'),
            'roles'            => $data->get('roles'),
        ]);
    }
}