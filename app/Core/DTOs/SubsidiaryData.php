<?php


namespace App\Core\DTOs;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SubsidiaryData
 * @package App\Core\DTOs
 */
class SubsidiaryData extends DataTransferObject
{

    /**
     * @var int|null
     */
    public ?int $id;

    /**
     * @var string|null
     */
    public ?string $name;

    /**
     * @var string|null
     */
    public ?string $fantasy_name;

    /**
     * @var string|null
     */
    public ?string $cnpj;

    /**
     * @var string|null
     */
    public ?string $company_type;

    /**
     * @var string|null
     */
    public ?string $business_area;

    /**
     * @var string|null
     */
    public ?string $lines_of_business;

    /**
     * @var string|null
     */
    public ?string $phone;

    /**
     * @var string|null
     */
    public ?string $email;

    /**
     * @var string|null
     */
    public ?string $cep;

    /**
     * @var string|null
     */
    public ?string $street;

    /**
     * @var string|null
     */
    public ?string $number;

    /**
     * @var string|null
     */
    public ?string $neighborhood;

    /**
     * @var string|null
     */
    public ?string $city;

    /**
     * @var string|null
     */
    public ?string $state;

    /**
     * @var string|null
     */
    public ?string $complement;

    /**
     * @var int|null
     */
    public ?int $manager_id;

    /**
     * @var string|null
     */
    public ?string $bank_number;

    /**
     * @var string|null
     */
    public ?string $agency_number;

    /**
     * @var string|null
     */
    public ?string $account_number;

    /**
     * @var string|null
     */
    public ?string $account_complement_number;

    /**
     * @var string|null
     */
    public ?string $account_type;

    /**
     * @var string|null
     */
    public ?string $account_holder_name;

    /**
     * @var string|null
     */
    public ?string $account_holder_document;

    /**
     * @param \Illuminate\Foundation\Http\FormRequest $request
     *
     * @return static
     */
    public static function fromRequest(FormRequest $request): self
    {
        $data = collect($request->validated());

        return new self([
            'id'                        => $data->get('id'),
            'name'                      => $data->get('name'),
            'fantasy_name'              => $data->get('fantasy_name'),
            'cnpj'                      => $data->get('cnpj'),
            'company_type'              => $data->get('company_type'),
            'business_area'             => $data->get('business_area'),
            'lines_of_business'         => $data->get('lines_of_business'),
            'phone'                     => $data->get('phone'),
            'email'                     => $data->get('email'),
            'cep'                       => $data->get('cep'),
            'street'                    => $data->get('street'),
            'number'                    => $data->get('number'),
            'neighborhood'              => $data->get('neighborhood'),
            'city'                      => $data->get('city'),
            'state'                     => $data->get('state'),
            'complement'                => $data->get('complement'),
            'manager_id'                => strict_cast('int', $data->get('manager_id')),
            'bank_number'               => $data->get('bank_number'),
            'agency_number'             => $data->get('agency_number'),
            'account_number'            => $data->get('account_number'),
            'account_complement_number' => $data->get('account_complement_number'),
            'account_type'              => $data->get('account_type'),
            'account_holder_name'       => $data->get('account_holder_name'),
            'account_holder_document'   => $data->get('account_holder_document')
        ]);
    }
}
