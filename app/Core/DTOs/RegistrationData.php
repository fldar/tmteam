<?php


namespace App\Core\DTOs;

use Illuminate\Http\UploadedFile;

/**
 * Class RegistrationData
 * @package App\Core\DTOs
 */
class RegistrationData extends DataTransferObject
{

    /**
     * @var int|null
     */
    public ?int $id;

    /**
     * @var \DateTime|null
     */
    public ?\DateTime $effective_date;

    /**
     * @var float|null
     */
    public ?float $acquisition;

    /**
     * @var float|null
     */
    public ?float $amount;

    /**
     * @var float|null
     */
    public ?float $advance_discount;

    /**
     * @var int|null
     */
    public ?int $payment_type;

    /**
     * @var int|null
     */
    public ?int $payment_day;

    /**
     * @var int|null
     */
    public ?int $status;

    /**
     * @var int|null
     */
    public ?int $student_id;

    /**
     * @var int|null
     */
    public ?int $subsidiary_id;

    /**
     * @var \Illuminate\Http\UploadedFile|null
     */
    public ?UploadedFile $fisic;

    /**
     * @var array|null
     */
    public ?array $classrooms;

}
