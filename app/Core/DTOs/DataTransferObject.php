<?php


namespace App\Core\DTOs;


use Spatie\DataTransferObject\FlexibleDataTransferObject;

class DataTransferObject extends FlexibleDataTransferObject
{

    /**
     * Returns only filled attributes
     *
     * @return array
     */
    public function onlyFilled(): array
    {
        return array_filter($this->toArray(), function ($value){
            return $value !== null;
        });
    }

}