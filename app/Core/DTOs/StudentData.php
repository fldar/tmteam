<?php


namespace App\Core\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

/**
 * Class StudentData
 * @package App\Core\DTOs
 */
class StudentData extends DataTransferObject
{
    /**
     * @var int|null
     */
    public ?int $id;

    /**
     * @var bool|null
     */
    public ?bool $experimental;

    /**
     * @var string|null
     */
    public ?string $cbjj;

    /**
     * @var int|null
     */
    public ?int $origin_id;

    /**
     * @var int|null
     */
    public ?int $age_category_id;

    /**
     * @var int|null
     */
    public ?int $guardian_id;

    /**
     * @var int|null
     */
    public ?int $user_id;

    /**
     * @var \App\Core\DTOs\UserData|null
     */
    public ?UserData $user;

    /**
     * @var \App\Core\DTOs\RegistrationData|null
     */
    public ?RegistrationData $registration;

    /**
     * @param \Illuminate\Foundation\Http\FormRequest $request
     *
     * @return static
     */
    public static function fromRequest(FormRequest $request): self
    {
        $data = collect($request->validated());

        return new self([
            'id'              => strict_cast('int', $data->get('id')),
            'user_id'         => strict_cast('int', $data->get('user_id')),
            'experimental'    => strict_cast('bool', $data->get('experimental')),
            'origin_id'       => strict_cast('int', $data->get('origin_id')),
            'age_category_id' => strict_cast('int', $data->get('age_category_id')),
            'guardian_id'     => strict_cast('int', $data->get('guardian_id')),
            'cbjj'            => $data->get('cbjj'),
            'user'            => self::collectUserData($data),
            'registration'    => self::collectRegistrationData($data)
        ]);
    }

    private static function collectRegistrationData(Collection $studentData)
    {
        if ($studentData->has('registration')) {
            $registration = collect($studentData->get('registration'));

            return array_merge($registration->toArray(), [
                'id'               => strict_cast('int', $studentData->get('registration_id')),
                'effective_date'   => strict_cast('date', $registration->get('effective_date')),
                'acquisition'      => strict_cast('float', $registration->get('acquisition')),
                'amount'           => strict_cast('float', $registration->get('amount')),
                'advance_discount' => strict_cast('float', $registration->get('advance_discount')),
                'payment_type'     => strict_cast('int', $registration->get('payment_type')),
                'payment_day'      => strict_cast('int', $registration->get('payment_day')),
                'status'           => strict_cast('int', $registration->get('status', 1)),
                'student_id'       => strict_cast('int', $studentData->get('id')),
                'subsidiary_id'    => strict_cast('int', $registration->get('subsidiary_id')),
                'fisic'            => $registration->get('fisic')
            ]);
        }
    }

    private static function collectUserData(Collection $studentData)
    {
        $user = collect($studentData->get('user'));

        return array_merge($user->toArray(), [
            'id'            => strict_cast('int', $studentData->get('user_id')),
            'name'          => $user->get('name'),
            'email'         => $user->get('email'),
            'phone'         => $user->get('phone'),
            'birthdate'     => strict_cast('date', $user->get('birthdate')),
            'sex'           => strict_cast('int', $user->get('sex')),
            'cep'           => $user->get('cep'),
            'street'        => $user->get('street'),
            'number'        => $user->get('number'),
            'neighborhood'  => $user->get('neighborhood'),
            'complement'    => $user->get('complement'),
            'city'          => $user->get('city'),
            'state'         => $user->get('state'),
            'password'      => $user->get('password'),
            'subsidiary_id' => strict_cast('int', $user->get('subsidiary_id')),
            'avatar'        => $user->get('avatar'),
            'active'        => strict_cast('bool', $user->get('active'))
        ]);
    }

}