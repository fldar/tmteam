<?php


namespace App\Core\DTOs;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FrequencyData
 * @package App\Core\DTOs
 */
class FrequencyData extends DataTransferObject
{

    /**
     * @var int|null
     */
    public ?int $id;

    /**
     * @var int|null
     */
    public ?int $registration_id;

    /**
     * @var int|null
     */
    public ?int $classroom_id;

    /**
     * @var int|null
     */
    public ?int $graduation_id;

    /**
     * @var \DateTime|null
     */
    public ?\DateTime $date;

    /**
     * @var bool|null
     */
    public ?bool $reviewed;

    /**
     * @var string|null
     */
    public ?string $remark;

    /**
     * @param \Illuminate\Foundation\Http\FormRequest $request
     *
     * @return static
     */
    public static function fromRequest(FormRequest $request): self
    {
        $data = collect($request->validated());

        return new self([
            'id'              => strict_cast('int', $data->get('id')),
            'registration_id' => strict_cast('int', $data->get('registration_id')),
            'classroom_id'    => strict_cast('int', $data->get('classroom_id')),
            'graduation_id'   => strict_cast('int', $data->get('graduation_id')),
            'date'            => strict_cast('date', $data->get('date')),
            'reviewed'        => strict_cast('bool', $data->get('reviewed')),
            'remark'          => $data->get('remark')
        ]);
    }
}
