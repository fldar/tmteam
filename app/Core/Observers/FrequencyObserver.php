<?php

namespace App\Core\Observers;

use App\Core\Models\TrainingCenter\Frequency;
use App\Core\Models\TrainingCenter\Registration;

class FrequencyObserver
{

    /**
     * Handle the frequency "creating" event.
     *
     * @param  \App\Core\Models\TrainingCenter\Frequency  $frequency
     * @return void
     */
    public function creating(Frequency $frequency)
    {
        if (empty($model->graduation_id)) {
            $registration = Registration::find($frequency->registration_id);
            $graduation   = $registration->currentGraduation();

            $frequency->graduation_id = $graduation->id ?? null;
        }
    }


    /**
     * Handle the frequency "created" event.
     *
     * @param  \App\Core\Models\TrainingCenter\Frequency  $frequency
     * @return void
     */
    public function created(Frequency $frequency)
    {
        if (($graduation = $frequency->graduation) && $frequency->reviewed) {
            $graduation->lessons_taken++;
            $graduation->save();
        }
    }

    /**
     * Handle the frequency "updated" event.
     *
     * @param  \App\Core\Models\TrainingCenter\Frequency  $frequency
     * @return void
     */
    public function updated(Frequency $frequency)
    {
        if (($graduation = $frequency->graduation) && $frequency->isDirty('reviewed')) {
            $frequency->reviewed ? $graduation->lessons_taken++ : $graduation->lessons_taken--;

            $graduation->save();
        }
    }

    /**
     * Handle the frequency "deleted" event.
     *
     * @param  \App\Core\Models\TrainingCenter\Frequency  $frequency
     * @return void
     */
    public function deleted(Frequency $frequency)
    {
        if (($graduation = $frequency->graduation)) {
            $graduation->lessons_taken--;
            $graduation->save();
        }
    }
}
