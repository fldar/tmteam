<?php


namespace App\Core\Services\Financial;

use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\TrainingCenter\Registration;
use App\Http\Requests\SaveBalanceRecord;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Illuminate\Support\Facades\DB;

class InstallmentService
{

    /**
     * @param \App\Http\Requests\SaveBalanceRecord $request
     * @param \App\Core\Models\TrainingCenter\Registration|null $registration
     *
     * @throws \Exception
     */
    public function generate(
        SaveBalanceRecord $request,
        Registration $registration = null
    ) {
        DB::beginTransaction();

        $period    = new DateInterval($request->repeat);
        $startDate = Carbon::createFromFormat('Y-m-d', $request->due_date);
        $endDate   = $this->fetchEndDate($startDate, $period, $request->repeat_times);
        $interval  = new DatePeriod($startDate, $period, $endDate);

        foreach ($interval as $key => $nextDate) {

            $record              = new BalanceRecord($request->validated());
            $record->due_date    = $nextDate;
            $record->description = $this->formatDescription($request, $key + 1);

            if ($registration) {
                $record->chargeable()->associate($registration);
            }

            $record->save();
        }

        DB::commit();
    }

    private function fetchEndDate(Carbon $startDate, DateInterval $period, $times)
    {
        $endDate = clone $startDate;

        for ($i = 0; $i < $times; $i++) {
            $endDate->add($period);
        }

        return $endDate;
    }

    private function formatDescription(SaveBalanceRecord $request, $installmentNumber)
    {
        $total       = $request->repeat_times;
        $description = $request->description;

        return "$installmentNumber/$total - $description";
    }
}