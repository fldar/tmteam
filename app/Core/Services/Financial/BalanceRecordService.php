<?php


namespace App\Core\Services\Financial;

use App\Core\DTOs\BalanceRecordData;
use App\Core\Filters\BalanceRecordFilter;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Services\Juno\AccountService;
use App\Core\Services\Juno\ChargeService;
use App\Enums\SplitTypeEnum;

class BalanceRecordService
{

    protected BalanceRecord  $model;
    protected ChargeService  $chargeService;
    protected AccountService $accountService;

    /**
     * BalanceRecordService constructor.
     *
     * @param \App\Core\Models\Financial\BalanceRecord $model
     * @param \App\Core\Services\Juno\ChargeService $chargeService
     * @param \App\Core\Services\Juno\AccountService $accountService
     */
    public function __construct(
        BalanceRecord $model, ChargeService $chargeService, AccountService $accountService
    ) {
        $this->model          = $model;
        $this->chargeService  = $chargeService;
        $this->accountService = $accountService;
    }

    /**
     * @param \App\Core\DTOs\BalanceRecordData $data
     *
     * @return \App\Core\Models\Financial\BalanceRecord|\Illuminate\Database\Eloquent\Model|null
     */
    public function createOrUpdate(BalanceRecordData $data)
    {
        $balanceRecord = $this->model->make();

        if ($data->id) {
            $balanceRecord = $this->find($data->id);
        }

        $balanceRecord->fill(array_filter($data->all()));
        $balanceRecord->save();

        return $balanceRecord->fresh();
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\Financial\BalanceRecord
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): BalanceRecord
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Filters\BalanceRecordFilter $filter
     * @param int|null $perPage
     * @param array|null $appends
     *
     * @return \App\Core\Models\Financial\BalanceRecord[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function list(BalanceRecordFilter $filter, int $perPage = null, array $appends = null)
    {
        $builder = $this->model->filter($filter)->latest();

        if ($perPage) {
            return $builder->paginate($perPage)->appends($appends);
        }

        return $builder->get();
    }

    /**
     * @param int $id
     *
     * @throws \App\Core\Services\Juno\ResponseErrorException
     * @throws \App\Core\Services\Juno\AccountNotVerifiedException
     */
    public function charge(int $id)
    {
        $record     = $this->find($id);
        $chargeable = $record->chargeable;

        if (!$chargeable) {
            throw new \InvalidArgumentException('The provided record does not contains any chargeable');
        }

        $record->split_amount = $this->calcSplitAmount($record);
        $user                 = $chargeable->financier->user;
        $result               = $this->chargeService->create($record, $user);
        $charge               = current($result->getCharges());
        $data                 = BalanceRecordData::fromChargeResource($charge);

        $record->fill($data->onlyFilled());
        $record->save();
    }

    private function calcSplitAmount(BalanceRecord $record): float
    {
        if ($this->accountService->isMainAccount($record->subsidiary)) {
            return 0;
        }

        $splitAmount = floatval(config('settings.split_amount'));

        if (config('settings.split_type') === SplitTypeEnum::AMOUNT) {
            return $splitAmount;
        }

        return $record->amount * round(($splitAmount / 100), 2);
    }
}
