<?php


namespace App\Core\Services\TrainingCenter\RegistrationIssues;

use App\Enums\AttachmentStatusEnum;
use App\Enums\RegistrationStatusEnum;
use App\Core\Models\Auth\User;
use App\Core\Models\TrainingCenter\Registration;

class InvalidMedias implements Issuer
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var Registration
     */
    private $registration;

    public function __construct(Registration $registration)
    {
        $this->registration = $registration;
        $this->user         = $registration->student->user;
    }

    public function check(): int
    {
        $result             = RegistrationStatusEnum::REGULAR;
        $avatarStatus       = $this->user->avatar_validation_status;
        $registrationStatus = $this->registration->fisic_validation_status;

        if ($avatarStatus === AttachmentStatusEnum::INVALID ||
            $registrationStatus === AttachmentStatusEnum::INVALID
        ) {
            $result = RegistrationStatusEnum::IRREGULAR;
        }

        return $result;
    }

    public function description(): string
    {
        return "Anexo inválido";
    }
}