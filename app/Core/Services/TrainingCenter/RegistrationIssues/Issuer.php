<?php


namespace App\Core\Services\TrainingCenter\RegistrationIssues;


interface Issuer
{
    public function check(): int;

    public function description(): string;
}