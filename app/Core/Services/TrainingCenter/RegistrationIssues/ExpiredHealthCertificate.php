<?php


namespace App\Core\Services\TrainingCenter\RegistrationIssues;

use App\Enums\RegistrationStatusEnum;
use App\Core\Models\Auth\User;
use App\Core\Models\TrainingCenter\Registration;
use App\Notifications\Member\HealthCertificateIsAboutToExpire;
use Illuminate\Support\Carbon;
use Spatie\MediaLibrary\Models\Media;

class ExpiredHealthCertificate implements Issuer
{
    /**
     * @var Registration
     */
    private $registration;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Media
     */
    private $healthCertificate;

    public function __construct(Registration $registration)
    {
        $this->registration      = $registration;
        $this->user              = $registration->student->user;
        $this->healthCertificate = $registration->fisic;
    }

    public function check(): int
    {
        if ($this->checkForHealthCertificateExpiration()) {
            return RegistrationStatusEnum::IRREGULAR;
        }

        return RegistrationStatusEnum::REGULAR;
    }

    private function checkForHealthCertificateExpiration()
    {
        if (empty($this->healthCertificate)) {
            return false;
        }

        $created      = $this->healthCertificate->created_at;
        $expireIn     = (clone $created)->addYear();
        $today        = Carbon::now();
        $daysToExpire = $expireIn->diffInDays($created);

        $this->notifyWhenCloseToExpire($daysToExpire);

        if ($today->lt($expireIn)) {
            return false;
        }

        return true;
    }

    private function notifyWhenCloseToExpire(int $daysToExpire)
    {
        if (in_array($daysToExpire, [60, 30, 15, 5, 1])) {
            $this->user->notify(new HealthCertificateIsAboutToExpire($daysToExpire));
        }
    }

    public function description(): string
    {
        return "O atestado médico anexado ultrapassou a validade de 1 ano";
    }
}