<?php


namespace App\Core\Services\TrainingCenter\RegistrationIssues;


use App\Core\Models\TrainingCenter\Registration;
use App\Enums\PaymentTypesEnum;
use App\Enums\RegistrationStatusEnum;

class IssueChecker
{

    /**
     * @var \App\Core\Models\TrainingCenter\Registration
     */
    protected $registration;

    protected $issuers = [
        Overdue::class,
        InvalidMedias::class,
        FirstHealthAttachment::class,
        ExpiredHealthCertificate::class
    ];

    /**
     * IssueChecker constructor.
     *
     * @param \App\Core\Models\TrainingCenter\Registration $registration
     */
    public function __construct(Registration $registration)
    {
        $this->registration = $registration;
    }

    protected function skipCheck()
    {
        return $this->registration->status === RegistrationStatusEnum::CANCELLED ||
            $this->registration->dontCheck();
    }

    public function check(): int
    {
        $status = RegistrationStatusEnum::REGULAR;

        if ($this->skipCheck()) {
            return $this->registration->status;
        }

        foreach ($this->issuers as $issuer) {
            $verificationStatus = $this->checkStatus(new $issuer($this->registration));
            $status             = max($status, $verificationStatus);
        }

        return $status;
    }

    protected function checkStatus(Issuer $issuer): int
    {
        $status      = $issuer->check();
        $issuerClass = get_class($issuer);

        if ($status !== RegistrationStatusEnum::REGULAR) {
            $this->registerIssue($issuerClass, $issuer->description());
        } else {
            $this->resolveIssue($issuerClass);
        }

        return $status;
    }

    protected function registerIssue(string $type, string $description)
    {
        $this->registration->issues()->firstOrCreate(
            ['type' => $type],
            ['description' => $description]);
    }

    protected function resolveIssue(string $type)
    {
        $this->registration->issues()->where('type', $type)->delete();
    }
}