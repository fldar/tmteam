<?php


namespace App\Core\Services\TrainingCenter;

use App\Core\DTOs\StudentData;
use App\Core\DTOs\UserData;
use App\Core\Filters\StudentFilter;
use App\Core\Models\TrainingCenter\Student;
use App\Core\Services\Auth\UserService;
use Illuminate\Support\Facades\DB;

class StudentService
{

    protected Student             $model;
    protected UserService         $userService;
    protected RegistrationService $registrationService;

    /**
     * StudentService constructor.
     *
     * @param \App\Core\Models\TrainingCenter\Student $model
     * @param \App\Core\Services\Auth\UserService $userService
     * @param \App\Core\Services\TrainingCenter\RegistrationService $registrationService
     */
    public function __construct(
        Student $model,
        UserService $userService,
        RegistrationService $registrationService
    ) {
        $this->model               = $model;
        $this->userService         = $userService;
        $this->registrationService = $registrationService;
    }


    /**
     * @param \App\Core\DTOs\StudentData $data
     *
     * @return \App\Core\Models\TrainingCenter\Student|\Illuminate\Database\Eloquent\Model|null
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function createOrUpdate(StudentData $data)
    {
        $student = $this->model->make();

        if ($data->user) {
            $data->user_id = $this->createOrUpdateUserForStudent($data->user);
        }

        if ($data->id) {
            $student = $this->find($data->id);
        }

        $student->fill(array_filter($data->all()));
        $student->save();

        if ($data->registration) {
            $data->registration->student_id = $student->id;
            $this->registrationService->createOrUpdate($data->registration);
        }

        return $student->fresh();
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\TrainingCenter\Student
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): Student
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Filters\StudentFilter $filter
     * @param int|null $perPage
     * @param array|null $appends
     *
     * @return \App\Core\Models\TrainingCenter\Student[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function list(StudentFilter $filter, int $perPage = null, array $appends = null)
    {
        $builder = $this->model->filter($filter)
                               ->withoutGlobalScopes()
                               ->latest();

        if ($perPage) {
            return $builder->paginate($perPage)->appends($appends);
        }

        return $builder->get();
    }

    /**
     * @param \App\Core\DTOs\UserData $userData
     *
     * @return int|mixed
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    protected function createOrUpdateUserForStudent(UserData $userData)
    {
        $user = $this->userService->createOrUpdate($userData);

        if (!$user->isStudent()) {
            $user->assignRole('aluno');
        }

        return $user->id;
    }
}