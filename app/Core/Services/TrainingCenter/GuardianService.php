<?php


namespace App\Core\Services\TrainingCenter;

use App\Core\DTOs\GuardianData;
use App\Core\DTOs\UserData;
use App\Core\Filters\GuardianFilter;
use App\Core\Models\TrainingCenter\Guardian;
use App\Core\Services\Auth\UserService;
use Illuminate\Support\Facades\DB;

class GuardianService
{

    protected Guardian    $model;
    protected UserService $userService;

    /**
     * GuardianService constructor.
     *
     * @param \App\Core\Models\TrainingCenter\Guardian $model
     * @param \App\Core\Services\Auth\UserService $userService
     */
    public function __construct(Guardian $model, UserService $userService)
    {
        $this->model       = $model;
        $this->userService = $userService;
    }


    /**
     * @param \App\Core\DTOs\GuardianData $data
     *
     * @return \App\Core\Models\TrainingCenter\Guardian|\Illuminate\Database\Eloquent\Model|null
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function createOrUpdate(GuardianData $data)
    {
        $guardian = $this->model->make();

        if($data->user){
            $data->user_id = $this->createOrUpdateUserForGuardian($data->user);
        }

        if ($data->id) {
            $guardian = $this->find($data->id);
        }

        $guardian->fill($data->all());
        $guardian->save();

        return $guardian->fresh();
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\TrainingCenter\Guardian
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): Guardian
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Filters\GuardianFilter $filter
     * @param int|null $perPage
     * @param array|null $appends
     *
     * @return \App\Core\Models\TrainingCenter\Guardian[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function list(GuardianFilter $filter, int $perPage = null, array $appends = null)
    {
        $builder = $this->model->filter($filter)
                               ->withoutGlobalScopes()
                               ->latest();

        if ($perPage) {
            return $builder->paginate($perPage)->appends($appends);
        }

        return $builder->get();
    }

    /**
     * @param \App\Core\DTOs\UserData $userData
     *
     * @return int|mixed
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    protected function createOrUpdateUserForGuardian(UserData $userData)
    {
        $user = $this->userService->createOrUpdate($userData);

        if(!$user->isGuardian()){
            $user->assignRole('responsavel');
        }

        return $user->id;
    }
}