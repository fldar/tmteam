<?php


namespace App\Core\Services;

use App\Core\DTOs\SubsidiaryData;
use App\Core\Filters\SubsidiaryFilter;
use App\Core\Models\Subsidiary;
use App\Core\Services\Juno\AccountService;
use Auth;

class SubsidiaryService
{

    protected Subsidiary     $model;
    protected AccountService $accountService;

    /**
     * SubsidiaryService constructor.
     *
     * @param \App\Core\Models\Subsidiary $model
     * @param \App\Core\Services\Juno\AccountService $accountService
     */
    public function __construct(Subsidiary $model, AccountService $accountService)
    {
        $this->model          = $model;
        $this->accountService = $accountService;
    }


    /**
     * @param \App\Core\DTOs\SubsidiaryData $data
     *
     * @return \App\Core\Models\Subsidiary|\Illuminate\Database\Eloquent\Model|null
     */
    public function createOrUpdate(SubsidiaryData $data)
    {
        $subsidiary = $this->model->make();

        if ($data->id) {
            $subsidiary = $this->find($data->id);
        }

        $subsidiary->fill($data->all());
        $subsidiary->save();

        $this->transferManager($subsidiary);

        return $subsidiary->fresh();
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\Subsidiary
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): Subsidiary
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param string $column
     * @param string $value
     *
     * @return \App\Core\Models\Subsidiary|\Illuminate\Database\Eloquent\Model
     */
    public function findBy(string $column, string $value)
    {
        return $this->model->where($column, $value)->firstOrFail();
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Filters\SubsidiaryFilter $filter
     * @param int|null $perPage
     * @param array|null $appends
     *
     * @return \App\Core\Models\Subsidiary[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function list(SubsidiaryFilter $filter, int $perPage = null, array $appends = null)
    {
        $builder = $this->model->filter($filter)->latest();

        if ($perPage) {
            return $builder->paginate($perPage)->appends($appends);
        }

        return $builder->get();
    }

    /**
     * @return \App\Core\Models\Subsidiary
     */
    public function current() : Subsidiary
    {
        return Auth::user()->subsidiary;
    }

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return bool
     */
    private function transferManager(Subsidiary $subsidiary): bool
    {
        $manager = $subsidiary->manager;

        if (!$manager->hasRole('gestor')) {
            $manager->assignRole('gestor');
        }

        if ($manager->subsidiary_id == $subsidiary->id) {
            return true;
        }

        $manager->subsidiary_id = $subsidiary->id;
        return $manager->save();
    }

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return bool
     * @throws \App\Core\Services\Juno\ResponseErrorException|\Jetimob\Juno\Exception\MissingPropertyBodySchemaException
     */
    public function createOrUpdateAccount(Subsidiary $subsidiary)
    {
        if(empty($subsidiary->resource_token)){
            $response = $this->accountService->create($subsidiary);
            $subsidiary->resource_token = $response->resourceToken;
            $subsidiary->gateway_id = $response->id;

            return $subsidiary->save();
        }

        $this->accountService->update($subsidiary);

        return true;
    }

}
