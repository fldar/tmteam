<?php


namespace App\Core\Services\Juno;

use App\Core\Models\Subsidiary;
use App\Core\Services\Juno\Adapters\Address;
use App\Core\Services\Juno\Adapters\BankAccount;
use App\Core\Services\Juno\Adapters\LegalRepresentative;
use Illuminate\Support\Facades\Log;
use Jetimob\Juno\Facades\Juno;
use Jetimob\Juno\Lib\Http\Account\AccountConsultRequest;
use Jetimob\Juno\Lib\Http\Account\AccountConsultResponse;
use Jetimob\Juno\Lib\Http\Account\AccountCreationRequest;
use Jetimob\Juno\Lib\Http\Account\AccountCreationResponse;
use Jetimob\Juno\Lib\Http\Account\AccountUpdateRequest;
use Jetimob\Juno\Lib\Http\Account\AccountUpdateResponse;
use Jetimob\Juno\Lib\Http\ErrorResponse;
use Jetimob\Juno\Lib\Http\ExtraData\ExtraBanksInfoRequest;
use Jetimob\Juno\Lib\Http\ExtraData\ExtraBanksInfoResponse;
use Jetimob\Juno\Lib\Http\ExtraData\ExtraBusinessAreasRequest;
use Jetimob\Juno\Lib\Http\ExtraData\ExtraBusinessAreasResponse;
use Jetimob\Juno\Lib\Http\ExtraData\ExtraCompanyTypesRequest;
use Jetimob\Juno\Lib\Http\ExtraData\ExtraCompanyTypesResponse;

/**
 * Class AccountService
 * @package App\Core\Services\Gateway
 */
class AccountService
{

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return \Jetimob\Juno\Lib\Http\Account\AccountCreationResponse
     * @throws \App\Core\Services\Juno\ResponseErrorException
     */
    public function create(Subsidiary $subsidiary): AccountCreationResponse
    {
        $request                      = new AccountCreationRequest();
        $request->bankAccount         = new BankAccount($subsidiary);
        $request->address             = new Address($subsidiary);
        $request->legalRepresentative = new LegalRepresentative($subsidiary);
        $request->document            = $subsidiary->cnpj ?? $subsidiary->manager->cpf;
        $request->birthDate           = $request->legalRepresentative->birthDate;
        $request->name                = $subsidiary->name;
        $request->tradingName         = $subsidiary->fantasy_name ?? $subsidiary->name;
        $request->companyType         = $subsidiary->company_type;
        $request->email               = $subsidiary->email;
        $request->phone               = $subsidiary->phone;
        $request->businessArea        = $subsidiary->business_area;
        $request->linesOfBusiness     = $subsidiary->lines_of_business;

        $resourceToken = Juno::getConfig('private_token');
        $response      = Juno::request($request, $resourceToken);

        Log::channel('system')->info('AccountCreationResponse', (array) $response);

        if ($response instanceof ErrorResponse) {
            throw new ResponseErrorException($response);
        }

        return $response;
    }

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return \Jetimob\Juno\Lib\Http\Account\AccountUpdateResponse
     * @throws \App\Core\Services\Juno\ResponseErrorException
     */
    public function update(Subsidiary $subsidiary): AccountUpdateResponse
    {
        $request                      = new AccountUpdateRequest();
        $request->bankAccount         = new BankAccount($subsidiary);
        $request->address             = new Address($subsidiary);
        $request->legalRepresentative = new LegalRepresentative($subsidiary);
        $request->name                = $subsidiary->name;
        $request->tradingName         = $subsidiary->fantasy_name;
        $request->companyType         = $subsidiary->company_type;
        $request->email               = $subsidiary->email;
        $request->phone               = $subsidiary->phone;
        $request->businessArea        = $subsidiary->business_area;
        $request->linesOfBusiness     = $subsidiary->lines_of_business;

        $response = Juno::request($request, $subsidiary->resource_token);

        Log::channel('system')->info('AccountUpdateResponse', (array) $response);

        if ($response instanceof ErrorResponse) {
            throw new ResponseErrorException($response);
        }

        return $response;
    }

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return AccountConsultResponse
     */
    public function find(Subsidiary $subsidiary): AccountConsultResponse
    {
        return Juno::request(new AccountConsultRequest(), $subsidiary->resource_token);
    }

    /**
     * Check if given subsidiary is the main one
     *
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return bool
     */
    public function isMainAccount(Subsidiary $subsidiary): bool
    {
        return $subsidiary->resource_token === Juno::getConfig('private_token');
    }

    /**
     * Requests the list of acceptable bank information
     *
     * @return \Jetimob\Juno\Lib\Http\ExtraData\ExtraBanksInfoResponse
     */
    public function listBanks(): ExtraBanksInfoResponse
    {
        return Juno::request(new ExtraBanksInfoRequest(), Juno::getConfig('private_token'));
    }

    /**
     * Requests the list of acceptable company types
     *
     * @return \Jetimob\Juno\Lib\Http\ExtraData\ExtraCompanyTypesResponse
     */
    public function listCompanyTypes(): ExtraCompanyTypesResponse
    {
        return Juno::request(new ExtraCompanyTypesRequest(), Juno::getConfig('private_token'));
    }

    /**
     * Requests the list of acceptable business areas
     *
     * @return \Jetimob\Juno\Lib\Http\ExtraData\ExtraBusinessAreasResponse
     */
    public function listBusinessAreas(): ExtraBusinessAreasResponse
    {
        return Juno::request(new ExtraBusinessAreasRequest(), Juno::getConfig('private_token'));
    }

}
