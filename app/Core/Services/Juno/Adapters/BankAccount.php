<?php


namespace App\Core\Services\Juno\Adapters;


use App\Core\Models\Subsidiary;
use Jetimob\Juno\Lib\Model\BankAccount as JunoBankAccount;

class BankAccount extends JunoBankAccount
{

    public function __construct(Subsidiary $subsidiary)
    {
        $this->accountHolder           = new AccountHolder($subsidiary);
        $this->bankNumber              = $subsidiary->bank_number;
        $this->accountNumber           = $subsidiary->account_number;
        $this->agencyNumber            = $subsidiary->agency_number;
        $this->accountComplementNumber = $subsidiary->account_complement_number ?? '';
        $this->accountType             = $subsidiary->account_type ?? 'CHECKING';

    }

}