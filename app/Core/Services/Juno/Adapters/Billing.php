<?php


namespace App\Core\Services\Juno\Adapters;


use App\Core\Models\Auth\User;
use Jetimob\Juno\Lib\Model\Billing as JunoBilling;

class Billing extends JunoBilling
{

    public function __construct(User $user)
    {
        $this->name     = $user->name;
        $this->document = $user->cpf;
        $this->email    = $user->email;
        $this->notify   = $this->notifyInProduction();
    }

    protected function notifyInProduction()
    {
        return app()->environment('production');
    }

}