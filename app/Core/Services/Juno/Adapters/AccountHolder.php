<?php


namespace App\Core\Services\Juno\Adapters;


use App\Core\Models\Subsidiary;
use Jetimob\Juno\Lib\Model\AccountHolder as JunoAccountHolder;

class AccountHolder extends JunoAccountHolder
{

    public function __construct(Subsidiary $subsidiary)
    {
        $this->name     = $subsidiary->account_holder_name;
        $this->document = $subsidiary->account_holder_document;
    }
}