<?php

namespace App\Core\Services\Juno\V1;

use GuzzleHttp\Client;

class InvoiceService
{

    private Client $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('gateway.apiUrl')]);
    }

    /**
     * Fetch payment details on v1 gateway
     *
     * @param $paymentToken
     *
     * @return \App\Core\Services\Juno\V1\PaymentDetailsResponse
     */
    public function fetchPaymentDetails($paymentToken): PaymentDetailsResponse
    {
        $data = [
            'paymentToken' => $paymentToken,
            'responseType' => 'JSON'
        ];

        $request  = $this->client->get('fetch-payment-details', ['query' => $data]);
        $response = json_decode($request->getBody()->getContents());

        return new PaymentDetailsResponse($response->data);
    }
}
