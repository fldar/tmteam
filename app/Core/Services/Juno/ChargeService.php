<?php


namespace App\Core\Services\Juno;

use App\Core\Models\Auth\User;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Services\Juno\Adapters\Billing;
use App\Core\Services\Juno\Adapters\Charge;
use Jetimob\Juno\Facades\Juno;
use Jetimob\Juno\Lib\Http\Charge\ChargeCancelRequest;
use Jetimob\Juno\Lib\Http\Charge\ChargeCancelResponse;
use Jetimob\Juno\Lib\Http\Charge\ChargeConsultRequest;
use Jetimob\Juno\Lib\Http\Charge\ChargeConsultResponse;
use Jetimob\Juno\Lib\Http\Charge\ChargeCreationRequest;
use Jetimob\Juno\Lib\Http\Charge\ChargeCreationResponse;
use Jetimob\Juno\Lib\Http\ErrorResponse;
use Jetimob\Juno\Lib\Model\Payment;
use Jetimob\Juno\Lib\Model\Split;

class ChargeService
{

    /**
     * Creates a invoice on gateway
     *
     * @param BalanceRecord $record
     * @param \App\Core\Models\Auth\User $payer
     *
     * @return \Jetimob\Juno\Lib\Http\Charge\ChargeCreationResponse
     * @throws \App\Core\Services\Juno\ResponseErrorException
     * @throws \App\Core\Services\Juno\AccountNotVerifiedException
     */
    public function create(BalanceRecord $record, User $payer): ChargeCreationResponse
    {
        $billing    = new Billing($payer);
        $charge     = new Charge($record);
        $subsidiary = $record->subsidiary;
        $subToken   = $subsidiary->resource_token;
        $mainToken  = Juno::getConfig('private_token');

        /*if($subsidiary->gateway_status != 'VERIFIED'){
            throw new AccountNotVerifiedException;
        }*/

        if ($record->split_amount > 0) {
            $charge->split = $this->makeSplit($record, $mainToken, $subToken);
        }

        $response = Juno::request(
            new ChargeCreationRequest($charge, $billing),
            $subsidiary->resource_token
        );

        if ($response instanceof ErrorResponse) {
            throw new ResponseErrorException($response);
        }

        return $response;

    }

    /**
     * @param \App\Core\Models\Financial\BalanceRecord $record
     *
     * @return \Jetimob\Juno\Lib\Http\Charge\ChargeConsultResponse
     * @throws \App\Core\Services\Juno\ResponseErrorException
     */
    public function find(BalanceRecord $record): ChargeConsultResponse
    {
        $subsidiary = $record->subsidiary;

        $response = Juno::request(
            new ChargeConsultRequest($record->gateway_id),
            $subsidiary->resource_token
        );

        if ($response instanceof ErrorResponse) {
            throw new ResponseErrorException($response);
        }

        return $response;
    }

    /**
     * @param \App\Core\Models\Financial\BalanceRecord $record
     *
     * @return \Jetimob\Juno\Lib\Http\Charge\ChargeCancelResponse
     * @throws \App\Core\Services\Juno\ResponseErrorException
     */
    public function cancel(BalanceRecord $record): ChargeCancelResponse
    {
        $subsidiary = $record->subsidiary;

        $response = Juno::request(
            new ChargeCancelRequest($record->gateway_id),
            $subsidiary->resource_token
        );

        if ($response instanceof ErrorResponse) {
            throw new ResponseErrorException($response);
        }

        return $response;
    }

    private function makeSplit(
        BalanceRecord $record, string $mainToken, string $subToken
    ): array {

        $main                 = new Split();
        $main->recipientToken = $mainToken;
        $main->amount         = $record->split_amount;
        $main->chargeFee      = false;

        $sub                  = new Split();
        $sub->recipientToken  = $subToken;
        $sub->amount          = $record->amount - $record->split_amount;
        $sub->amountRemainder = true;
        $sub->chargeFee       = true;

        return [$main, $sub];
    }

    public function fetchPaymentDetails(BalanceRecord $record): Payment
    {
        return current($this->find($record)->payments);
    }


}
