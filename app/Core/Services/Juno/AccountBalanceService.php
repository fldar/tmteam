<?php


namespace App\Core\Services\Juno;


use App\Core\Models\Subsidiary;
use App\Core\Services\Juno\Adapters\BankAccount;
use Jetimob\Juno\Facades\Juno;
use Jetimob\Juno\Lib\Http\Balance\BalanceConsultRequest;
use Jetimob\Juno\Lib\Http\Transference\TransferenceRequest;

class AccountBalanceService
{

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return mixed
     */
    public function fetch(Subsidiary $subsidiary)
    {
        return Juno::request(new BalanceConsultRequest(), $subsidiary->resource_token);
    }

    public function transfer(Subsidiary $subsidiary, float $amount)
    {
        $request     = new TransferenceRequest();
        $bankAccount = new BankAccount($subsidiary);

        $bankAccount->accountHolder = null;
        $request->bankAccount       = $bankAccount;
        $request->name              = $subsidiary->name;
        $request->document          = $subsidiary->cnpj;
        $request->amount            = $amount;

        return Juno::request($request, $subsidiary->resource_token);
    }

}