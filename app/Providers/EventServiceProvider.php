<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\UserEventListener@handleLogin',
        ],

        'Spatie\MediaLibrary\Events\MediaHasBeenAdded' => [
            'App\Listeners\MediaEventListener@handleMediaHasBeenAdded'
        ],

        'App\Events\MediaHasBeenValidated' => [
            'App\Listeners\MediaEventListener@handleMediaHasBeenValidated'
        ],

        'App\Events\RegistrationHasBeenCanceled' => [
            'App\Listeners\RegistrationEventListener@handleCancellation',
        ],

        'App\Events\StudentIsNowQualified' => [
            'App\Listeners\QualificationEventListener',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
