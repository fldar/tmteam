<?php

namespace App\Jobs;

use App\Core\Services\Juno\V1\InvoiceService;
use App\Core\Services\Juno\ChargeService;
use App\Core\Services\TrainingCenter\RegistrationService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessPaymentNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public bool $deleteWhenMissingModels = true;
    public int  $tries                   = 3;

    /**
     * @var \App\Core\Models\Financial\BalanceRecord
     */
    protected $invoice;

    /**
     * @var string
     */
    protected $paymentToken;

    /**
     * Create a new job instance.
     *
     * @param $invoice
     * @param $paymentToken
     */
    public function __construct($invoice, $paymentToken)
    {
        $this->invoice      = $invoice;
        $this->paymentToken = $paymentToken;
    }

    /**
     * Execute the job.
     *
     * @param \App\Core\Services\Juno\V1\InvoiceService $invoiceService
     * @param \App\Core\Services\Juno\ChargeService $chargeService
     * @param \App\Core\Services\TrainingCenter\RegistrationService $registrationService
     *
     * @return void
     */
    public function handle(
        InvoiceService $invoiceService,
        ChargeService $chargeService,
        RegistrationService $registrationService
    ) {

        $result = empty($this->invoice->gateway_id) ?
            $invoiceService->fetchPaymentDetails($this->paymentToken) :
            $chargeService->fetchPaymentDetails($this->invoice);

        if (!empty($result)) {

            $this->invoice->fill([
                'charge_fee'      => $result->fee,
                'amount_paid'     => $result->amount - $result->fee,
                'payment_date'    => $result->date,
                'discount_amount' => $this->invoice->amount - $result->amount
            ])->save();

            $registrationService->checkForIssues(
                $this->invoice->chargeable
            );
        }
    }
}
