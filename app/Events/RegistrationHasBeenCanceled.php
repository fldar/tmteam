<?php

namespace App\Events;

use App\Core\Models\TrainingCenter\Registration;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class RegistrationHasBeenCanceled
{
    use Dispatchable, SerializesModels;
    /**
     * @var Registration
     */
    public $registration;

    /**
     * Create a new event instance.
     *
     * @param Registration $registration
     */
    public function __construct(Registration $registration)
    {
        $this->registration = $registration;
    }
}
