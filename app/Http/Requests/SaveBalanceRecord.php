<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SaveBalanceRecord
 * @package App\Http\Requests
 *
 * @property-read string $description
 * @property-read string $type
 * @property-read string $amount
 * @property-read string $amount_paid
 * @property-read string $discount_amount
 * @property-read string $discount_days
 * @property-read string $due_date
 * @property-read string $payment_date
 * @property-read string $payment_type
 * @property-read string $charge_fee
 * @property-read string $repeat
 * @property-read string $repeat_times
 */
class SaveBalanceRecord extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'description'     => 'required',
            'type'            => 'required',
            'amount'          => 'required|min:1',
            'amount_paid'     => 'nullable',
            'discount_amount' => 'nullable',
            'discount_days'   => 'nullable',
            'due_date'        => 'required|date_format:Y-m-d',
            'payment_date'    => 'nullable|date_format:Y-m-d',
            'payment_type'    => 'required',
            'charge_fee'      => 'nullable',
            'subsidiary_id'   => 'required'
        ];

        if($this->repeat_times || $this->repeat){
            $rules['repeat'] = 'required|string';
            $rules['repeat_times'] = 'required|numeric';
        }

        return $rules;
    }
}
