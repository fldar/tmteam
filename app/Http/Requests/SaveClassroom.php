<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveClassroom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $classroomId = $this->classroom ? $this->classroom->id : "";

        return [
            'name'          => "required|max:255|unique:classrooms,name,$classroomId",
            'teachers'      => 'required|array',
            'students'      => 'nullable|array',
            'modality_id'   => 'required',
            'subsidiary_id' => 'required'
        ];
    }
}
