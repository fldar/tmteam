<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveSettings extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'max_overdue_days' => 'required|numeric',
            'fine'             => 'required|numeric',
            'interest'         => 'required|numeric',
            'split_amount'     => 'required|numeric'
        ];
    }
}
