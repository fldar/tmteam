<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SaveUser
 * @package App\Http\Requests
 *
 * @property mixed user
 */
class SaveUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fieldRequired = $this->user ? "nullable" : "required";
        $userId           = $this->user ?? "";

        return [
            "name"                  => "nullable|max:100",
            "nickname"              => "required|max:50",
            "email"                 => "required|email|unique:users,email,$userId",
            "password"              => "$fieldRequired|min:8|confirmed",
            "password_confirmation" => "$fieldRequired|min:6",
            "cpf"                   => "nullable|cpf|unique:users,cpf,$userId",
            "phone"                 => "nullable|max:20",
            "sex"                   => "nullable",
            "birthdate"             => "nullable|date_format:Y-m-d",
            "street"                => "nullable|max:255",
            "cep"                   => "nullable|max:255",
            "neighborhood"          => "nullable|max:255",
            "number"                => "nullable|max:10",
            "city"                  => "nullable|max:255",
            "state"                 => "nullable|max:255",
            "avatar"                => "nullable|image|max:2048",
            "roles"                 => "$fieldRequired|array|min:1",
            "subsidiary_id"         => "required",
        ];
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $data = parent::validationData();

        if (empty($data["password"])) {
            unset($data["password"], $data["password_confirmation"]);
        }

        return $data;
    }
}
