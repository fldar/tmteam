<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveGraduation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'registration_id' => 'required',
            'modality_id'     => 'required',
            'teacher_id'      => 'nullable',
            'band_id'         => 'required',
            'degree_id'       => 'required',
            'lessons_taken'   => 'required',
            'graduated_on'    => 'required'
        ];
    }
}
