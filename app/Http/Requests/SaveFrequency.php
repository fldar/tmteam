<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveFrequency extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reviewed'        => 'nullable',
            'registration_id' => 'required',
            'date'            => 'required|date_format:Y-m-d H:i:s',
            'classroom_id'    => 'required'
        ];
    }
}
