<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveGuardian extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->user_id ?? "";

        return [
            "user_id"                    => "nullable",
            "user.name"                  => "required|max:100",
            "user.nickname"              => "required|max:50",
            "user.email"                 => "required|max:50|email|unique:users,email,$userId",
            "user.cpf"                   => "required|cpf|unique:users,cpf,$userId",
            "user.phone"                 => "required|max:20",
            "user.sex"                   => "required",
            "user.birthdate"             => "required|date_format:Y-m-d",
            "user.street"                => "nullable|max:255",
            "user.cep"                   => "nullable|max:9",
            "user.neighborhood"          => "nullable|max:50",
            "user.number"                => "nullable|max:10",
            "user.city"                  => "nullable|max:50",
            "user.state"                 => "nullable|max:2",
            "user.avatar"                => "nullable|image|max:2048",
            "user.subsidiary_id"         => "required",
            "user.password"              => "nullable|min:8|confirmed",
            "user.password_confirmation" => "nullable|min:6",
        ];
    }
}
