<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string|null $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            $user  = Auth::user();
            $group = 'admin';

            if ($user->isStudent()) {
                $group = 'students';
            }

            if ($user->isTeacher()) {
                $group = 'teachers';
            }

            if ($user->isGuardian()) {
                $group = 'guardians';
            }

            if ($user->isAdmin()){
                $group = 'admin';
            }

            return redirect()->route("$group.dashboard.index");
        }

        return $next($request);
    }


}
