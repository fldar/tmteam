<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class CheckRoleBeforeRoute
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->is('student*') &&
            !Auth::user()->hasRole(['aluno', 'responsavel']) ||
            $request->is('teacher*') &&
            !Auth::user()->hasRole(['professor']) ||
            $request->is('guardian*') &&
            !Auth::user()->hasRole(['responsavel'])
        ) {
            return redirect("/");
        }

        $this->setCurrentAccess($request->route()->getName());
        return $next($request);
    }

    private function setCurrentAccess($routeName)
    {
        View::share('currentAccess', explode('.', $routeName)[0]);
    }
}
