<?php

namespace App\Http\Controllers\Teacher\Classrooms;

use App\Http\Controllers\Student\BaseController;
use App\Core\Models\TrainingCenter\Classroom;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class StudentController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Display a listing of the resource.
     *
     * @param \App\Core\Models\TrainingCenter\Classroom $classroom
     *
     * @return View
     */
    public function index(Classroom $classroom)
    {
        $this->seo()->setTitle('Estudantes | Professor');

        $teacher = Auth::user()->teacher;

        $registrations = $classroom->selectStudents()
                                   ->orderBy('users.name')
                                   ->get();

        //dd($registrations);

        return view('teachers.students.index',
            compact('teacher', 'registrations', 'classroom')
        );
    }

    /**
     * Print a listing of the resource.
     *
     * @param \App\Core\Models\TrainingCenter\Classroom $classroom
     *
     * @return View
     */
    public function print(Classroom $classroom)
    {
        $this->seo()->setTitle('Estudantes | Professor');

        $teacher = Auth::user()->teacher;

        $registrations = $classroom->selectStudents()
                                   ->orderBy('users.name')
                                   ->get();

        return view('teachers.students.print',
            compact('teacher', 'registrations', 'classroom')
        );
    }
}
