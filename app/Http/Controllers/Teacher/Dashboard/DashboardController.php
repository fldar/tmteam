<?php

namespace App\Http\Controllers\Teacher\Dashboard;

use App\Http\Controllers\Student\BaseController;
use App\Core\Models\Message;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class DashboardController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Display a listing of the resource.
     * @return View
     */
    public function index()
    {
        $this->seo()->setTitle('Dashboard | Aluno');

        $teacher  = Auth::user()->teacher;
        $messages = Message::nonExpired()->get();

        return view('teachers.dashboard.index', compact('teacher', 'messages'));
    }

}
