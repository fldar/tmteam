<?php

namespace App\Http\Controllers\Teacher\Notices;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveMessage;
use App\Core\Models\Notice\Message;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;


class MessageController extends BaseController
{
    use SEOToolsTrait;

    public function __construct()
    {
        $this->middleware('permission:add_messages', ['only' => ['create', 'store']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $this->seo()->setTitle('Avisos');

        return view('teachers.messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveMessage $request
     *
     * @return RedirectResponse
     */
    public function store(SaveMessage $request)
    {
        $data = $request->validated();

        Message::create($data);

        toast()->success('Mensagem criada com sucesso.', 'Sucesso');

        return redirect()->route('teachers.dashboard.index');
    }
}
