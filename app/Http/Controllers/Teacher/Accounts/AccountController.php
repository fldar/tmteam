<?php

namespace App\Http\Controllers\Teacher\Accounts;

use App\Core\DTOs\TeacherData;
use App\Core\Services\TrainingCenter\TeacherService;
use App\Http\Controllers\Teacher\BaseController;
use App\Http\Requests\SaveTeacher;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AccountController extends BaseController
{
    use SEOToolsTrait;

    protected TeacherService $service;

    /**
     * AccountController constructor.
     *
     * @param \App\Core\Services\TrainingCenter\TeacherService $service
     */
    public function __construct(TeacherService $service)
    {
        $this->service = $service;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return View
     */
    public function edit()
    {
        $this->seo()->setTitle('Conta | Aluno');

        $model = Auth::user()->teacher;

        return view('teachers.accounts.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\SaveTeacher $request
     * @param int $id
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(SaveTeacher $request, int $id)
    {
        $data = TeacherData::fromRequest($request);
        $data->id = $id;

        $this->service->createOrUpdate($data);

        toast()->success('Dados atualizados com sucesso', 'Sucesso');

        return redirect()->route('teachers.accounts.edit');
    }
}
