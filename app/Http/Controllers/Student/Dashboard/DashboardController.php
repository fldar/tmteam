<?php

namespace App\Http\Controllers\Student\Dashboard;

use App\Http\Controllers\Student\BaseController;
use App\Core\Models\Message;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class DashboardController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Display a listing of the resource.
     * @return View
     */
    public function index()
    {
        $this->seo()->setTitle('Dashboard | Aluno');

        $student = Auth::user()->student;
        $messages = Message::nonExpired()->get();
        $registration = $student->registration;

        $installments = !$registration ? [] :
            $registration->lastSixInstallments()->get();

        $frequencies = !$registration ? [] :
            $registration->frequenciesOfWeek()->get();

        $graduation = !$registration ? [] :
            $registration->currentGraduation();

        $achievements = empty($graduation) ? [] :
            $registration->graduations()
                         ->where('band_id', $graduation->band_id)
                         ->get();

        $history = empty($graduation) ? [] :
            $registration->graduations()
                         ->orderBy('graduated_on', 'DESC')
                         ->get();

        return view('students.dashboard.index',
            compact(
                'achievements',
                'student',
                'messages',
                'installments',
                'frequencies',
                'graduation',
                'history')
        );
    }
}
