<?php

namespace App\Http\Controllers\Student\Frequencies;

use App\Core\DTOs\FrequencyData;
use App\Core\Services\TrainingCenter\FrequencyService;
use App\Http\Controllers\Student\BaseController;
use App\Http\Requests\SaveStudentFrequency;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Student;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Support\Facades\Auth;

class FrequencyController extends BaseController
{
    use SEOToolsTrait;

    protected FrequencyService $service;

    /**
     * FrequencyController constructor.
     *
     * @param \App\Core\Services\TrainingCenter\FrequencyService $service
     */
    public function __construct(FrequencyService $service)
    {
        $this->seo()->setTitle('Frequências');

        $this->service = $service;
    }


    public function index()
    {
        $registration = Auth::user()->student->registration;

        $frequencies = $registration->frequencies()
                                    ->paginate(20);

        $achievements = $registration->graduations()
                                     ->orderBy('graduated_on', 'DESC')
                                     ->get();

        return view('students.frequencies.index',
            compact('registration', 'achievements', 'frequencies')
        );
    }

    public function create(Classroom $classroom)
    {
        $user = Auth::user();
        $query = Student::with(['user', 'registration']);

        if ($user->isGuardian()) {
            $query->where('guardian_id', $user->guardian->id);
        }

        if ($user->isStudent()) {
            $query->orWhere('id', $user->student->id);
        }

        $students = $query->get()->pluck('user.name', 'registration.id');

        return view('students.frequencies.create', compact('students', 'classroom'));
    }

    public function store(SaveStudentFrequency $request)
    {
        $data = FrequencyData::fromRequest($request);

        $this->service->createOrUpdate($data);

        toast()->success('Frequência registrada com sucesso', 'Sucesso');

        $prefix = Auth::user()->isGuardian() ? 'guardians' : 'students';

        return redirect()->route("$prefix.dashboard.index");
    }
}
