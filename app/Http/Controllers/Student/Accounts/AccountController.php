<?php

namespace App\Http\Controllers\Student\Accounts;

use App\Core\DTOs\StudentData;
use App\Core\Services\TrainingCenter\StudentService;
use App\Enums\AttachmentStatusEnum;
use App\Http\Controllers\Student\BaseController;
use App\Http\Requests\SaveStudentProfile;
use App\Core\Models\TrainingCenter\Student;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AccountController extends BaseController
{
    use SEOToolsTrait;

    protected StudentService $service;

    /**
     * AccountController constructor.
     *
     * @param \App\Core\Services\TrainingCenter\StudentService $service
     */
    public function __construct(StudentService $service)
    {
        $this->service = $service;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @return View
     */
    public function edit()
    {
        $this->seo()->setTitle('Conta | Aluno');

        $model = Auth::user()->student;

        return view('students.accounts.edit', compact('model'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param SaveStudentProfile $request
     * @param int $id
     *
     * @return RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(SaveStudentProfile $request, int $id)
    {
        try {
            DB::beginTransaction();

            $data     = StudentData::fromRequest($request);
            $data->id = $id;

            $this->service->createOrUpdate($data);
        } catch (\Exception $ex) {
            DB::rollBack();

            throw $ex;
        }

        DB::commit();

        toast()->success('Dados atualizados com sucesso', 'Sucesso');

        return redirect()->route('students.accounts.edit');
    }
}
