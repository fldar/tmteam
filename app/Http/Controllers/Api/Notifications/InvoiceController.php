<?php

namespace App\Http\Controllers\Api\Notifications;

use App\Http\Controllers\Admin\BaseController;
use App\Jobs\ProcessPaymentNotification;
use App\Core\Models\Financial\BalanceRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InvoiceController extends BaseController
{
    public function update(Request $request, BalanceRecord $record)
    {
        Log::channel('system')->info('Notificação de pagamento', $request->all());

        $validate = validator($request->all(), [
            'paymentToken' => 'required'
        ]);

        if($validate->fails()) {
            return response()->json([
                'message' => 'Invalid data',
                'errors'  => $validate->getMessageBag()
            ], 403);
        }

        ProcessPaymentNotification::dispatch($record, $request->get('paymentToken'));

        return response()->json(['success' => 'ok'], 200);
    }
}
