<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SaveMessage;
use App\Core\Models\Message;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

use Exception;

class MessageController extends BaseController
{
    use SEOToolsTrait;

    public function __construct()
    {
        $this->seo()->setTitle('Avisos');

        $this->middleware('permission:view_messages', ['only' => ['index']]);
        $this->middleware('permission:add_messages', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_messages', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_messages', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return View
     */
    public function index(Request $request)
    {
        $results = Message::latest()
                          ->paginate(20)
                          ->appends($request->except('page'));

        return view('admin.messages.index', compact('results'));
    }

    /**
     * Show the specified resource.
     *
     * @param Message $message
     *
     * @return View
     */
    public function show(Message $message)
    {
        $message->users()->syncWithoutDetaching([Auth::user()->id]);

        return view('admin.messages.show', compact('message'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveMessage $request
     *
     * @return RedirectResponse
     */
    public function store(SaveMessage $request)
    {
        $data = $request->validated();

        Message::create($data);

        toast()->success('Mensagem criada com sucesso.', 'Sucesso');

        return redirect()->route('admin.messages.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Message $message
     *
     * @return View
     */
    public function edit(Message $message)
    {
        return view('admin.messages.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveMessage $request
     * @param Message $message
     *
     * @return RedirectResponse
     */
    public function update(SaveMessage $request, Message $message)
    {
        $data = $request->validated();

        $message->fill($data)->save();

        toast()->success('Mensagem atualizada com sucesso.', 'Sucesso');

        return redirect()->route('admin.messages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Message $message
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(Request $request, Message $message)
    {
        $message->delete();

        $msg = 'Mensagem removida com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
