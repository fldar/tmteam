<?php

namespace App\Http\Controllers\Admin;

use App\Core\Services\Juno\DocumentService;
use App\Core\Services\SubsidiaryService;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    use SEOToolsTrait;

    protected DocumentService   $service;
    protected SubsidiaryService $subsidiaryService;

    /**
     * DocumentController constructor.
     *
     * @param \App\Core\Services\Juno\DocumentService $service
     * @param \App\Core\Services\SubsidiaryService $subsidiaryService
     */
    public function __construct(DocumentService $service, SubsidiaryService $subsidiaryService)
    {
        $this->seo()->setTitle('Documentos');

        $this->middleware('permission:view_subsidiaries', ['only' => ['index']]);
        $this->middleware('permission:add_subsidiaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_subsidiaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_subsidiaries', ['only' => ['destroy']]);

        $this->service           = $service;
        $this->subsidiaryService = $subsidiaryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(int $id)
    {
        $subsidiary = $this->subsidiaryService->find($id);
        $results    = $this->service->list($subsidiary)->documents;

        return view('admin.subsidiaries.documents.index',
            compact('results', 'subsidiary')
        );
    }

    public function store(Request $request, int $id)
    {
        $subsidiary = $this->subsidiaryService->find($id);
        $this->service->send(
            $subsidiary,
            $request->file('document'),
            $request->get('document_id'));

        toast()->success('Documento enviado com sucesso.', 'Sucesso');

        return redirect()->route('admin.subsidiaries.documents.index', ['id' => $id]);
    }


}
