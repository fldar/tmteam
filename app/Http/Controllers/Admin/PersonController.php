<?php

namespace App\Http\Controllers\Admin;

use App\Core\Models\Auth\User;
use App\Http\Controllers\Controller;

class PersonController extends Controller
{

    /**
     * Show the data from a resource.
     *
     * @param string $cpf
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $cpf)
    {
        $user = User::whereCpf($cpf)->firstOrFail();

        $data = $user->toArray();

        $data['phone']       = $user->phone;
        $data['birthdate']   = $user->birthdate->format('Y-m-d');
        $data['guardian_id'] = $user->guardian ? $user->guardian->id : "";
        $data['student_id']  = $user->student ? $user->student->id : null;
        $data['teacher_id']  = $user->teacher ? $user->teacher->id : null;

        return response()->json(compact('data'));
    }
}
