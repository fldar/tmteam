<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Core\DTOs\UserData;
use App\Core\Filters\UserFilter;
use App\Core\Models\Subsidiary;
use App\Core\Services\Auth\UserService;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveUser;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Core\Models\Auth\User;
use App\Core\Models\Auth\Role;
use Illuminate\View\View;

class UserController extends BaseController
{
    use SEOToolsTrait;

    protected UserService $service;

    public function __construct(UserService $service)
    {
        $this->seo()->setTitle('Usuários');

        $this->middleware('permission:view_users', ['only' => ['index']]);
        $this->middleware('permission:add_users', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_users', ['only' => ['edit', 'update', 'activate']]);
        $this->middleware('permission:delete_users', ['only' => ['deactivate']]);

        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\UserFilter $filter
     *
     * @return View
     */
    public function index(Request $request, UserFilter $filter)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $results      = $this->service->list($filter, 20, $request->except('page'));

        return view('admin.users.index', compact('results', 'subsidiaries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $roles        = Role::pluck('details', 'id');
        $subsidiaries = Subsidiary::pluck('name', 'id');

        return view('admin.users.create', compact('roles', 'subsidiaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveUser $request
     *
     * @return RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function store(SaveUser $request)
    {
        $userData         = UserData::fromRequest($request);
        $userData->active = true;

        $this->service->createOrUpdate($userData);

        toast()->success('Usuário criado com sucesso.', 'Sucesso');

        return redirect()->route('admin.users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id)
    {
        $roles        = Role::all()->pluck('details', 'id');
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $user         = $this->service->find($id);

        return view('admin.users.edit', compact(
            'user', 'roles', 'subsidiaries'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveUser $request
     * @param int $id
     *
     * @return RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(SaveUser $request, int $id)
    {
        $userData     = UserData::fromRequest($request);
        $userData->id = $id;

        $this->service->createOrUpdate($userData);

        toast()->success('Usuário atualziado com sucesso.', 'Sucesso');

        return redirect()->route('admin.users.index');
    }

    /**
     * Activate the specified user
     *
     * @param User $user
     *
     * @return RedirectResponse
     */
    public function activate(User $user)
    {
        $user->active = true;
        $user->save();

        toast()->success('Usuário desativado com sucesso.', 'Sucesso');

        return redirect()->back();
    }

    /**
     * Deactivate the specified user
     *
     * @param User $user
     *
     * @return RedirectResponse
     */
    public function deactivate(User $user)
    {
        $user->active = false;
        $user->save();

        toast()->success('Usuário desativado com sucesso.', 'Sucesso');

        return redirect()->back();
    }

    /**
     * Send welcome mail to the specified user
     *
     * @param User $user
     *
     * @return RedirectResponse
     */
    public function sendEmail(User $user)
    {
        $user->sendWelcomeNotification();

        toast()->success('E-mail de boas vindas enviado com sucesso.', 'Sucesso');

        return redirect()->back();
    }
}
