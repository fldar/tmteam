<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Core\Filters\PermissionFilter;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SavePermission;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Core\Models\Auth\Permission;
use App\Core\Models\Auth\Role;

class PermissionController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seo()->setTitle('Permissões');

        $this->middleware('permission:view_permissions', ['only' => ['index']]);
        $this->middleware('permission:add_permissions', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_permissions', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_permissions', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @param \App\Core\Filters\PermissionFilter $filter
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request, PermissionFilter $filter)
    {
        $results = Permission::filter($filter)
                             ->latest()
                             ->paginate(20)
                             ->appends($request->except('page'));

        return view('admin.permissions.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $roles = Role::byUser(Auth::user())->pluck('details', 'id');

        return view('admin.permissions.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SavePermission $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SavePermission $request)
    {
        $permission = Permission::create($request->only('name', 'details'));

        $roles = $request->get('roles', []);

        foreach ($roles as $role) {
            $roleName = Role::find($role);

            if (!$roleName->hasPermissionTo($permission->name)) {
                $permission->assignRole($role);
            }
        }

        $permission->save();

        toast()->success('Permissão criada com sucesso.');

        return redirect()->route('admin.permissions.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Core\Models\Auth\Permission $permission
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Permission $permission)
    {
        $roles = Role::byUser(Auth::user())->pluck('details', 'id');

        return view('admin.permissions.edit', compact('permission', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SavePermission $request
     *
     * @param \App\Core\Models\Auth\Permission $permission
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SavePermission $request, Permission $permission)
    {
        $data = $request->validated();
        $permission->fill($data);
        $permission->save();

        $permission->syncRoles($data['roles']);

        toast()->success('Permissão atualizada com sucesso.');

        return redirect()->route('admin.permissions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @param \App\Core\Models\Auth\Permission $permission
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request, Permission $permission)
    {
        $permission->delete();

        $msg = 'Permissão removida com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
