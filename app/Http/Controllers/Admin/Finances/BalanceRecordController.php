<?php

namespace App\Http\Controllers\Admin\Finances;

use App\Core\Services\TrainingCenter\RegistrationService;
use App\Core\Filters\BalanceRecordFilter;
use App\Core\Models\Subsidiary;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveBalanceRecord;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Services\Financial\InstallmentService;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BalanceRecordController extends BaseController
{
    use SEOToolsTrait;

    protected $installmentService;
    protected $registrationService;

    public function __construct(
        InstallmentService $installmentService,
        RegistrationService $registrationService
    ) {
        $this->seo()->setTitle('Lançamentos');

        $this->middleware('permission:view_balance_records', ['only' => ['index']]);
        $this->middleware('permission:add_balance_records', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_balance_records', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_balance_records', ['only' => ['destroy']]);

        $this->installmentService = $installmentService;
        $this->registrationService = $registrationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\BalanceRecordFilter $filter
     *
     * @return View
     */
    public function index(Request $request, BalanceRecordFilter $filter)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $records      = BalanceRecord::filter($filter)
                                     ->latest()
                                     ->paginate(20)
                                     ->appends($request->except('page'));

        return view('admin.balance-records.index', compact(
            'records', 'subsidiaries'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');

        return view('admin.balance-records.create', compact('subsidiaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveBalanceRecord $request
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function store(SaveBalanceRecord $request)
    {
        $data = $request->validated();

        if (empty($data['repeat_times'])) {
            BalanceRecord::create($data);
        } else {
            $this->installmentService->generate($request);
        }

        toast()->success('Registro criado com sucesso.', 'Sucesso');

        return redirect()->route('admin.balance-records.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param BalanceRecord $balanceRecord
     *
     * @return View
     */
    public function edit(BalanceRecord $balanceRecord)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');

        return view('admin.balance-records.edit', compact(
            'balanceRecord', 'subsidiaries'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveBalanceRecord $request
     * @param BalanceRecord $balanceRecord
     *
     * @return RedirectResponse
     */
    public function update(SaveBalanceRecord $request, BalanceRecord $balanceRecord)
    {
        $data = $request->validated();

        $balanceRecord->fill($data)->save();

        if($balanceRecord->paid() && $balanceRecord->isInvoice()){
            $this->registrationService->checkForIssues($balanceRecord->chargeable);
        }

        toast()->success('Registro modificado com sucesso.', 'Sucesso');

        return redirect()->route('admin.balance-records.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param BalanceRecord $balanceRecord
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(Request $request, BalanceRecord $balanceRecord)
    {
        $balanceRecord->delete();

        $msg = 'Registro removido com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
