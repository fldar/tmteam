<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\Models\Media;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Http\Request;

class AttachmentController extends Controller
{
    use SEOTools;

    public function show(Media $attachment)
    {
        $this->seo()->setTitle('Anexos');

        return view('admin.students.attachments.show', compact('attachment'));
    }

    public function update(Request $request, Media $attachment)
    {
        $attachment->validateAttachment($request->get('validation'));
        $attachment->save();

        toast()->success('Anexo validado com sucesso.', 'Sucesso');

        return redirect()->route('admin.students.index');
    }
}
