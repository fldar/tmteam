<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\Services\TrainingCenter\RegistrationService;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveBalanceRecord;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\TrainingCenter\Registration;
use App\Core\Services\Financial\InstallmentService;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class InstallmentController extends BaseController
{
    use SEOToolsTrait;

    protected InstallmentService $installmentService;
    protected RegistrationService $registrationService;

    public function __construct(
        InstallmentService $installmentService,
        RegistrationService $registrationService
    ) {
        $this->seo()->setTitle('Parcelas');

        $this->middleware('permission:view_balance_records', ['only' => ['index']]);
        $this->middleware('permission:add_balance_records', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_balance_records', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_balance_records', ['only' => ['destroy']]);

        $this->installmentService  = $installmentService;
        $this->registrationService = $registrationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param Registration $registration
     *
     * @return View
     */
    public function index(Request $request, Registration $registration)
    {
        $records = $registration->installments()
                                ->paginate(12)
                                ->appends($request->except('page'));

        return view('admin.students.installments.index',
            compact('records', 'registration')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Registration $registration
     *
     * @return View
     */
    public function create(Registration $registration)
    {
        return view('admin.students.installments.create',
            compact('registration')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Registration $registration
     * @param BalanceRecord $installment
     *
     * @return View
     */
    public function edit(
        Registration $registration,
        BalanceRecord $installment
    ) {
        return view('admin.students.installments.edit',
            compact('installment', 'registration')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveBalanceRecord $request
     *
     * @param Registration $registration
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function store(SaveBalanceRecord $request, Registration $registration)
    {
        $data = $request->validated();

        if (empty($data['repeat_times'])) {
            $record = new BalanceRecord($data);
            $registration->installments()->save($record);
        } else {
            $this->installmentService->generate($request, $registration);
        }

        toast()->success('Registro criado com sucesso.', 'Sucesso');

        return redirect()->route('admin.students.installments.index', [
            'registration' => $registration->id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveBalanceRecord $request
     * @param Registration $registration
     * @param \App\Core\Models\Financial\BalanceRecord $installment
     *
     * @return RedirectResponse
     */
    public function update(
        SaveBalanceRecord $request,
        Registration $registration,
        BalanceRecord $installment
    ) {
        $data = $request->validated();

        $installment->fill($data)->save();

        if ($installment->paid()) {
            $this->registrationService->checkForIssues($registration);
        }

        toast()->success('Registro modificado com sucesso.', 'Sucesso');

        return redirect()->route('admin.students.installments.index', [
            'registration' => $registration->id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Registration $registration
     * @param \App\Core\Models\Financial\BalanceRecord $installment
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws \Exception
     */
    public function destroy(
        Request $request,
        Registration $registration,
        BalanceRecord $installment
    ) {
        $installment->delete();
        $msg = 'Registro removido com sucesso.';

        if ($request->ajax()) {
            return response()
                ->json([
                        'message' => $msg,
                        'route'   => route(
                            'admin.students.installments.index',
                            ['registration' => $registration->id])]
                    , 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
