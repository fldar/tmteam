<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Http\Requests\SaveGraduation;
use App\Core\Models\TrainingCenter\Band;
use App\Core\Models\TrainingCenter\Degree;
use App\Core\Models\TrainingCenter\Graduation;
use App\Core\Models\TrainingCenter\Registration;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class GraduationController extends Controller
{
    use SEOToolsTrait;

    private $modalities;
    private $teachers;
    private $bands;
    private $degrees;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seo()->setTitle('Graduações');

        $this->middleware('permission:view_graduations', ['only' => ['index']]);
        $this->middleware('permission:add_graduations', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_graduations', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_graduations', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Registration $registration
     *
     * @return View
     */
    public function index(Registration $registration)
    {
        $results = $registration->graduations()
                                ->with(['modality', 'band', 'degree'])
                                ->latest('graduated_on')
                                ->paginate(25);

        return view('admin.students.graduations.index',
            compact('results', 'registration')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @param Registration $registration
     *
     * @return VIew
     */
    public function create(Request $request, Registration $registration)
    {
        $this->setOptions($registration);

        $graduation = new Graduation();

        if ($request->filled('last')) {
            $last = Graduation::find($request->get('last'));
            $nextDegree = $last->nextDegree();

            if ($nextDegree) {
                $graduation->fill([
                    'modality_id' => $last->modality_id,
                    'band_id'     => $nextDegree->band_id,
                    'degree_id'   => $nextDegree->id
                ]);
            }
        }

        return view('admin.students.graduations.create')->with([
            'graduation'   => $graduation,
            'registration' => $registration,
            'modalities'   => $this->modalities,
            'bands'        => $this->bands,
            'teachers'     => $this->teachers,
            'degrees'      => $this->degrees
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveGraduation $request
     * @param Registration $registration
     *
     * @return RedirectResponse
     */
    public function store(SaveGraduation $request, Registration $registration)
    {
        $data = $request->validated();

        Graduation::create($data);

        toast()->success('Graduação criada com sucesso.', 'Sucesso');

        return redirect()->route('admin.students.graduations.index', [
            'registration_id' => $registration->id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Registration $registration
     * @param Graduation $graduation
     *
     * @return View
     */
    public function edit(Registration $registration, Graduation $graduation)
    {
        $this->setOptions($registration);

        return view('admin.students.graduations.edit')->with([
            'graduation'   => $graduation,
            'registration' => $registration,
            'modalities'   => $this->modalities,
            'bands'        => $this->bands,
            'teachers'     => $this->teachers,
            'degrees'      => $this->degrees
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveGraduation $request
     * @param Registration $registration
     * @param Graduation $graduation
     *
     * @return RedirectResponse
     */
    public function update(SaveGraduation $request,
                           Registration $registration,
                           Graduation $graduation)
    {
        $data = $request->validated();

        $graduation->fill($data)->save();

        toast()->success('Graduação atualizada com sucesso.', 'Sucesso');

        return redirect()->route('admin.students.graduations.index', [
            'registration_id' => $registration->id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Registration $registration
     * @param Graduation $graduation
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(Request $request,
                            Registration $registration,
                            Graduation $graduation)
    {
        $graduation->delete();

        $msg = 'Graduação removida com sucesso.';

        if ($request->ajax()) {
            return response()->json([
                'message' => $msg,
                'route'   => route('admin.students.graduations.index', [
                    'registration' => $registration->id
                ])
            ], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }

    public function print(Registration $registration, Graduation $graduation)
    {
        return view('admin.students.graduations.certificate',
            compact('registration', 'graduation')
        );
    }

    private function setOptions(Registration $registration)
    {
        $classrooms = $registration->classrooms()
                                   ->with('modality', 'teachers')
                                   ->get();

        $this->modalities = collect($classrooms->map(function ($classroom) {
            return $classroom->modality;
        }))->pluck('name', 'id');

        $teachers = [];

        $classrooms->map(function ($classroom) use (&$teachers) {
            foreach ($classroom->teachers as $teacher) {
                $teachers[ $teacher->id ] = $teacher->user->name;
            };
        });

        $this->teachers = $teachers;
        $this->bands = Band::whereIn('modality_id',
            array_keys($this->modalities->toArray())
        )->get();

        $this->degrees = Degree::whereIn('band_id',
            $this->bands->pluck('id')
        )->get();

    }
}
