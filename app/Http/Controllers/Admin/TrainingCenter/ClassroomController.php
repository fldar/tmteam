<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\Models\Subsidiary;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Core\Filters\ClassroomFilter;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveClassroom;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Teacher;
use App\Core\Models\TrainingCenter\Modality;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Exception;

class ClassroomController extends BaseController
{
    use SEOToolsTrait;

    public function __construct()
    {
        $this->seo()->setTitle('Turmas');

        $this->middleware('permission:view_classrooms', ['only' => ['index']]);
        $this->middleware('permission:add_classrooms', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_classrooms', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_classrooms', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\ClassroomFilter $filter
     *
     * @return View
     */
    public function index(Request $request, ClassroomFilter $filter)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $results      = Classroom::filter($filter)->latest()
                                 ->paginate(20)
                                 ->appends($request->except('page'));

        return view('admin.classrooms.index', compact(
            'results', 'subsidiaries'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $teachers     = Teacher::with('user')
                               ->get()
                               ->pluck('user.name', 'id');

        $modalities = Modality::asc()
                              ->pluck('name', 'id');

        return view('admin.classrooms.create', compact(
            'teachers', 'modalities', 'subsidiaries'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveClassroom $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(SaveClassroom $request)
    {
        DB::beginTransaction();

        $data      = $request->validated();
        $classroom = Classroom::create($request->validated());

        if ($classroom) {

            $classroom->teachers()->attach($data['teachers']);

            DB::commit();

            toast()->success('Turma criada com sucesso.', 'Sucesso');

            return redirect()->route('admin.classrooms.index');
        }

        DB::rollBack();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Classroom $classroom
     *
     * @return View
     */
    public function edit(Classroom $classroom)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $teachers     = Teacher::with('user')
                               ->get()
                               ->pluck('user.name', 'id');

        $modalities = Modality::asc()
                              ->pluck('name', 'id');

        return view('admin.classrooms.edit', compact(
            'classroom', 'teachers', 'modalities', 'subsidiaries'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveClassroom $request
     * @param Classroom $classroom
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(SaveClassroom $request, Classroom $classroom)
    {
        DB::beginTransaction();

        $data = $request->validated();

        if ($classroom->fill($data)->save()) {
            $classroom->teachers()->sync($data['teachers']);

            DB::commit();

            toast()->success('Turma atualizada com sucesso.', 'Sucesso');

            return redirect()->route('admin.classrooms.index');
        }

        DB::rollBack();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Classroom $classroom
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(Request $request, Classroom $classroom)
    {
        $classroom->delete();

        $msg = 'Turma removida com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
