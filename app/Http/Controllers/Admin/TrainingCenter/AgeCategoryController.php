<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveAgeCategory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Core\Models\TrainingCenter\AgeCategory;
use Illuminate\View\View;

class AgeCategoryController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seo()->setTitle("Categorias de idade");

        $this->middleware('permission:view_bands_submodules', ['only' => ['index']]);
        $this->middleware('permission:add_bands_submodules', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_bands_submodules', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_bands_submodules', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return View
     */
    public function index(Request $request)
    {
        $results = AgeCategory::latest()
                              ->paginate(20)
                              ->appends($request->except('page'));

        return view('admin.age-categories.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.age-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveAgeCategory $request
     *
     * @return RedirectResponse
     */
    public function store(SaveAgeCategory $request)
    {
        $data = $request->validated();

        AgeCategory::create($data);

        toast()->success('Categoria criada com sucesso.', 'Sucesso');

        return redirect()->route('admin.age-categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AgeCategory $ageCategory
     *
     * @return View
     */
    public function edit(AgeCategory $ageCategory)
    {
        return view('admin.age-categories.edit', compact('ageCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveAgeCategory $request
     * @param AgeCategory $ageCategory
     *
     * @return RedirectResponse
     */
    public function update(SaveAgeCategory $request, AgeCategory $ageCategory)
    {
        $data = $request->validated();
        $ageCategory->fill($data)->save();

        toast()->success('Categoria atualizada com sucesso.', 'Sucesso');

        return redirect()->route('admin.age-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param AgeCategory $ageCategory
     *
     * @return RedirectResponse|JsonResponse
     * @throws Exception
     */
    public function destroy(Request $request, AgeCategory $ageCategory)
    {
        $ageCategory->delete();

        $message = 'Categoria removida com sucesso.';

        if ($request->ajax()) {
            return response()->json([
                'message' => $message,
                'route' => route('admin.age-categories.index')
            ], 200);
        }

        toast()->success($message, 'Success');

        return redirect()->back();
    }
}
