<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Core\Filters\DashboardFilter;
use App\Core\Models\Report\ActiveStudents;
use App\Core\Models\Report\BillingPerPaymentType;
use App\Core\Models\Report\MonthlyRegistrations;
use App\Core\Models\Report\StudentsPerStatus;
use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Registration;
use App\Core\Models\Report\BillingSummary;
use App\Core\Models\Report\Balance;
use App\Enums\RegistrationStatusEnum;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\View\View;

class FinancialController extends Controller
{

    use SEOTools;

    public function __construct()
    {
        $this->middleware('permission:view_finances_dashboard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $this->seo()->setTitle('Financeiro');

        $balance        = Balance::currentMonth()->first();
        $registrations  = Registration::currentMonth()->count();
        $activeStudents = ActiveStudents::currentMonth()->first();

        $subsidiaries = Subsidiary::pluck('name', 'id');
        $classrooms = Classroom::pluck('name', 'id');

        return view('admin.dashboard.financial.index',
            compact('balance', 'registrations', 'activeStudents', 'classrooms', 'subsidiaries')
        );
    }

    public function income()
    {
        $report = Balance::lastYear()->pluck('total_received');

        return response()->json($report, 200);
    }

    public function outcome()
    {
        $report = Balance::lastYear()->pluck('total_spent');

        return response()->json($report, 200);
    }

    public function activeStudents()
    {
        $report = ActiveStudents::take(12)
                                ->get()
                                ->pluck('students');

        return response()->json($report, 200);
    }

    public function registrations()
    {
        $report = MonthlyRegistrations::take(12)->pluck('registries');

        return response()->json($report, 200);
    }

    public function billingSummary(DashboardFilter $filter)
    {
        $report = BillingSummary::filter($filter)->get()->resume();

        return response()->json($report, 200);
    }

    public function billingPerPaymentType(DashboardFilter $filter)
    {
        $report = BillingPerPaymentType::filter($filter)
                                       ->get()
                                       ->resume()
                                       ->values();

        return response()->json($report, 200);
    }

    public function studentsByStatus(DashboardFilter $filter)
    {
        $types  = ['0' => 0] + RegistrationStatusEnum::options();
        $values = array_fill_keys(array_keys($types), 0);
        $report = StudentsPerStatus::filter($filter)
                                   ->get()
                                   ->resume()
                                   ->union($values)
                                   ->sortKeys()
                                   ->values();

        return response()->json($report, 200);
    }
}
