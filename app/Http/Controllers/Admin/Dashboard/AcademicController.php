<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Core\Filters\DashboardFilter;
use App\Core\Models\Report\StudentsPerAgeCategories;
use App\Core\Models\Report\StudentsPerBands;
use App\Core\Models\Report\StudentsPerSex;
use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Classroom;
use Artesaos\SEOTools\Traits\SEOTools;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class AcademicController extends Controller
{
    use SEOTools;

    public function __construct()
    {
        $this->middleware('permission:view_finances_dashboard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $this->seo()->setTitle('Acadêmico');

        $subsidiaries = Subsidiary::pluck('name', 'id');
        $classrooms = Classroom::pluck('name', 'id');

        return view('admin.dashboard.academic.index',
            compact('classrooms', 'subsidiaries')
        );
    }

    public function studentsPerSex(DashboardFilter $filter)
    {
        $report = StudentsPerSex::filter($filter)
                                ->get()
                                ->resume()
                                ->sortKeys()
                                ->values();

        return response()->json($report, 200);
    }

    public function studentsPerAgeCategories(DashboardFilter $filter)
    {
        $report = StudentsPerAgeCategories::filter($filter)
                                          ->get()
                                          ->resume()
                                          ->toArray();

        return response()->json($report, 200);
    }

    public function studentsPerBands(DashboardFilter $filter)
    {
        $report = StudentsPerBands::filter($filter)
                                          ->get()
                                          ->resume()
                                          ->toArray();

        return response()->json($report, 200);
    }
}
