<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Core\Models\Auth\User;
use App\Http\Controllers\Admin\BaseController;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\View\View;

class DashboardController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $this->seo()->setTitle('Dashboard');

        $birthDays = User::bySubsidiary()
                         ->whereMonth('birthdate', date('m'))
                         ->whereDay('birthdate', '>=', date('d'))
                         ->get()
                         ->sortBy('day_of_birth');

        return view('admin.dashboard.index', compact('birthDays'));
    }
}
