<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Core\Filters\BillingPerClassFilter;
use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\Report\BillingPerClass;
use Artesaos\SEOTools\Traits\SEOTools;
use App\Http\Controllers\Controller;

class BillingPerClassController extends Controller
{

    use SEOTools;

    public function index(BillingPerClassFilter $filter)
    {
        $this->seo()->setTitle('Relatório de faturamento por turma');

        $classrooms   = Classroom::pluck('name', 'id');
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $reports      = BillingPerClass::filter($filter)
                                       ->take(12)
                                       ->get();

        return view('admin.reports.class-billing',
            compact('reports', 'classrooms', 'subsidiaries')
        );
    }
}
