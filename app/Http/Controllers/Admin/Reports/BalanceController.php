<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Core\Filters\BalanceFilter;
use App\Core\Models\Report\Balance;
use App\Core\Models\Subsidiary;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BalanceController extends Controller
{

    use SEOToolsTrait;

    public function index(Request $request, BalanceFilter $filter)
    {
        $this->seo()->setTitle('Relatório de Receitas X Despesas');

        $subsidiaries = Subsidiary::pluck('name', 'id');
        $reports      = Balance::filter($filter)
                               ->withoutGlobalScope('subsidiary')
                               ->paginate(12)
                               ->appends($request->except('page'));

        return view('admin.reports.balance', compact('reports', 'subsidiaries'));
    }
}
