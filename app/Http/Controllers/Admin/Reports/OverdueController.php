<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Core\Filters\OverdueFilter;
use App\Core\Models\Report\Overdue;
use App\Core\Models\Subsidiary;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OverdueController extends Controller
{
    use SEOToolsTrait;

    public function index(Request $request, OverdueFilter $filter)
    {
        $this->seo()->setTitle('Relatório de inadimplência');

        $subsidiaries = Subsidiary::pluck('name', 'id');
        $records      = Overdue::filter($filter)
                               ->paginate(20)
                               ->appends($request->except('page'));

        return view('admin.reports.overdue', compact('records', 'subsidiaries'));
    }
}
