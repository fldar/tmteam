<?php

namespace App\Http\Controllers\Admin\Juno;

use App\Core\Filters\InvoiceFilter;
use App\Core\Models\Subsidiary;
use App\Core\Services\Financial\BalanceRecordService;
use App\Core\Services\Juno\AccountNotVerifiedException;
use App\Core\Services\Juno\ResponseErrorException;
use App\Http\Controllers\Admin\BaseController;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\TrainingCenter\Registration;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class InvoiceController extends BaseController
{
    use SEOToolsTrait;

    protected BalanceRecordService $service;

    public function __construct(BalanceRecordService $service)
    {
        $this->seo()->setTitle('Faturas');

        $this->middleware('permission:view_balance_records', ['only' => ['index']]);
        $this->middleware('permission:add_balance_records', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_balance_records', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_balance_records', ['only' => ['destroy']]);

        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\InvoiceFilter $filter
     *
     * @return View
     */
    public function index(Request $request, InvoiceFilter $filter)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $records      = BalanceRecord::filter($filter)
                                     ->invoice()
                                     ->inTime()
                                     ->orderBy('due_date', 'ASC')
                                     ->paginate(20)
                                     ->appends($request->except('page'));

        return view('admin.invoices.index', compact('records', 'subsidiaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'balance_records' => 'required|array|min:1'
        ]);

        try {

            foreach ($data['balance_records'] as $id) {
                $this->service->charge($id);
            }

            toast()->success('Registros faturados com sucesso.', 'Sucesso');

        } catch (ResponseErrorException $ex){
            toast()->warning($ex->getMessage());

            Session::flash('gateway_messages', $ex->errorBag);
        } catch (AccountNotVerifiedException $ex){

            toast()->warning($ex->getMessage());

        }

        return redirect()->route('admin.invoices.index');
    }
}
