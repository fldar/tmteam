<?php

namespace App\Http\Controllers\Admin\Juno;

use App\Core\Filters\SplitReportFilter;
use App\Core\Models\Report\SplitPerSubsidiary;
use App\Core\Models\Subsidiary;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SplitReportController extends Controller
{
    use SEOToolsTrait;

    public function index(Request $request, SplitReportFilter $filter)
    {
        $this->seo()->setTitle('Split');

        $subsidiaries = Subsidiary::pluck('name', 'id');
        $reports      = SplitPerSubsidiary::filter($filter)
                               ->paginate(12)
                               ->appends($request->except('page'));

        return view('admin.split.index', compact('reports', 'subsidiaries'));
    }
}
