<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Carbon\Carbon;

class UserEventListener
{
    /**
     * Handle the event.
     *
     * @param Login $event
     * @return void
     */
    public function handleLogin(Login $event)
    {
        $user = $event->user;
        $user->latest_login = Carbon::now();
        $user->save();
    }
}
