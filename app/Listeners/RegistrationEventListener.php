<?php


namespace App\Listeners;


use App\Events\RegistrationHasBeenCanceled;
use App\Core\Models\Auth\User;
use App\Core\Models\TrainingCenter\Student;
use App\Notifications\Admin\MemberCancellation;
use App\Notifications\Member\Registration\RegistrationCanceled;

class RegistrationEventListener
{
    /**
     * Handle the event.
     *
     * @param  RegistrationHasBeenCanceled  $event
     * @return void
     */
    public function handleCancellation(RegistrationHasBeenCanceled $event)
    {
        $registration = $event->registration;
        $student = $registration->student;
        $member = $student->user;

        $member->active = false;
        $member->save();
        $member->notify(new RegistrationCanceled());

        $this->notifyAdminsAboutCancellation($student);
    }

    private function notifyAdminsAboutCancellation(Student $student)
    {
        $admins = User::byRole(['root', 'gestor'])
                      ->where('receive_messages', 1)
                      ->get();

        foreach ($admins as $admin) {
            $admin->notify(new MemberCancellation($student));
        }
    }
}
