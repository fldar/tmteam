<?php

namespace App\Listeners;

use App\Enums\AttachmentStatusEnum;
use App\Events\MediaHasBeenValidated;
use App\Core\Models\Auth\User;
use App\Core\Models\TrainingCenter\Registration;
use App\Notifications\Admin\MediaHasBeenAdded as MediaHasBeenAddedNotification;
use App\Notifications\Member\MediaIsInvalid;
use App\Core\Services\TrainingCenter\RegistrationService;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\Events\MediaHasBeenAdded;

class MediaEventListener
{

    protected RegistrationService $registrationService;

    /**
     * MediaEventListener constructor.
     *
     * @param \App\Core\Services\TrainingCenter\RegistrationService $registrationService
     */
    public function __construct(RegistrationService $registrationService)
    {
        $this->registrationService = $registrationService;
    }


    public function handleMediaHasBeenAdded(MediaHasBeenAdded $event)
    {
        $createdBy = Auth::user();
        $media = $event->media;
        $validation = $media->getCustomProperty('validation');


        if ($validation && $validation == AttachmentStatusEnum::PENDING) {
            $admins = User::byRole(['root', 'gestor'])
                          ->where('receive_messages', 1)
                          ->get();

            foreach ($admins as $admin) {
                $admin->notify(
                    new MediaHasBeenAddedNotification($createdBy, $media)
                );
            }
        }
    }

    public function handleMediaHasBeenValidated(MediaHasBeenValidated $event)
    {
        $media      = $event->media;
        $validation = $media->getCustomProperty('validation');
        $user       = $this->retrieveUserFromMediaModel($media->model);

        if ($validation == AttachmentStatusEnum::INVALID) {
            $user->notify(new MediaIsInvalid($media));
        }

        $this->registrationService->checkForIssues($user->student->registration);
    }

    private function retrieveUserFromMediaModel($model)
    {
        if($model instanceof User){
            return $model;
        }

        if($model instanceof Registration){
            return $model->student->user;
        }
    }
}
