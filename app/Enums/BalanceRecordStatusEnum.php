<?php


namespace App\Enums;


class BalanceRecordStatusEnum
{
    const CREATED  = 1;
    const BILLED   = 2;
    const PAID     = 3;
    const UNPAID   = 4;
    const CANCELED = 5;

    public function option()
    {
        return [
            self::CREATED  => 'Criado',
            self::BILLED   => 'Faturado',
            self::PAID     => 'Pago',
            self::UNPAID   => 'Atrasado',
            self::CANCELED => 'Cancelado'
        ];
    }
}