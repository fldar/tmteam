<?php


namespace App\Enums;

class PaymentTypesEnum
{
    const INVOICE     = 1;
    const CREDIT_CARD = 2;
    const CASH        = 3;
    const SCHOLARSHIP = 4;

    public static function options()
    {
        return [
            self::INVOICE     => 'Boleto',
            self::CREDIT_CARD => 'Cartão de crédito',
            self::CASH        => 'Dinheiro',
            self::SCHOLARSHIP => 'Bolsista'
        ];
    }

}