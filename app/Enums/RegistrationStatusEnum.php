<?php


namespace App\Enums;


class RegistrationStatusEnum
{
    const REGULAR      = 1;
    const IRREGULAR    = 2;
    const CANCELLED    = 3;

    public static function options()
    {
        return [
            self::REGULAR      => 'Regular',
            self::IRREGULAR    => 'Pendência',
            self::CANCELLED    => 'Cancelado',
        ];
    }
}