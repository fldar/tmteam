<?php


namespace App\Enums;

class PaymentSituationEnum
{
    const PAID = 1;
    const PENDENT = 2;
    const OVERDUE = 3;

    public static function options()
    {
        return [
            self::PAID    => 'Pago',
            self::PENDENT => 'Pendente',
            self::OVERDUE => 'Atrasado'
        ];
    }

}