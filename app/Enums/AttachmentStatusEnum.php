<?php


namespace App\Enums;


class AttachmentStatusEnum
{
    const PENDING = 1;
    const INVALID = 2;
    const VALID   = 3;
}