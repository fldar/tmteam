<?php


namespace App\Notifications\Member;


use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class HealthCertificateIsAboutToExpire extends Notification
{
    private $daysToExpire;

    public function __construct($daysToExpire)
    {
        $this->daysToExpire = $daysToExpire;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(config('app.name') . ' | Atestado médico expirando')
            ->markdown('students.notifications.registration.fisic-expiring',
                [
                    'daysToExpire' => $this->daysToExpire,
                    'url'          => route('students.accounts.edit')
                ]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $message = "Atencão, o atestado médico anexado expira em " .
            "$this->daysToExpire dias, clique para anexar um novo";

        return [
            'subject' => 'Atestado médico expirando',
            'message' => $message,
            'link'    => route('students.accounts.edit')
        ];
    }
}