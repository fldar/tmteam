<?php

namespace App\Notifications\Admin;

use App\Core\Models\TrainingCenter\Student;
use Illuminate\Notifications\Notification;

class MemberCancellation extends Notification
{
    /**
     * @var Student
     */
    private $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $user = $this->student->user;
        $registration = $this->student->registration;

        return [
            'subject' => "Matrícula cancelada",
            'message' => "O aluno $user->name, está inadimplente há mais de 15 dias",
            'link'    => route('admin.students.installments.index', [
                'registration' => $registration->id
            ])
        ];
    }
}
