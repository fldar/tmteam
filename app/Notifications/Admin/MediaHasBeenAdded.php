<?php

namespace App\Notifications\Admin;

use App\Core\Models\Auth\User;
use App\Core\Models\Media;
use Illuminate\Notifications\Notification;

class MediaHasBeenAdded extends Notification
{
    /**
     * @var User
     */
    private $from;

    /**
     * @var Media
     */
    private $media;

    public function __construct(User $from, Media $media)
    {
        $this->from = $from;
        $this->media = $media;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $fromName = $this->from->nickname;
        $type = $this->media->human_readable_type;
        $id = $this->media->id;

        return [
            'subject' => "$fromName adicionou um novo anexo",
            'message' => "Atenção um novo $type foi adicionado, clique para interagir",
            'link'    => route('admin.students.attachments.show', ['media' => $id]),
        ];
    }
}
