<?php

namespace App\Notifications\Users;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WelcomeUser extends Notification
{
    private $user;
    private $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $user)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $url = route('auth.activation.create', ['token' => $this->token]);

        return (new MailMessage)
            ->subject(config('app.name') . ' | Bem Vindo')
            ->markdown(
                'admin.notifications.users.welcome',
                ['user' => $this->user, 'url' => $url]
            );
    }
}
