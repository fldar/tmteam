<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('orders', function (Blueprint $table) {
      $table->increments('id');
      $table->string('code', 255)->unique();
      $table->string('code_gateway', 255)->unique()->nullable();
      $table->string('first_name', 255);
      $table->string('last_name', 255);
      $table->text('payNumber')->nullable();
      $table->text('boleto_url')->nullable();
      $table->text('checkout_Url')->nullable();
      $table->text('installmentLink')->nullable();
      $table->string('registration', 15)->unique();
      $table->string('cpf', 80);
      $table->text('description');
      $table->float('amount', 10, 2);
      $table->unsignedInteger('type');
      $table->unsignedInteger('status');
      $table->unsignedInteger('student_id')->index();
      $table->foreign('student_id')->references('id')->on('students');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('orders');
  }
}
