<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryRelationToTables extends Migration
{

    protected $tables = [
        'balance_records',
        'classrooms',
        'messages',
        'users',
        'registrations'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table){
            Schema::table($table, function (Blueprint $table) {
                $table->unsignedInteger('subsidiary_id')->index()->default(1);
                $table->foreign('subsidiary_id')->references('id')
                                                ->on('subsidiaries')
                                                ->onDelete('restrict');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table){
            Schema::table($table, function (Blueprint $table) {
                $table->dropForeign(['subsidiary_id']);
                $table->dropColumn('subsidiary_id');
            });
        }


    }
}
