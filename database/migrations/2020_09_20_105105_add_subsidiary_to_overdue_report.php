<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryToOverdueReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW overdue_report AS
                SELECT
                    p.name AS `student_name`,
                    p.email AS `student_email`,
                    GROUP_CONCAT(DATE_FORMAT(due_date, '%d/%m/%Y')) AS overdues,
                    r.subsidiary_id AS `subsidiary_id`
                FROM students s
                         INNER JOIN people p ON s.person_id = p.id
                         INNER JOIN registrations r ON s.id = r.student_id
                         INNER JOIN balance_records br ON br.chargeable_id = r.id AND
                                                          br.chargeable_type like '%Registration'
                WHERE br.due_date < CURDATE() AND (br.payment_date IS NULL OR br.payment_date = '0000-00-00')
                GROUP BY `student_name`, `student_email`;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            CREATE OR REPLACE VIEW overdue_report AS
                SELECT 
                        p.name AS `student_name`,
                        p.email AS `student_email`,
                        GROUP_CONCAT(DATE_FORMAT(due_date, '%d/%m/%Y')) AS overdues
                FROM students s
                INNER JOIN people p ON s.person_id = p.id
                INNER JOIN registrations r ON s.id = r.student_id
                INNER JOIN balance_records br ON br.chargeable_id = r.id AND
                                              br.chargeable_type like '%Registration'
                WHERE br.due_date < CURDATE() AND (br.payment_date IS NULL OR br.payment_date = '0000-00-00')
                GROUP BY `student_name`, `student_email`
        ");
    }
}
