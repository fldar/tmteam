<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTermResponsibleForGuardian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('responsibles', 'guardians');

        Schema::table('students', function (Blueprint $table) {
            $table->dropForeign('students_responsible_id_foreign');
            $table->renameColumn('responsible_id', 'guardian_id');

            $table->foreign('guardian_id')
                  ->references('id')
                  ->on('guardians')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('guardians', 'responsibles');

        Schema::table('students', function (Blueprint $table) {
            $table->dropForeign('students_guardian_id_foreign');
            $table->renameColumn('guardian_id', 'responsible_id');

            $table->foreign('responsible_id')
                  ->references('id')
                  ->on('responsibles')
                  ->onDelete('set null');
        });
    }
}
