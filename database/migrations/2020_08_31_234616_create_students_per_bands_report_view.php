<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsPerBandsReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE OR REPLACE VIEW students_per_bands_report AS
            SELECT date,
                   rf.registration_id,
                   rf.classroom_id,
                   g.id   AS graduation_id,
                   g.band_id,
                   b.name AS band,
                   b.color AS color
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id
                     INNER JOIN graduations g ON rf.graduation_id = g.id
                     INNER JOIN bands b ON g.band_id = b.id;

        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW students_per_band_report');
    }
}
