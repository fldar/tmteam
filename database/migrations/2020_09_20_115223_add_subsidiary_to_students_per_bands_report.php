<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryToStudentsPerBandsReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW students_per_bands_report AS
            SELECT  date,
                   rf.registration_id,
                   c.id            AS classroom_id,
                   g.id            AS graduation_id,
                   g.band_id       AS band_id,
                   b.name          AS band,
                   b.color         AS color,
                   r.subsidiary_id AS subsidiary_id
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id
                     INNER JOIN registrations_classrooms rc ON r.id = rc.registration_id
                     INNER JOIN classrooms c ON rc.classroom_id = c.id
                     INNER JOIN graduations g ON rf.graduation_id = g.id
                     INNER JOIN bands b ON g.band_id = b.id;
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('
            CREATE OR REPLACE VIEW students_per_bands_report AS
            SELECT date,
                   rf.registration_id,
                   rf.classroom_id,
                   g.id   AS graduation_id,
                   g.band_id,
                   b.name AS band,
                   b.color AS color
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id
                     INNER JOIN graduations g ON rf.graduation_id = g.id
                     INNER JOIN bands b ON g.band_id = b.id;

        ');
    }
}
