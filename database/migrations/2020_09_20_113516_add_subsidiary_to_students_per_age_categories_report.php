<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryToStudentsPerAgeCategoriesReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW students_per_age_categories_report AS
            SELECT date,
                   r.id            AS registration_id,
                   c.id            AS classroom_id,
                   age_category_id,
                   ac.name         AS age_category,
                   r.subsidiary_id AS subsidiary_id
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id
                     INNER JOIN registrations_classrooms rc ON r.id = rc.registration_id
                     INNER JOIN classrooms c ON rc.classroom_id = c.id
                     INNER JOIN students s ON r.student_id = s.id
                     INNER JOIN ages_categories ac ON s.age_category_id = ac.id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('
            CREATE OR REPLACE VIEW students_per_age_categories_report AS
            SELECT date,
                   registration_id,
                   classroom_id,
                   age_category_id,
                   ac.name AS age_category
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id
                     INNER JOIN students s ON r.student_id = s.id
                     INNER JOIN ages_categories ac ON s.age_category_id = ac.id;
        ');
    }
}
