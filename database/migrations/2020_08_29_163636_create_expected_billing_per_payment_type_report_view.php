<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpectedBillingPerPaymentTypeReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE OR REPLACE VIEW expected_billing_per_payment_type_report AS
            SELECT br.due_date AS date,
                   SUM(br.amount) AS expected_value,
                   br.payment_type,
                   c.id      AS classroom_id,
                   c.name    AS classroom
            FROM balance_records br
                     INNER JOIN registrations r
                                ON br.chargeable_type LIKE "%Registration" AND
                                   br.chargeable_id = r.id
                     INNER JOIN registrations_classrooms rc ON r.id = rc.registration_id
                     INNER JOIN classrooms c ON rc.classroom_id = c.id
            WHERE br.payment_type <> 4
            GROUP BY due_date, payment_type, classroom_id
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW expected_billing_per_payment_type_report');
    }
}
