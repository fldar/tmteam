<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryToActiveStudentsReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW active_students_report AS
                SELECT count(distinct registration_id) AS `students`,
                       MONTH(registrations_frequencies.date) AS `competence_month`,
                       YEAR(registrations_frequencies.date) AS `competence_year`,
                       registrations.subsidiary_id AS `subsidiary_id`
                FROM registrations_frequencies
                         INNER JOIN registrations ON registrations_frequencies.registration_id = registrations.id
                WHERE reviewed = true
                GROUP BY competence_year, competence_month, registration_id
                ORDER BY competence_year DESC, competence_month DESC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('
            CREATE OR REPLACE VIEW active_students_report AS
                SELECT count(distinct registration_id) AS students,
                    MONTH(registrations_frequencies.date) AS competence_month,
                    YEAR(registrations_frequencies.date) AS competence_year
                FROM registrations_frequencies WHERE reviewed = true
                GROUP BY competence_year, competence_month
                ORDER BY competence_year DESC, competence_month DESC
        ');
    }
}
