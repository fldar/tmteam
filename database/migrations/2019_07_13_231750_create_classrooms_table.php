<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 255);
            $table->string('name', 255)->unique();
            $table->boolean('active')->nullable()->default(0);
            $table->unsignedInteger('modality_id')->index();
            $table->foreign('modality_id')->references('id')->on('modalities')->onDelete('restrict');
            $table->timestamps();
        });

        Schema::create('classrooms_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('classroom_id')->index();
            $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('restrict');
            $table->unsignedInteger('teacher_id')->index();
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classrooms_frequencies');
        Schema::dropIfExists('classrooms_students');
        Schema::dropIfExists('classrooms');
    }
}
