<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPeopleAndRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table){
            $table->dropForeign(['person_id']);
            $table->dropIndex(['person_id']);
            $table->dropColumn('person_id');
        });

        Schema::table('students', function (Blueprint $table){
            $table->dropForeign(['person_id']);
            $table->dropIndex(['person_id']);
            $table->dropColumn('person_id');
        });

        Schema::table('guardians', function (Blueprint $table){
            $table->dropForeign('responsibles_person_id_foreign');
            $table->dropIndex('responsibles_person_id_index');
            $table->dropColumn('person_id');
        });

        Schema::drop('people');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('nickname', 50);
            $table->string('email', 100)->unique();
            $table->char('cpf', 11)->nullable();
            $table->char('phone', 20)->nullable();
            $table->unsignedInteger('age')->nullable();
            $table->unsignedInteger('sex')->nullable();
            $table->date('birthdate')->nullable();
            $table->char('cep', 11)->nullable();
            $table->string('street', 100)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('neighborhood', 50)->nullable();
            $table->char('state', 2)->nullable();
            $table->string('complement', 100)->nullable();
            $table->char('number', 20)->nullable();
            $table->float('lat', 10, 6)->nullable();
            $table->float('lng', 10, 6)->nullable();
            $table->timestamps();
        });

        Schema::table('teachers', function (Blueprint $table){
            $table->unsignedInteger('person_id')->index();
            $table->foreign('person_id')->references('id')
                  ->on('people');
        });

        Schema::table('guardians', function (Blueprint $table){
            $table->unsignedInteger('person_id')->index();
            $table->foreign('person_id')->references('id')
                  ->on('people');
        });

        Schema::table('students', function (Blueprint $table){
            $table->unsignedInteger('person_id')->index();
            $table->foreign('person_id')->references('id')
                  ->on('people');
        });
    }
}
