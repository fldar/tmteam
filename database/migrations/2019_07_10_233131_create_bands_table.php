<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->unique();
            $table->unsignedInteger('age_category_id')->default(0);
            $table->unsignedInteger('age_finish')->default(0);
            $table->unsignedInteger('color')->default();
            $table->unsignedInteger('modality_id');
            $table->foreign('modality_id')->references('id')->on('modalities');
            $table->timestamps();
        });

        Schema::create('bands_ages_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('age_category_id')->index();
            $table->foreign('age_category_id')->references('id')->on('ages_categories')->onDelete('restrict');
            $table->unsignedInteger('band_id')->index();
            $table->foreign('band_id')->references('id')->on('bands')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bands_ages_categories');
        Schema::dropIfExists('bands');
    }
}
