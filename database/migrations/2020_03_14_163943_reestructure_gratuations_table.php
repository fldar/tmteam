<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReestructureGratuationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('graduations');
        Schema::dropIfExists('graduations_rules');

        Schema::create('graduations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('registration_id')->index();
            $table->foreign('registration_id')->references('id')->on('registrations');
            $table->unsignedInteger('teacher_id')->nullable();
            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->unsignedInteger('modality_id')->index();
            $table->foreign('modality_id')->references('id')->on('modalities');
            $table->unsignedInteger('band_id')->index();
            $table->foreign('band_id')->references('id')->on('bands');
            $table->unsignedInteger('degree_id')->index();
            $table->foreign('degree_id')->references('id')->on('degrees');
            $table->integer('lessons_taken')->default(0);
            $table->date('graduated_on');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('graduations');

        Schema::create('graduations_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->unique();
            $table->unsignedInteger('limit')->default(0)->nullable();
            $table->unsignedInteger('period')->default(0)->nullable();
            $table->unsignedInteger('month')->default(0)->nullable();
            $table->unsignedInteger('classrooms');
            $table->boolean('active')->nullable()->default(0);
            $table->boolean('no_limit')->nullable()->default(0);
            $table->boolean('is_kids')->nullable()->default(0);
            $table->unsignedInteger('band_id')->index();
            $table->foreign('band_id')->references('id')->on('bands');
            $table->timestamps();
        });

        Schema::create('graduations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255)->unique();
            $table->boolean('student_active')->nullable()->default(0);
            $table->boolean('is_kids')->nullable()->default(0);
            $table->unsignedInteger('band_id')->index();
            $table->foreign('band_id')->references('id')->on('bands');
            $table->unsignedInteger('student_id')->index();
            $table->foreign('student_id')->references('id')->on('students');
            $table->unsignedInteger('teacher_id')->index();
            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->unsignedInteger('classroom_id')->index();
            $table->foreign('classroom_id')->references('id')->on('classrooms');
            $table->unsignedInteger('rule_id')->index();
            $table->foreign('rule_id')->references('id')->on('graduations_rules');
            $table->timestamps();
        });
    }
}
