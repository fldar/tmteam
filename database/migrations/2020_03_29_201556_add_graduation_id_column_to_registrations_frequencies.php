<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGraduationIdColumnToRegistrationsFrequencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registrations_frequencies', function (Blueprint $table) {
            $table->unsignedInteger('graduation_id')->after('registration_id')->nullable();
            $table->foreign('graduation_id')->references('id')->on('graduations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registrations_frequencies', function (Blueprint $table) {
            $table->dropForeign('registrations_frequencies_graduation_id_foreign');
            $table->dropColumn('graduation_id');
        });
    }
}
