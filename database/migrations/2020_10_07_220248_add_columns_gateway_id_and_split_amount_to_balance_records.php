<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsGatewayIdAndSplitAmountToBalanceRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balance_records', function (Blueprint $table) {
            $table->string('gateway_id')->nullable();
            $table->decimal('split_amount', 10, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balance_records', function (Blueprint $table) {
            $table->dropColumn('gateway_id');
            $table->dropColumn('split_amount');
        });
    }
}
