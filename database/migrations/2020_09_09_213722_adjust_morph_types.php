<?php

use Illuminate\Database\Migrations\Migration;

class AdjustMorphTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            UPDATE media
            SET model_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Student'
            WHERE model_type like '%Student%';
        ");

        DB::statement("
            UPDATE media
            SET model_type = 'App\\\\Core\\\\Models\\\\Auth\\\\User'
            WHERE model_type like '%User%';
        ");

        DB::statement("
            UPDATE media
            SET model_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Registration'
            WHERE model_type like '%Registration%';
        ");

        DB::statement("
            UPDATE media
            SET model_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Registration'
            WHERE model_type like '%Registration%';
        ");

        DB::statement("
            UPDATE model_has_roles
            SET model_type = 'App\\\\Core\\\\Models\\\\Auth\\\\User'
            WHERE model_type like '%User%';
        ");

        DB::statement("
            UPDATE balance_records
            SET chargeable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Registration'
            WHERE chargeable_type like '%Registration';
        ");

        DB::statement("
            UPDATE notifications
            SET notifiable_type = 'App\\\\Core\\\\Models\\\\Auth\\\\User'
            WHERE notifiable_type = '%User%';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\Auth\\\\User'
            WHERE auditable_type like '%User';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Band'
            WHERE auditable_type like '%Band';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Classroom'
            WHERE auditable_type like '%Classroom';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\Financial\\\\BalanceRecord'
            WHERE auditable_type like '%BalanceRecord';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Graduation'
            WHERE auditable_type like '%Graduation';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Graduation'
            WHERE auditable_type like '%Graduation';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Modality'
            WHERE auditable_type like '%Modality';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\Message'
            WHERE auditable_type like '%Message';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\Notification'
            WHERE auditable_type like '%Notification';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\Notification'
            WHERE auditable_type like '%Notification';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Person'
            WHERE auditable_type like '%Person';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Registration'
            WHERE auditable_type like '%Registration';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\RegistrationFrequency'
            WHERE auditable_type like '%RegistrationFrequency';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\AgeCategory'
            WHERE auditable_type like '%AgeCategory';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Student'
            WHERE auditable_type like '%Student';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Origin'
            WHERE auditable_type like '%StudentOrigin';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Guardian'
            WHERE auditable_type like '%StudentResponsible';
        ");

        DB::statement("
            UPDATE audits SET auditable_type = 'App\\\\Core\\\\Models\\\\TrainingCenter\\\\Teacher'
            WHERE auditable_type like '%Teacher';
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
