<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryToStudentsPerSexReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW students_per_sex_report AS
            SELECT date,
                   r.id AS registration_id,
                   c.id AS classroom_id,
                   sex,
                   r.subsidiary_id AS subsidiary_id
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id
                     INNER JOIN registrations_classrooms rc on r.id = rc.registration_id
                     INNER JOIN classrooms c on rc.classroom_id = c.id
                     INNER JOIN students s ON r.student_id = s.id
                     INNER JOIN users u ON s.user_id = u.id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('
            CREATE OR REPLACE VIEW students_per_sex_report AS
            SELECT date,
                   registration_id,
                   classroom_id,
                   sex
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id
                     INNER JOIN students s ON r.student_id = s.id
                     INNER JOIN people p ON s.person_id = p.id;
            
        ');
    }
}
