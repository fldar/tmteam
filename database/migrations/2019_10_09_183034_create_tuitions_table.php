<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTuitionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('tuitions', function (Blueprint $table) {
      $table->increments('id');
      $table->string('code')->unique();
      $table->date('schedule')->nullable();
      $table->unsignedInteger('status');
      $table->unsignedInteger('type');
      $table->float('amount', 10, 2);
      $table->float('discount', 10, 2)->nullable();
      $table->boolean('active')->nullable()->default(0);
      $table->boolean('signature')->nullable()->default(0);
      $table->boolean('isSchedule')->nullable()->default(0);
      $table->unsignedInteger('student_id')->index();
      $table->foreign('student_id')->references('id')->on('students');
      $table->timestamps();
    });

    Schema::create('tuitions_orders', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('tuitions_id')->index();
      $table->foreign('tuitions_id')->references('id')->on('tuitions')->onDelete('cascade');
      $table->unsignedInteger('order_id')->index();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('tuitions_orders');
    Schema::dropIfExists('tuitions');
  }
}
