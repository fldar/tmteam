<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('nickname', 50);
            $table->string('email', 100)->unique();
            $table->char('cpf', 11)->nullable();
            $table->char('phone', 20)->nullable();
            $table->unsignedInteger('age')->nullable();
            $table->unsignedInteger('sex')->nullable();
            $table->date('birthdate')->nullable();
            $table->char('cep', 11)->nullable();
            $table->string('street', 100)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('neighborhood', 50)->nullable();
            $table->char('state', 2)->nullable();
            $table->string('complement', 100)->nullable();
            $table->char('number', 20)->nullable();
            $table->float('lat', 10, 6)->nullable();
            $table->float('lng', 10, 6)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
