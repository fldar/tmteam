<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsPerAgeCategoriesReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE OR REPLACE VIEW students_per_age_categories_report AS
            SELECT date,
                   registration_id,
                   classroom_id,
                   age_category_id,
                   ac.name AS age_category
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id
                     INNER JOIN students s ON r.student_id = s.id
                     INNER JOIN ages_categories ac ON s.age_category_id = ac.id;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW students_per_age_category_report');
    }
}
