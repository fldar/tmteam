<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateQualifiedStudentsReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE VIEW qualified_students_report AS
                SELECT g.id as id,
                       r.id as registration_id,
                       p.name as name,
                       p.nickname as nickname,
                       m.name as modality,
                       b.name as band,
                       d.name as degree,
                       CONCAT(g.lessons_taken, \'/\', d.lessons) as lessons,
                       g.graduated_on as graduated_on
                FROM registrations r
                         INNER JOIN graduations g on g.registration_id = r.id
                         AND g.id = (SELECT max(id) as id FROM graduations WHERE registration_id = r.id)
                         INNER JOIN bands b on g.band_id = b.id
                         INNER JOIN degrees d on g.degree_id = d.id
                         INNER JOIN modalities m on b.modality_id = m.id
                         INNER JOIN students s on r.student_id = s.id
                         INNER JOIN people p on s.person_id = p.id
                WHERE g.lessons_taken >= d.lessons AND
                      ABS(DATEDIFF(g.graduated_on, CURDATE())) >= d.minimum_period
                GROUP BY r.id
                ORDER BY name
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW qualified_students_report');
    }
}
