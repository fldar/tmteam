<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryToMonthlyRegistrationsReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW monthly_registrations_report AS
                SELECT count(id) AS `registries`,
                       MONTH(registrations.effective_date) AS `competence_month`,
                       YEAR(registrations.effective_date) AS `competence_year`,
                       registrations.subsidiary_id AS `subsidiary_id`
                FROM registrations
                GROUP BY competence_year, competence_month, subsidiary_id
                ORDER BY competence_year DESC, competence_month DESC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('
            CREATE OR REPLACE VIEW monthly_registrations_report AS
                SELECT count(id) AS registries,
                    MONTH(registrations.effective_date) AS competence_month,
                    YEAR(registrations.effective_date) AS competence_year
                FROM registrations
                GROUP BY competence_year, competence_month
                ORDER BY competence_year DESC, competence_month DESC
        ');
    }
}
