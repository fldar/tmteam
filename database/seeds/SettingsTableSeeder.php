<?php

use App\Core\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    private array $defaults = [
        'max_overdue_days' => 29,
        'min_overdue_days' => 5,
        'fine'             => 2,
        'interest'         => 2,
        'split_amount'     => 2,
        'split_type'       => 1,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->defaults as $key => $value){
            Setting::updateOrCreate(
                ['key' => $key],
                ['value' => $value]
            );
        }
    }
}
