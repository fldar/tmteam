<?php

use App\Core\Models\TrainingCenter\Modality;
use Illuminate\Database\Seeder;
use App\Core\Models\Auth\User;

class ModalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Modality::create([
            'name'  => 'jiu-jitsu',
            'price' => 80,
        ]);

        Modality::create([
            'name'  => 'Boxe',
            'price' => 80,
        ]);

        Modality::create([
            'name'  => 'Karate',
            'price' => 80,
        ]);

        Modality::create([
            'name'  => 'Judo',
            'price' => 80,
        ]);
    }


}
