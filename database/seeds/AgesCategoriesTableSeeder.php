<?php

use Illuminate\Database\Seeder;
use App\Core\Models\TrainingCenter\AgeCategory;

class AgesCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_1 = AgeCategory::create([
            'name'       => 'Pré-Mirim I',
            'age_begin'  => 4,
            'age_finish' => 5
        ]);

        $category_2 = AgeCategory::create([
            'name'       => 'Pré-Mirim II',
            'age_begin'  => 5,
            'age_finish' => 6
        ]);

        $category_3 = AgeCategory::create([
            'name'       => 'Pré-Mirim III',
            'age_begin'  => 6,
            'age_finish' => 7
        ]);

        $category_4 = AgeCategory::create([
            'name'       => 'Mirim I',
            'age_begin'  => 7,
            'age_finish' => 8
        ]);

        $category_5 = AgeCategory::create([
            'name'       => 'Mirim II',
            'age_begin'  => 8,
            'age_finish' => 9
        ]);

        $category_6 = AgeCategory::create([
            'name'       => 'Mirim III',
            'age_begin'  => 9,
            'age_finish' => 10
        ]);

        $category_7 = AgeCategory::create([
            'name'       => 'INFANTIL I',
            'age_begin'  => 10,
            'age_finish' => 11
        ]);

        $category_8 = AgeCategory::create([
            'name'       => 'INFANTIL II',
            'age_begin'  => 11,
            'age_finish' => 12
        ]);

        $category_9 = AgeCategory::create([
            'name'       => 'INFANTIL III',
            'age_begin'  => 12,
            'age_finish' => 13
        ]);

        $category_10 = AgeCategory::create([
            'name'       => 'INFANT0-JUVENIL I',
            'age_begin'  => 13,
            'age_finish' => 14
        ]);

        $category_11 = AgeCategory::create([
            'name'       => 'INFANT0-JUVENIL II',
            'age_begin'  => 14,
            'age_finish' => 15
        ]);

        $category_12 = AgeCategory::create([
            'name'       => 'INFANT0-JUVENIL III',
            'age_begin'  => 15,
            'age_finish' => 16
        ]);

        $category_13 = AgeCategory::create([
            'name'       => 'JUVENIL I',
            'age_begin'  => 16,
            'age_finish' => 17
        ]);

        $category_14 = AgeCategory::create([
            'name'       => 'JUVENIL II',
            'age_begin'  => 17,
            'age_finish' => 18
        ]);

        $category_15 = AgeCategory::create([
            'name'       => 'ADULTO',
            'age_begin'  => 18,
            'age_finish' => 30
        ]);

        $category_16 = AgeCategory::create([
            'name'       => 'MASTER I',
            'age_begin'  => 30,
            'age_finish' => 36
        ]);

        $category_17 = AgeCategory::create([
            'name'       => 'MASTER II',
            'age_begin'  => 36,
            'age_finish' => 41
        ]);

        $category_18 = AgeCategory::create([
            'name'       => 'MASTER III',
            'age_begin'  => 41,
            'age_finish' => 46
        ]);

        $category_19 = AgeCategory::create([
            'name'       => 'MASTER IV',
            'age_begin'  => 46,
            'age_finish' => 51
        ]);

        $category_20 = AgeCategory::create([
            'name'       => 'MASTER V',
            'age_begin'  => 51,
            'age_finish' => 56
        ]);

        $category_21 = AgeCategory::create([
            'name'       => 'MASTER VI',
            'age_begin'  => 56,
            'age_finish' => 100
        ]);
    }
}
