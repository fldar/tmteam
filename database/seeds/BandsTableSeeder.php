<?php

use App\Core\Models\TrainingCenter\Modality;
use Illuminate\Database\Seeder;
use App\Core\Models\TrainingCenter\Band;


class BandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Band::create([
            'name'        => 'Branca',
            'color'       => Band::WHITE,
            'modality_id' => Modality::first()->id
        ]);

        Band::create([
            'name'        => 'Cinza',
            'color'       => Band::GREY,
            'modality_id' => Modality::first()->id
        ]);

        Band::create([
            'name'        => 'Preta',
            'color'       => Band::BLACK,
            'modality_id' => Modality::first()->id
        ]);

        Band::create([
            'name'        => 'Amarela',
            'color'       => Band::YELLOW,
            'modality_id' => Modality::first()->id
        ]);
        Band::create([
            'name'        => 'Marrom',
            'color'       => Band::BROWN,
            'modality_id' => Modality::first()->id
        ]);

        Band::create([
            'name'        => 'Laranja',
            'color'       => Band::ORANGE,
            'modality_id' => Modality::first()->id
        ]);

        Band::create([
            'name'        => 'Verde',
            'color'       => Band::GREEN,
            'modality_id' => Modality::first()->id
        ]);

        Band::create([
            'name'        => 'Azul',
            'color'       => Band::BLUE,
            'modality_id' => Modality::first()->id
        ]);

        Band::create([
            'name'        => 'Roxa',
            'color'       => Band::PURPLE,
            'modality_id' => Modality::first()->id
        ]);
    }
}
