<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AgesCategoriesTableSeeder::class);
        $this->call(OriginsTableSeeder::class);
        //$this->call(ModalitiesTableSeeder::class);
        //$this->call(StudentsTableSeeder::class);
        //$this->call(TeachersTableSeeder::class);
        //$this->call(BandsTableSeeder::class);
    }
}
