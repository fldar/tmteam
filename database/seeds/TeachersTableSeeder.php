<?php

use Illuminate\Database\Seeder;
use App\Core\Models\TrainingCenter\Teacher;
use App\Core\Models\TrainingCenter\Modality;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Teacher::class, 2)->create()->each(function ($teacher) {
            $teacher->modalities()->attach(Modality::all());
        });
    }
}
