<?php

use Illuminate\Database\Seeder;
use App\Core\Models\TrainingCenter\Student;

class StudentsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(Student::class, 5)->create();
  }
}
