<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Core\Models\Person;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Person::class, function (Faker $faker) {

    $firstName = $faker->unique()->firstName;

    return [
        'name'      => $firstName . " " . $faker->lastName,
        'nickname'  => $firstName,
        'email'     => $faker->unique()->safeEmail,
        'cpf'       => $faker->randomNumber(9) . $faker->randomNumber(2),
        'phone'     => $faker->randomNumber(3) . $faker->randomNumber(8),
        'birthdate' => Carbon::now()->format('d/m/Y'),
        'age'       => $faker->randomNumber(2),
        'sex'       => $faker->numberBetween(1, 2)
    ];
});