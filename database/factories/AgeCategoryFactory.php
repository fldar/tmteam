<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Core\Models\TrainingCenter\AgeCategory;
use App\Core\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(AgeCategory::class, function (Faker $faker) {
    return [
        'name'       => $faker->unique()->name,
        'age_begin'  => $faker->numberBetween(1, 5),
        'age_finish' => $faker->numberBetween(5, 10)
    ];
});
