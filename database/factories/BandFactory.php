<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Core\Models\TrainingCenter\Band;
use App\Core\Models\TrainingCenter\AgeCategory;
use App\Core\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(Band::class, function (Faker $faker) {
  return [
    'name' => $faker->unique()->name,
    'color' => $faker->numberBetween(1,7),
    'category_id' => factory(AgeCategory::class)->create()->id,
  ];
});
