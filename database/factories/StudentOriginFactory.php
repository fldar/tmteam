<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Core\Models\TrainingCenter\Origin;
use App\Core\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(Origin::class, function (Faker $faker) {
  return [
    'name' => $faker->unique()->name,
  ];
});
