<?php

/**
 * Student Routes
 */
Route::prefix('aluno')->middleware(['auth.admin', 'role.check'])->namespace('Student')->group(function () {
    Route::name('students.dashboard.index')->get('/', 'Dashboard\DashboardController@index');

    Route::name('students.frequencies.index')->get('/frequencias/', 'Frequencies\FrequencyController@index');
    Route::name('students.frequencies.create')->get('/frequencias/{classroom}', 'Frequencies\FrequencyController@create');
    Route::name('students.frequencies.store')->post('/frequencias', 'Frequencies\FrequencyController@store');

    Route::name('students.accounts.edit')->get('/perfil', 'Accounts\AccountController@edit');
    Route::name('students.accounts.update')->put('/perfil/atualizar/{student}', 'Accounts\AccountController@update');
});
