<?php

/**
 * Teacher Routes
 */
Route::prefix('professor')->middleware(['auth.admin', 'role.check'])->namespace('Teacher')->group(function () {
    Route::name('teachers.dashboard.index')->get('/', 'Dashboard\DashboardController@index');

    Route::name('teachers.frequencies.index')->get('/turmas/{classroom}/frequencias', 'Classrooms\FrequencyController@index');
    Route::name('teachers.frequencies.create')->get('/turmas/{classroom}/frequencias/adicionar', 'Classrooms\FrequencyController@create');
    Route::name('teachers.frequencies.store')->post('/turmas/{classroom}/frequencias', 'Classrooms\FrequencyController@store');
    Route::name('teachers.frequencies.update')->put('/turmas/{classroom}/frequencias', 'Classrooms\FrequencyController@update');

    Route::name('teachers.students.index')->get('/turmas/{classroom}/students', 'Classrooms\StudentController@index');
    Route::name('teachers.students.print')->get('/turmas/{classroom}/students/print', 'Classrooms\StudentController@print');

    Route::name('teachers.accounts.edit')->get('/perfil', 'Accounts\AccountController@edit');
    Route::name('teachers.accounts.update')->put('/perfil/atualizar/{teacher}', 'Accounts\AccountController@update');

    Route::name('teachers.messages.create')->get('/mensagens', 'Notices\MessageController@create');
    Route::name('teachers.messages.store')->post('/mensagens', 'Notices\MessageController@store');
});
