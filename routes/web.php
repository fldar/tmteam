<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('guest.admin')->group(function () {

    //Auto login
    Route::name('autologin')->get('/autologin/{token}', '\Watson\Autologin\AutologinController@autologin');

    Route::namespace('Auth\Sessions')->group(function () {

        Route::get('/', 'SessionController@showLoginForm');
        Route::name('login')->get('/login', 'SessionController@showLoginForm');
        Route::name('auth.session.login')->get('/acessar', 'SessionController@showLoginForm');
        Route::name('auth.session.login')->post('/acessar', 'SessionController@login');

        // Reset Password
        Route::name('auth.password.forgot')->get('/esqueci-minha-senha', 'ForgotPasswordController@showLinkRequestForm');
        Route::name('auth.password.processForgot')->post('/esqueci-minha-senha', 'ForgotPasswordController@sendResetLinkEmail');
        Route::name('auth.password.reset')->get('/cadastrar-senha/{token}/{email}', 'ResetPasswordController@showResetForm');
        Route::name('auth.password.processReset')->post('/cadastrar-senha', 'ResetPasswordController@reset');

        // First access
        Route::name('auth.activation.create')->get('/primeiro-acesso/{activation}', 'ActivationController@showActivationForm');
        Route::name('auth.activation.processCreate')->post('/primeiro-acesso/{token}', 'ActivationController@store');
    });
});

// Logout
Route::middleware('auth.admin')->group(function() {
    Route::name('auth.session.logout')->get('/sair', 'Auth\Sessions\SessionController@logout');
    Route::name('core.notifications.destroy')->delete('/excluir/{notification}', 'Core\NotificationController@destroy');
    Route::name('core.notifications.read')->put('/{notification}', 'Core\NotificationController@update');
});


