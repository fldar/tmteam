<?php

/**
 * Admin Routes
 */
Route::prefix('admin')
     ->namespace('Admin')
     ->name('admin.')
     ->middleware(['auth.admin', 'role.check', 'first.access'])
     ->group(function () {

         Route::namespace('Dashboard')->group(function () {
             Route::name('dashboard.index')->get('/', 'DashboardController@index');

             Route::prefix('financial')->name('financial.')->group(function () {
                 Route::name('index')->get('/', 'FinancialController@index');
                 Route::get('/income', 'FinancialController@income');
                 Route::get('/outcome', 'FinancialController@outcome');
                 Route::get('/active-students', 'FinancialController@activeStudents');
                 Route::get('/registrations', 'FinancialController@registrations');
                 Route::get('/billing-summary', 'FinancialController@billingSummary');
                 Route::get('/billing-per-payment-type', 'FinancialController@billingPerPaymentType');
                 Route::get('/students-by-status', 'FinancialController@studentsByStatus');
             });

             Route::prefix('academic')->name('academic.')->group(function () {
                 Route::name('index')->get('/', 'AcademicController@index');
                 Route::get('/students-per-sex', 'AcademicController@studentsPerSex');
                 Route::get('/students-per-age-categories', 'AcademicController@studentsPerAgeCategories');
                 Route::get('/students-per-bands', 'AcademicController@studentsPerBands');
             });
         });

         Route::namespace('TrainingCenter')->group(function () {
             Route::resource('students', 'StudentController', ['except' => 'show']);
             Route::resource('age-categories', 'AgeCategoryController', ['except' => 'show']);
             Route::resource('origins', 'OriginController', ['except' => 'show']);
             Route::resource('guardians', 'GuardianController', ['except' => 'show']);
             Route::resource('teachers', 'TeacherController', ['except' => 'show']);
             Route::resource('bands', 'BandController', ['except' => 'show']);
             Route::resource('modalities', 'ModalityController', ['except' => 'show']);

             Route::resource('classrooms', 'ClassroomController');

             Route::prefix('classrooms')->name('classrooms.')->group(function () {
                 Route::resource('{classroom}/frequencies', 'FrequencyController');
             });

             Route::name('students.')->prefix('students')->group(function () {
                 Route::resource('attachments', 'AttachmentController', ['only' => ['show', 'update']]);

                 Route::resource('{registration}/graduations', 'GraduationController', ['except' => 'show']);
                 Route::resource('{registration}/installments', 'InstallmentController', ['except' => 'show']);
                 Route::resource('{registration}/issues', 'IssueController', ['only' => 'index']);

                 Route::name('skip-check')->post('/skip-check/{registration}', 'StudentController@skipcheck');
                 Route::name('graduations.print')->get('{registration}/print/{graduation}', 'GraduationController@print');
             });
         });

         Route::namespace('Auth')->prefix('users')->group(function () {
             Route::resource('permissions', 'PermissionController', ['except' => 'show']);
             Route::resource('roles', 'RoleController', ['except' => 'show']);

             Route::name('users.sendEmail')->get('/resend-confirmation/{user}', 'UserController@sendEmail');
             Route::name('users.activate')->get('/activate/{user}', 'UserController@activate');
             Route::name('users.deactivate')->get('/deactivate/{user}', 'UserController@deactivate');
         });

         Route::namespace('Finances')->group(function () {
             Route::resource('balance-records', 'BalanceRecordController', ['except' => 'show']);
             Route::resource('monthly-charge', 'MonthlyChargeController', ['only' => 'store']);
         });

         Route::namespace('Juno')->prefix('juno')->group(function() {
             Route::resource('transfer', 'TransferController', ['only' => ['index', 'store']]);
             Route::resource('split', 'SplitReportController', ['only' => ['index']]);
             Route::resource('invoices', 'InvoiceController', ['only' => ['index', 'store']]);
         });

         Route::namespace('Reports')->group(function(){
             Route::prefix('qualifications')->name('qualifications.')->group(function() {
                 Route::name('index')->get('/', 'QualificationController@index');
                 Route::name('print')->get('/print', 'QualificationController@print');
             });

              Route::prefix('reports')->name('reports.')->group(function () {
                 Route::name('balance')->get('/balance', 'BalanceController@index');
                 Route::name('overdue')->get('/overdue', 'OverdueController@index');
                 Route::name('class-billing')->get('/class-billing', 'BillingPerClassController@index');
              });
         });

         Route::resource('persons', 'PersonController', ['only' => 'show']);
         Route::resource('messages', 'MessageController');
         Route::resource('users', 'Auth\UserController', ['except' => 'show']);
         Route::resource('audits', 'AuditController', ['only' => ['index', 'show']]);
         Route::resource('subsidiaries', 'SubsidiaryController', ['except' => 'show']);

         Route::prefix('subsidiaries')->name('subsidiaries.')->group(function(){
             Route::name('documents.index')->get('{id}/documents', 'DocumentController@index');
             Route::name('documents.store')->post('{id}/documents/store', 'DocumentController@store');
         });

         Route::prefix('settings')->name('settings.')->group(function (){
             Route::name('edit')->get('/edit', 'SettingController@edit');
             Route::name('update')->put('/update', 'SettingController@update');
         });

     });
