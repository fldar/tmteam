<?php

return [
    'modules' => [
        [
            'name'       => 'Início',
            'permission' => 'view_dashboard',
            'options'    => [
                [
                    'name'       => 'Home',
                    'is_tree'    => false,
                    'permission' => 'view_dashboard',
                    'icon'       => 'fas fa-home lga',
                    'route'      => 'admin.dashboard.index'
                ]
            ]
        ],
        [
            'name'       => 'Centro de Treinamento',
            'permission' => 'view_centers',
            'options'    =>
                [
                    [
                        'name'       => 'Estatísticas',
                        'is_tree'    => false,
                        'permission' => 'view_academic_dashboard',
                        'icon'       => 'fas fa-chart-pie lga',
                        'route'      => 'admin.academic.index'
                    ],
                    [
                        'name'       => 'Avisos',
                        'is_tree'    => false,
                        'permission' => 'view_messages',
                        'icon'       => 'fas fa-chalkboard lga',
                        'route'      => 'admin.messages.index'
                    ],
                    [
                        'name'       => 'Qualificações',
                        'is_tree'    => false,
                        'permission' => 'view_graduations',
                        'icon'       => 'fas fa-graduation-cap lga',
                        'route'      => 'admin.qualifications.index'
                    ],
                    [
                        'name'       => 'Registros',
                        'is_tree'    => true,
                        'permission' => 'view_students',
                        'icon'       => 'fas fa-folder-open lga',
                        'options'    => [
                            [
                                'name'       => 'Alunos',
                                'is_tree'    => false,
                                'icon'       => 'fas fa-user-ninja lga',
                                'permission' => 'view_students',
                                'route'      => 'admin.students.index'
                            ],
                            [
                                'name'       => 'Responsáveis',
                                'is_tree'    => false,
                                'icon'       => 'fas fa-user-tie lga',
                                'permission' => 'view_guardians',
                                'route'      => 'admin.guardians.index'
                            ],
                            [
                                'name'       => 'Categorias',
                                'is_tree'    => false,
                                'icon'       => 'fas fa-tag lga',
                                'permission' => 'view_bands_submodules',
                                'route'      => 'admin.age-categories.index'
                            ],
                            [
                                'name'       => 'Origens',
                                'is_tree'    => false,
                                'icon'       => 'fas fa-location-arrow lga',
                                'permission' => 'view_students_origins',
                                'route'      => 'admin.origins.index'
                            ],
                            [
                                'name'       => 'Modalidades',
                                'is_tree'    => false,
                                'icon'       => 'fas fa-bookmark lga',
                                'permission' => 'view_modalities',
                                'route'      => 'admin.modalities.index'
                            ],

                            [
                                'name'       => 'Faixas',
                                'is_tree'    => false,
                                'permission' => 'view_bands',
                                'icon'       => 'far fa-star lga',
                                'route'      => 'admin.bands.index'
                            ],
                            [
                                'name'       => 'Turmas',
                                'is_tree'    => false,
                                'permission' => 'view_classrooms',
                                'icon'       => 'far fa-clipboard lga',
                                'route'      => 'admin.classrooms.index'
                            ],
                            [
                                'name'       => 'Professores',
                                'is_tree'    => false,
                                'permission' => 'view_teachers',
                                'icon'       => 'fas fa-chalkboard-teacher lga',
                                'route'      => 'admin.teachers.index'
                            ]
                        ]
                    ],

                ],
        ],
        [
            'name'       => 'Finanças',
            'permission' => 'view_controls',
            'options'    =>
                [
                    [
                        'name'       => 'Estatísticas',
                        'is_tree'    => false,
                        'permission' => 'view_finances_dashboard',
                        'icon'       => 'fas fa-chart-line lga',
                        'route'      => 'admin.financial.index'
                    ],
                    [
                        'name'       => 'Lançamentos',
                        'is_tree'    => false,
                        'permission' => 'view_balance_records',
                        'icon'       => 'fas fa-receipt lga',
                        'route'      => 'admin.balance-records.index'
                    ],
                    [
                        'name'       => 'Juno',
                        'is_tree'    => true,
                        'permission' => 'view_gateway_services',
                        'icon'       => 'fas fa-dollar-sign lga',
                        'options'    => [
                            [
                                'name'       => 'Boletos',
                                'is_tree'    => false,
                                'permission' => 'view_balance_records',
                                'icon'       => 'fas fa-file-invoice-dollar lga',
                                'route'      => 'admin.invoices.index'
                            ],
                            [
                                'name'       => 'Saldo',
                                'is_tree'    => false,
                                'permission' => 'view_balance_records',
                                'icon'       => 'fas fa-wallet lga',
                                'route'      => 'admin.transfer.index'
                            ],
                            [
                                'name'       => 'Split',
                                'is_tree'    => false,
                                'permission' => 'view_split_report',
                                'icon'       => 'fas fa-coins lga',
                                'route'      => 'admin.split.index'
                            ],
                        ]
                    ],
                    [
                        'name'       => 'Relatórios',
                        'is_tree'    => true,
                        'permission' => 'view_balance_records',
                        'icon'       => 'fas fa-archive lga',
                        'options'    => [
                            [
                                'name'       => 'Caixa',
                                'is_tree'    => false,
                                'permission' => 'view_balance_records',
                                'icon'       => 'fas fa-cash-register lga',
                                'route'      => 'admin.reports.balance'
                            ],
                            [
                                'name'       => 'Inadimplência',
                                'is_tree'    => false,
                                'permission' => 'view_balance_records',
                                'icon'       => 'fas fa-user-clock lga',
                                'route'      => 'admin.reports.overdue'
                            ],
                            [
                                'name'       => 'Faturamento por turma',
                                'is_tree'    => false,
                                'permission' => 'view_balance_records',
                                'icon'       => 'fas fa-comments-dollar lga',
                                'route'      => 'admin.reports.class-billing'
                            ],
                        ]
                    ],
                ],
        ],
        [
            'name'       => 'Administrativo',
            'permission' => 'view_settings',
            'options'    =>
                [
                    [
                        'name'       => 'Acesso',
                        'is_tree'    => true,
                        'permission' => 'view_users',
                        'icon'       => 'fas fa-exclamation lga',
                        'options' => [
                            [
                                'name'       => 'Usuários',
                                'is_tree'    => false,
                                'permission' => 'view_users',
                                'icon'       => 'far fa-user lga',
                                'route'      => 'admin.users.index'
                            ],
                            [
                                'name'       => 'Grupos',
                                'is_tree'    => false,
                                'permission' => 'view_roles',
                                'icon'       => 'fas fa-users lga',
                                'route'      => 'admin.roles.index'
                            ],
                            [
                                'name'       => 'Permissões',
                                'is_tree'    => false,
                                'permission' => 'view_permissions',
                                'icon'       => 'fas fa-key lga',
                                'route'      => 'admin.permissions.index'
                            ],
                        ]
                    ],
                    [
                        'name'       => 'Unidades',
                        'is_tree'    => false,
                        'permission' => 'view_subsidiaries',
                        'icon'       => 'fas fa-building lga',
                        'route'      => 'admin.subsidiaries.index'
                    ],
                    [
                        'name'       => 'Auditoria',
                        'is_tree'    => false,
                        'permission' => 'view_audits',
                        'icon'       => 'fas fa-redo lga',
                        'route'      => 'admin.audits.index'
                    ],
                    [
                        'name'       => 'Configurações',
                        'is_tree'    => false,
                        'permission' => 'edit_settings',
                        'icon'       => 'fas fa-cogs lga',
                        'route'      => 'admin.settings.edit'
                    ],
                ],
        ],
    ],
    'audit'   => [
        'types'   => [
            'Band'           => 'Faixa',
            'AgeCategory'    => 'Categoria de idade',
            'BalanceRecord'  => 'Lançamento',
            'Classroom'      => 'Turmas',
            'Frequency'      => 'Frequência',
            'Graduation'     => 'Graduação',
            'GraduationRule' => 'Regra de graduação',
            'Modality'       => 'Molidade',
            'Person'         => 'Pessoa',
            'StudentOrigin'  => 'Origem',
            'Guardian'       => 'Responsável',
            'Registration'   => 'Matrícula',
            'Teacher'        => 'Professor',
            'Student'        => 'Estudante',
            'User'           => 'Usuário',
            'Subsidiary'     => 'Unidade'
        ],
        'actions' => [
            'created'  => 'Criação',
            'updated'  => 'Alteração',
            'deleted'  => 'Exclusão',
            'restored' => 'Reativação'
        ],
    ],
    'email'   => [
        'default' => 'suporte@qualitare.com.br'
    ]
];
