"use strict";

(function($, window, i) {
  // Bootstrap 4 Modal
  $.fn.fireModal = function(options) {
    var options = $.extend({
      size: 'modal-md',
      center: false,
      animation: true,
      title: 'Modal Title',
      closeButton: true,
      header: true,
      bodyClass: '',
      footerClass: '',
      body: '',
      buttons: [],
      autoFocus: true,
      removeOnDismiss: false,
      created: function() {},
      appended: function() {},
      onFormSubmit: function() {},
      modal: {}
    }, options);

    this.each(function() {
      i++;
      var id = 'fire-modal-' + i,
        trigger_class = 'trigger--' + id,
        trigger_button = $('.' + trigger_class);

      $(this).addClass(trigger_class);

      // Get modal body
      let body = options.body;

      if(typeof body == 'object') {
        if(body.length) {
          let part = body;
          body = body.removeAttr('id').clone().removeClass('modal-part');
          part.remove();
        }else{
          body = '<div class="text-danger">Modal part element not found!</div>';
        }
      }

      // Modal base template
      var modal_template = '   <div class="modal'+ (options.animation == true ? ' fade' : '') +'" tabindex="-1" role="dialog" id="'+ id +'">  '  +
                 '     <div class="modal-dialog '+options.size+(options.center ? ' modal-dialog-centered' : '')+'" role="document">  '  +
                 '       <div class="modal-content">  '  +
                 ((options.header == true) ?
                 '         <div class="modal-header">  '  +
                 '           <h5 class="modal-title">'+ options.title +'</h5>  '  +
                 ((options.closeButton == true) ?
                 '           <button type="button" class="close" data-dismiss="modal" aria-label="Close">  '  +
                 '             <span aria-hidden="true">&times;</span>  '  +
                 '           </button>  '
                 : '') +
                 '         </div>  '
                 : '') +
                 '         <div class="modal-body">  '  +
                 '         </div>  '  +
                 (options.buttons.length > 0 ?
                 '         <div class="modal-footer">  '  +
                 '         </div>  '
                 : '')+
                 '       </div>  '  +
                 '     </div>  '  +
                 '  </div>  ' ;

      // Convert modal to object
      var modal_template = $(modal_template);

      // Start creating buttons from 'buttons' option
      var this_button;
      options.buttons.forEach(function(item) {
        // get option 'id'
        let id = "id" in item ? item.id : '';

        // Button template
        this_button = '<button type="'+ ("submit" in item && item.submit == true ? 'submit' : 'button') +'" class="'+ item.class +'" id="'+ id +'">'+ item.text +'</button>';

        // add click event to the button
        this_button = $(this_button).off('click').on("click", function() {
          // execute function from 'handler' option
          item.handler.call(this, modal_template);
        });
        // append generated buttons to the modal footer
        $(modal_template).find('.modal-footer').append(this_button);
      });

      // append a given body to the modal
      $(modal_template).find('.modal-body').append(body);

      // add additional body class
      if(options.bodyClass) $(modal_template).find('.modal-body').addClass(options.bodyClass);

      // add footer body class
      if(options.footerClass) $(modal_template).find('.modal-footer').addClass(options.footerClass);

      // execute 'created' callback
      options.created.call(this, modal_template, options);

      // modal form and submit form button
      let modal_form = $(modal_template).find('.modal-body form'),
        form_submit_btn = modal_template.find('button[type=submit]');

      // append generated modal to the body
      $("body").append(modal_template);

      // execute 'appended' callback
      options.appended.call(this, $('#' + id), modal_form, options);

      // if modal contains form elements
      if(modal_form.length) {
        // if `autoFocus` option is true
        if(options.autoFocus) {
          // when modal is shown
          $(modal_template).on('shown.bs.modal', function() {
            // if type of `autoFocus` option is `boolean`
            if(typeof options.autoFocus == 'boolean')
              modal_form.find('input:eq(0)').focus(); // the first input element will be focused
            // if type of `autoFocus` option is `string` and `autoFocus` option is an HTML element
            else if(typeof options.autoFocus == 'string' && modal_form.find(options.autoFocus).length)
              modal_form.find(options.autoFocus).focus(); // find elements and focus on that
          });
        }

        // form object
        let form_object = {
          startProgress: function() {
            modal_template.addClass('modal-progress');
          },
          stopProgress: function() {
            modal_template.removeClass('modal-progress');
          }
        };

        // if form is not contains button element
        if(!modal_form.find('button').length) $(modal_form).append('<button class="d-none" id="'+ id +'-submit"></button>');

        // add click event
        form_submit_btn.click(function() {
          modal_form.submit();
        });

        // add submit event
        modal_form.submit(function(e) {
          // start form progress
          form_object.startProgress();

          // execute `onFormSubmit` callback
          options.onFormSubmit.call(this, modal_template, e, form_object);
        });
      }

      $(document).on("click", '.' + trigger_class, function() {
        let modal = $('#' + id).modal(options.modal);

        if(options.removeOnDismiss) {
          modal.on('hidden.bs.modal', function() {
            modal.remove();
          });
        }

        return false;
      });
    });
  }

  // Bootstrap Modal Destroyer
  $.destroyModal = function(modal) {
    modal.modal('hide');
    modal.on('hidden.bs.modal', function() {
    });
  }

  // Card Progress Controller
  $.cardProgress = function(card, options) {
    var options = $.extend({
      dismiss: false,
      dismissText: 'Cancel',
      spinner: true,
      onDismiss: function() {}
    }, options);

    var me = $(card);

    me.addClass('card-progress');
    if(options.spinner == false) {
      me.addClass('remove-spinner');
    }

    if(options.dismiss == true) {
      var btn_dismiss = '<a class="btn btn-danger card-progress-dismiss">'+options.dismissText+'</a>';
      btn_dismiss = $(btn_dismiss).off('click').on('click', function() {
        me.removeClass('card-progress');
        me.find('.card-progress-dismiss').remove();
        options.onDismiss.call(this, me);
      });
      me.append(btn_dismiss);
    }

    return {
      dismiss: function(dismissed) {
        $.cardProgressDismiss(me, dismissed);
      }
    };
  }

  $.cardProgressDismiss = function(card, dismissed) {
    var me = $(card);
    me.removeClass('card-progress');
    me.find('.card-progress-dismiss').remove();
    if(dismissed)
      dismissed.call(this, me);
  }

  $.chatCtrl = function(element, chat) {
    var chat = $.extend({
      position: 'chat-right',
      text: '',
      time: moment(new Date().toISOString()).format('hh:mm'),
      picture: '',
      type: 'text', // or typing
      timeout: 0,
      onShow: function() {}
    }, chat);

    var target = $(element),
        element = '<div class="chat-item '+chat.position+'" style="display:none">' +
                  '<img src="'+chat.picture+'">' +
                  '<div class="chat-details">' +
                  '<div class="chat-text">'+chat.text+'</div>' +
                  '<div class="chat-time">'+chat.time+'</div>' +
                  '</div>' +
                  '</div>',
        typing_element = '<div class="chat-item chat-left chat-typing" style="display:none">' +
                  '<img src="'+chat.picture+'">' +
                  '<div class="chat-details">' +
                  '<div class="chat-text"></div>' +
                  '</div>' +
                  '</div>';

      var append_element = element;
      if(chat.type == 'typing') {
        append_element = typing_element;
      }

      if(chat.timeout > 0) {
        setTimeout(function() {
          target.find('.chat-content').append($(append_element).fadeIn());
        }, chat.timeout);
      }else{
        target.find('.chat-content').append($(append_element).fadeIn());
      }

      var target_height = 0;
      target.find('.chat-content .chat-item').each(function() {
        target_height += $(this).outerHeight();
      });
      setTimeout(function() {
        target.find('.chat-content').scrollTop(target_height, -1);
      }, 100);
      chat.onShow.call(this, append_element);
  }
})(jQuery, this, 0);


"use strict";

// ChartJS
if(window.Chart) {
  Chart.defaults.global.defaultFontFamily = "'Nunito', 'Segoe UI', 'Arial'";
  Chart.defaults.global.defaultFontSize = 12;
  Chart.defaults.global.defaultFontStyle = 500;
  Chart.defaults.global.defaultFontColor = "#999";
  Chart.defaults.global.tooltips.backgroundColor = "#000";
  Chart.defaults.global.tooltips.bodyFontColor = "rgba(255,255,255,.7)";
  Chart.defaults.global.tooltips.titleMarginBottom = 10;
  Chart.defaults.global.tooltips.titleFontSize = 14;
  Chart.defaults.global.tooltips.titleFontFamily = "'Nunito', 'Segoe UI', 'Arial'";
  Chart.defaults.global.tooltips.titleFontColor = '#fff';
  Chart.defaults.global.tooltips.xPadding = 15;
  Chart.defaults.global.tooltips.yPadding = 15;
  Chart.defaults.global.tooltips.displayColors = false;
  Chart.defaults.global.tooltips.intersect = false;
  Chart.defaults.global.tooltips.mode = 'nearest';
}

// DropzoneJS
if(window.Dropzone) {
  Dropzone.autoDiscover = false;
}

// Basic confirm box
$('[data-confirm]').each(function() {
  var me = $(this),
      me_data = me.data('confirm');

  me_data = me_data.split("|");
  me.fireModal({
    title: me_data[0],
    body: me_data[1],
    buttons: [
      {
        text: me.data('confirm-text-yes') || 'Yes',
        class: 'btn btn-danger btn-shadow',
        handler: function() {
          eval(me.data('confirm-yes'));
        }
      },
      {
        text: me.data('confirm-text-cancel') || 'Cancel',
        class: 'btn btn-secondary',
        handler: function(modal) {
          $.destroyModal(modal);
          eval(me.data('confirm-no'));
        }
      }
    ]
  })
});

// Global
$(function() {
  let sidebar_nicescroll_opts = {
    cursoropacitymin: 0,
    cursoropacitymax: .8,
    zindex: 892
  }, now_layout_class = null;

  var sidebar_sticky = function() {
    if($("body").hasClass('layout-2')) {
      $("body.layout-2 #sidebar-wrapper").stick_in_parent({
        parent: $('body')
      });
      $("body.layout-2 #sidebar-wrapper").stick_in_parent({recalc_every: 1});
    }
  }
  sidebar_sticky();

  var sidebar_nicescroll;
  var update_sidebar_nicescroll = function() {
    let a = setInterval(function() {
      if(sidebar_nicescroll != null)
        sidebar_nicescroll.resize();
    }, 10);

    setTimeout(function() {
      clearInterval(a);
    }, 600);
  }

  var sidebar_dropdown = function() {
    if($(".main-sidebar").length) {
      $(".main-sidebar").niceScroll(sidebar_nicescroll_opts);
      sidebar_nicescroll = $(".main-sidebar").getNiceScroll();

      $(".main-sidebar .sidebar-menu li a.has-dropdown").off('click').on('click', function() {
        var me     = $(this);
        var active = false;
        if(me.parent().hasClass("active")){
          active = true;
        }
        
        $('.main-sidebar .sidebar-menu li.active > .dropdown-menu').slideUp(500, function() {
          update_sidebar_nicescroll();          
          return false;
        });
        
        $('.main-sidebar .sidebar-menu li.active').removeClass('active');

        if(active==true) {
          me.parent().removeClass('active');          
          me.parent().find('> .dropdown-menu').slideUp(500, function() {            
            update_sidebar_nicescroll();
            return false;
          });
        }else{
          me.parent().addClass('active');          
          me.parent().find('> .dropdown-menu').slideDown(500, function() {            
            update_sidebar_nicescroll();
            return false;
          });
        }

        return false;
      });

      $('.main-sidebar .sidebar-menu li.active > .dropdown-menu').slideDown(500, function() {
        update_sidebar_nicescroll();        
        return false;
      });
    }
  }
  sidebar_dropdown();

  if($("#top-5-scroll").length) {
    $("#top-5-scroll").css({
      height: 315
    }).niceScroll();
  }

  $(".main-content").css({
    minHeight: $(window).outerHeight() - 108
  })

  $(".nav-collapse-toggle").click(function() {
    $(this).parent().find('.navbar-nav').toggleClass('show');
    return false;
  });

  $(document).on('click', function(e) {
    $(".nav-collapse .navbar-nav").removeClass('show');
  });

  var toggle_sidebar_mini = function(mini) {
    let body = $('body');

    if(!mini) {
      body.removeClass('sidebar-mini');
      $(".main-sidebar").css({
        overflow: 'hidden'
      });
      setTimeout(function() {
        $(".main-sidebar").niceScroll(sidebar_nicescroll_opts);
        sidebar_nicescroll = $(".main-sidebar").getNiceScroll();
      }, 500);
      $(".main-sidebar .sidebar-menu > li > ul .dropdown-title").remove();
      $(".main-sidebar .sidebar-menu > li > a").removeAttr('data-toggle');
      $(".main-sidebar .sidebar-menu > li > a").removeAttr('data-original-title');
      $(".main-sidebar .sidebar-menu > li > a").removeAttr('title');
    }else{
      body.addClass('sidebar-mini');
      body.removeClass('sidebar-show');
      sidebar_nicescroll.remove();
      sidebar_nicescroll = null;
      $(".main-sidebar .sidebar-menu > li").each(function() {
        let me = $(this);

        if(me.find('> .dropdown-menu').length) {
          me.find('> .dropdown-menu').hide();
          me.find('> .dropdown-menu').prepend('<li class="dropdown-title pt-3">'+ me.find('> a').text() +'</li>');
        }else{
          me.find('> a').attr('data-toggle', 'tooltip');
          me.find('> a').attr('data-original-title', me.find('> a').text());
          $("[data-toggle='tooltip']").tooltip({
            placement: 'right'
          });
        }
      });
    }
  }

  $("[data-toggle='sidebar']").click(function() {
    var body = $("body"),
      w = $(window);

    if(w.outerWidth() <= 1024) {
      body.removeClass('search-show search-gone');
      if(body.hasClass('sidebar-gone')) {
        body.removeClass('sidebar-gone');
        body.addClass('sidebar-show');
      }else{
        body.addClass('sidebar-gone');
        body.removeClass('sidebar-show');
      }

      update_sidebar_nicescroll();
    }else{
      body.removeClass('search-show search-gone');
      if(body.hasClass('sidebar-mini')) {
        toggle_sidebar_mini(false);
      }else{
        toggle_sidebar_mini(true);
      }
    }

    return false;
  });

  var toggleLayout = function() {
    var w = $(window),
      layout_class = $('body').attr('class') || '',
      layout_classes = (layout_class.trim().length > 0 ? layout_class.split(' ') : '');

    if(layout_classes.length > 0) {
      layout_classes.forEach(function(item) {
        if(item.indexOf('layout-') != -1) {
          now_layout_class = item;
        }
      });
    }

    if(w.outerWidth() <= 1024) {
      if($('body').hasClass('sidebar-mini')) {
        toggle_sidebar_mini(false);
        $('.main-sidebar').niceScroll(sidebar_nicescroll_opts);
        sidebar_nicescroll = $(".main-sidebar").getNiceScroll();
      }

      $("body").addClass("sidebar-gone");
      $("body").removeClass("layout-2 layout-3 sidebar-mini sidebar-show");
      $("body").off('click touchend').on('click touchend', function(e) {
        if($(e.target).hasClass('sidebar-show') || $(e.target).hasClass('search-show')) {
          $("body").removeClass("sidebar-show");
          $("body").addClass("sidebar-gone");
          $("body").removeClass("search-show");

          update_sidebar_nicescroll();
        }
      });

      update_sidebar_nicescroll();

      if(now_layout_class == 'layout-3') {
        let nav_second_classes = $(".navbar-secondary").attr('class'),
          nav_second = $(".navbar-secondary");

        nav_second.attr('data-nav-classes', nav_second_classes);
        nav_second.removeAttr('class');
        nav_second.addClass('main-sidebar');

        let main_sidebar = $(".main-sidebar");
        main_sidebar.find('.container').addClass('sidebar-wrapper').removeClass('container');
        main_sidebar.find('.navbar-nav').addClass('sidebar-menu').removeClass('navbar-nav');
        main_sidebar.find('.sidebar-menu .nav-item.dropdown.show a').click();
        main_sidebar.find('.sidebar-brand').remove();
        main_sidebar.find('.sidebar-menu').before($('<div>', {
          class: 'sidebar-brand'
        }).append(
          $('<a>', {
            href: $('.navbar-brand').attr('href'),
          }).html($('.navbar-brand').html())
        ));
        setTimeout(function() {
          sidebar_nicescroll = main_sidebar.niceScroll(sidebar_nicescroll_opts);
          sidebar_nicescroll = main_sidebar.getNiceScroll();
        }, 700);

        sidebar_dropdown();
        $(".main-wrapper").removeClass("container");
      }
    }else{
      $("body").removeClass("sidebar-gone sidebar-show");
      if(now_layout_class)
        $("body").addClass(now_layout_class);

      let nav_second_classes = $(".main-sidebar").attr('data-nav-classes'),
        nav_second = $(".main-sidebar");

      if(now_layout_class == 'layout-3' && nav_second.hasClass('main-sidebar')) {
        nav_second.find(".sidebar-menu li a.has-dropdown").off('click');
        nav_second.find('.sidebar-brand').remove();
        nav_second.removeAttr('class');
        nav_second.addClass(nav_second_classes);

        let main_sidebar = $(".navbar-secondary");
        main_sidebar.find('.sidebar-wrapper').addClass('container').removeClass('sidebar-wrapper');
        main_sidebar.find('.sidebar-menu').addClass('navbar-nav').removeClass('sidebar-menu');
        main_sidebar.find('.dropdown-menu').hide();
        main_sidebar.removeAttr('style');
        main_sidebar.removeAttr('tabindex');
        main_sidebar.removeAttr('data-nav-classes');
        $(".main-wrapper").addClass("container");
        // if(sidebar_nicescroll != null)
        //   sidebar_nicescroll.remove();
      }else if(now_layout_class == 'layout-2') {
        $("body").addClass("layout-2");
      }else{
        update_sidebar_nicescroll();
      }
    }
  }
  toggleLayout();
  $(window).resize(toggleLayout);

  $("[data-toggle='search']").click(function() {
    var body = $("body");

    if(body.hasClass('search-gone')) {
      body.addClass('search-gone');
      body.removeClass('search-show');
    }else{
      body.removeClass('search-gone');
      body.addClass('search-show');
    }
  });

  // tooltip
  $("[data-toggle='tooltip']").tooltip();

  // popover
  $('[data-toggle="popover"]').popover({
    container: 'body'
  });

  // Select2
  if(jQuery().select2) {
    $(".select2").select2();
  }

  // Selectric
  if(jQuery().selectric) {
    $(".selectric").selectric({
      disableOnMobile: false,
      nativeOnMobile: false
    });
  }

  $(".notification-toggle").dropdown();
  $(".notification-toggle").parent().on('shown.bs.dropdown', function() {
    $(".dropdown-list-icons").niceScroll({
      cursoropacitymin: .3,
      cursoropacitymax: .8,
      cursorwidth: 7
    });
  });

  $(".message-toggle").dropdown();
  $(".message-toggle").parent().on('shown.bs.dropdown', function() {
    $(".dropdown-list-message").niceScroll({
      cursoropacitymin: .3,
      cursoropacitymax: .8,
      cursorwidth: 7
    });
  });

  if($(".chat-content").length) {
    $(".chat-content").niceScroll({
        cursoropacitymin: .3,
        cursoropacitymax: .8,
    });
    $('.chat-content').getNiceScroll(0).doScrollTop($('.chat-content').height());
  }

  if(jQuery().summernote) {
    $(".summernote").summernote({
       dialogsInBody: true,
      minHeight: 250,
    });
    $(".summernote-simple").summernote({
       dialogsInBody: true,
      minHeight: 150,
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough']],
        ['para', ['paragraph']]
      ]
    });
  }

  if(window.CodeMirror) {
    $(".codeeditor").each(function() {
      let editor = CodeMirror.fromTextArea(this, {
        lineNumbers: true,
        theme: "duotone-dark",
        mode: 'javascript',
        height: 200
      });
      editor.setSize("100%", 200);
    });
  }

  // Follow function
  $('.follow-btn, .following-btn').each(function() {
    var me = $(this),
        follow_text = 'Follow',
        unfollow_text = 'Following';

    me.click(function() {
      if(me.hasClass('following-btn')) {
        me.removeClass('btn-danger');
        me.removeClass('following-btn');
        me.addClass('btn-primary');
        me.html(follow_text);

        eval(me.data('unfollow-action'));
      }else{
        me.removeClass('btn-primary');
        me.addClass('btn-danger');
        me.addClass('following-btn');
        me.html(unfollow_text);

        eval(me.data('follow-action'));
      }
      return false;
    });
  });

  // Dismiss function
  $("[data-dismiss]").each(function() {
    var me = $(this),
        target = me.data('dismiss');

    me.click(function() {
      $(target).fadeOut(function() {
        $(target).remove();
      });
      return false;
    });
  });

  // Collapsable
  $("[data-collapse]").each(function() {
    var me = $(this),
        target = me.data('collapse');

    me.click(function() {
      $(target).collapse('toggle');
      $(target).on('shown.bs.collapse', function(e) {
        e.stopPropagation();
        me.html('<i class="fas fa-minus"></i>');
      });
      $(target).on('hidden.bs.collapse', function(e) {
        e.stopPropagation();
        me.html('<i class="fas fa-plus"></i>');
      });
      return false;
    });
  });

  // Gallery
  $(".gallery .gallery-item").each(function() {
    var me = $(this);

    me.attr('href', me.data('image'));
    me.attr('title', me.data('title'));
    if(me.parent().hasClass('gallery-fw')) {
      me.css({
        height: me.parent().data('item-height'),
      });
      me.find('div').css({
        lineHeight: me.parent().data('item-height') + 'px'
      });
    }
    me.css({
      backgroundImage: 'url("'+ me.data('image') +'")'
    });
  });
  if(jQuery().Chocolat) {
    $(".gallery").Chocolat({
      className: 'gallery',
      imageSelector: '.gallery-item',
    });
  }

  // Background
  $("[data-background]").each(function() {
    var me = $(this);
    me.css({
      backgroundImage: 'url(' + me.data('background') + ')'
    });
  });

  // Custom Tab
  $("[data-tab]").each(function() {
    var me = $(this);

    me.click(function() {
      if(!me.hasClass('active')) {
        var tab_group = $('[data-tab-group="' + me.data('tab') + '"]'),
            tab_group_active = $('[data-tab-group="' + me.data('tab') + '"].active'),
            target = $(me.attr('href')),
            links = $('[data-tab="'+me.data('tab') +'"]');

        links.removeClass('active');
        me.addClass('active');
        target.addClass('active');
        tab_group_active.removeClass('active');
      }
      return false;
    });
  });

  // Bootstrap 4 Validation
  $(".needs-validation").submit(function() {
    var form = $(this);
    if (form[0].checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    form.addClass('was-validated');
  });

  // alert dismissible
  $(".alert-dismissible").each(function() {
    var me = $(this);

    me.find('.close').click(function() {
      me.alert('close');
    });
  });

  if($('.main-navbar').length) {
  }

  // Image cropper
  $('[data-crop-image]').each(function(e) {
    $(this).css({
      overflow: 'hidden',
      position: 'relative',
      height: $(this).data('crop-image')
    });
  });

  // Slide Toggle
  $('[data-toggle-slide]').click(function() {
    let target = $(this).data('toggle-slide');

    $(target).slideToggle();
    return false;
  });

  // Dismiss modal
  $("[data-dismiss=modal]").click(function() {
    $(this).closest('.modal').modal('hide');

    return false;
  });

  // Width attribute
  $('[data-width]').each(function() {
    $(this).css({
      width: $(this).data('width')
    });
  });

  // Height attribute
  $('[data-height]').each(function() {
    $(this).css({
      height: $(this).data('height')
    });
  });

  // Chocolat
  if($('.chocolat-parent').length && jQuery().Chocolat) {
    $('.chocolat-parent').Chocolat();
  }

  // Sortable card
  if($('.sortable-card').length && jQuery().sortable) {
    $('.sortable-card').sortable({
      handle: '.card-header',
      opacity: .8,
      tolerance: 'pointer'
    });
  }

  // Daterangepicker
  if(jQuery().daterangepicker) {
    if($(".datepicker").length) {
      $('.datepicker').daterangepicker({
        locale: {format: 'YYYY-MM-DD'},
        singleDatePicker: true,
      });
    }
    if($(".datetimepicker").length) {
      $('.datetimepicker').daterangepicker({
        locale: {format: 'YYYY-MM-DD hh:mm'},
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
      });
    }
    if($(".daterange").length) {
      $('.daterange').daterangepicker({
        locale: {format: 'YYYY-MM-DD'},
        drops: 'down',
        opens: 'right'
      });
    }
  }

  // Timepicker
  if(jQuery().timepicker && $(".timepicker").length) {
    $(".timepicker").timepicker({
      icons: {
        up: 'fas fa-chevron-up',
        down: 'fas fa-chevron-down'
      }
    });
  }
});

const APP_URL = window.location.origin;

$(function () {
    $("select.form-control").not('.js-box').selectpicker({
        style: 'btn btn-light form-control',
        actionsBox: true,
        deselectAllText: 'Nenhum',
        selectAllText: 'Todos'
    });

    $("form").on("change", ".file-upload-field", function () {
        $(this).parent(".file-upload-wrapper")
            .attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
    });

    $('.js-confirm-delete').on('click', function (e) {
        e.preventDefault();

        swal({
            title: 'Você tem certeza?',
            text: $(this).data('title') + " será excluído(a).",
            type: 'error',
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            showCancelButton: true,
            confirmButtonColor: '#3f51b5',
            cancelButtonColor: '#ff4081',
        }).then((result) => {

            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    url: $(this).data('link'),
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (data) {
                        swal('Excluído com sucesso', data.message, 'success');

                        if (data.route) {
                            setTimeout(function () {
                                window.location.href = data.route;
                            }, 500);

                        }
                    },
                    error: function (data) {
                        swal('Falha na solicitação', data.responseJSON.error, 'error');
                    }
                });
            } else if (result.dismiss === 'cancel') {
                swal('Ação cancelada', 'O registro não foi excluido', 'warning');
            }
        })
    });

    $(".js-clear").on("click", function () {
        $('.form')[0].reset();
    });

    $(".js-box").bootstrapDualListbox({
        filterPlaceHolder: "Filtrar",
        infoText: false
    });

    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

    if ($(".js-redactor").length) {
        $('.js-redactor').summernote({
            lang: 'pt-BR',
            height: 300,
            tabsize: 2,
            maximumImageFileSize: 2097152
        });
    }
});

function randomColor() {
    return ("#" + Math.floor(Math.random() * 16777215).toString(16)).padEnd(7, '');
}

function getHexContrast(hex) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }

    return (parseInt(hex, 16) > 0xffffff / 2) ? 'black' : 'white';
}



$(function () {
    $('.js-mask-date').mask('00/00/0000');

    $('.js-mask-cnpj').mask('00.000.000/0000-00', {reverse: true});

    $('.js-mask-cpf').mask('000.000.000-00', {reverse: true});

    $('.js-mask-time').mask('00:00:00');

    $('.js-mask-datetime').mask('00/00/0000 00:00:00');

    $('.js-mask-hour').mask('00:00');

    $('.js-mask-date-cart').mask('00/00');

    $('.js-mask-month').mask('00');

    $('.js-mask-year').mask('00');

    $('.js-mask-monthyear').mask('00/0000');

    $('.js-mask-size').mask('0000,00', {reverse: true});

    $('.js-mask-money').mask('000.000.000.000.000,00', {reverse: true});

    $('.js-mask-percent').mask('##0,00%', {reverse: true});

    $('.js-mask-phone').mask('(00) 00000-0000');

    $('.js-mask-cep').mask('00000-000');

    $('.js-mask-state-registration').mask('00.000.000-0', {reverse: true});

    $('.js-mask-type').on('change', maskByType);

    maskByType();
});

function maskByType() {

    $('select.js-mask-type').each(function (){
        let selected   = $(this).find('option:selected');
        let mask       = selected.data('mask');
        let target     = $(this).data('target');

        $(target).unmask();
        $(target).mask(mask, {reverse: true});
    });
}





$(".cep").on('blur', function()
{
    let value = $(this).val();

    $.get("https://viacep.com.br/ws/"+ value +"/json/", function(data)
    {
        $(".rua").val('');
        $(".bairro").val('');
        $(".cidade").val('');
        $(".uf").val('');

        if (data.sucesso != "0")
        {
            $(".cep").val(value);
            $(".bairro").val(data.bairro);
            $(".complemento").val(data.complemento);
            $(".cidade").val(data.localidade);
            $(".rua").val(data.logradouro);
            $(".uf").val(data.uf);
        }
    }, 'json');
});

$('.numero').change(function() {
    $('.numero').val($(this).val());
});

$('.complemento').change(function() {
    $('.complemento').val($(this).val());
});
function pieChart(el, data) {
    return new Chart(el, {
        type: 'outlabeledPie',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                enabled: false
            },
            plugins: {
                zoomOutPercentage: 55,
                legend: true,
                outlabels: {
                    text: '%v - %p',
                    color: 'white',
                    stretch: 45,
                    font: {
                        resizable: true,
                        minSize: 10,
                        maxSize: 14
                    }
                }
            }
        }
    });
}
$(function () {

    $('.birthdate').on('focusout', function () {
        let date = $(this).val().split(/\/|\-/);
        let birthDate = new Date(date[2], date[1], date[0]);
        let otherDate = new Date();
        let years = (otherDate.getFullYear() - birthDate.getFullYear());

        fetchAgeCategory(years);

        if (otherDate.getMonth() < birthDate.getMonth() ||
            otherDate.getMonth() == birthDate.getMonth() &&
            otherDate.getDate() < birthDate.getDate()) {
            years--;
        }

        let form =  $(this).closest('form');
        form.find('.js-age').val(years);

        showResponsible(years, form);
    });
});

function fetchAgeCategory(years) {
    if($('.age-category') !== undefined){
        let option = $('.age-category>option').filter(function () {
            return ($(this).data('age-begin')) <= years &&
                ($(this).data('age-finish')) >= years;
        });

        $('.age-category').selectpicker('val', option.val());
    }
}

function showResponsible(years, form) {
    let field = $(form).find('#guardian-field');

    if(field !== undefined){
        if(years < 18) {
            field.show(); return;
        }
        field.hide();
    }
}
$('.js-notification-delete').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();

    var target = $(this);

    $.ajax({
        type: "DELETE",
        url: target.data('link'),
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        success: function (data) {
            target.closest('.notification').fadeOut(300, function () {
                console.log('fadding');
                $(this.remove());
            });
        },
        error: function (data) {
            swal('Falha na solicitação', data.responseJSON.error, 'error');
        }
    });
});

$('.js-notification-redirect').on('click', function (e) {
    e.preventDefault();

    var target = $(this);

    $.ajax({
        type: "PUT",
        url: target.data('read-link'),
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {read: 'ok'},
        success: function (data) {
            window.location = target.data('link');
        },
        error: function (data) {
            swal('Falha na solicitação', data.responseJSON.error, 'error');
        }
    });
});
$(function () {
    const bands = document.getElementById('chart-student-bands');
    const categories = document.getElementById('chart-age-category');
    const sex = document.getElementById('chart-student-sex');

    if (bands != null) {
        var bands_dash = pieChart(bands, {});
    }

    if (categories != null) {
        var categories_dash = pieChart(categories, {});
    }

    if (sex != null) {
        var sex_dash = pieChart(sex, {});
    }

    $('#filter-academic').on('click', function () {

        let dates = $('input[name="date_range"]').val();
        let classroom = $('select[name="classroom_id[]"]').val();
        let subsidiaryElement = $('select[name="subsidiary_id"]');
        let subsidiary = subsidiaryElement === undefined ? null : subsidiaryElement.val();

        updateStudentsPerSex(sex_dash, dates, classroom, subsidiary);
        updateStudentsPerAgeCategories(categories_dash, dates, classroom, subsidiary);
        updateStudentsPerBands(bands_dash, dates, classroom, subsidiary);
    });
});

function updateStudentsPerSex(sex_dash, dates, classroom, subsidiary = null) {

    $.ajax({
        url: APP_URL + "/admin/academic/students-per-sex",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function (response) {
            sex_dash.data.datasets.splice(0, 1);

            sex_dash.data.labels = ['Masculino', 'Feminino'];
            sex_dash.data.datasets.push({
                data: response,
                backgroundColor: [
                    '#2d4abd',
                    '#e02fdd',
                ]
            });
            sex_dash.update();
        }
    });
}

function updateStudentsPerAgeCategories(categories_dash, dates, classroom, subsidiary = null) {

    $.ajax({
        url: APP_URL + "/admin/academic/students-per-age-categories",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function (response) {

            let labels = Object.keys(response);
            let data = Object.values(response);
            let colors = data.map(() => randomColor());

            categories_dash.data.labels = labels;
            categories_dash.data.datasets.splice(0, 1);
            categories_dash.data.datasets.push({
                data: data,
                backgroundColor: colors
            });

            categories_dash.options.plugins.outlabels.color =
                colors.map((color) => getHexContrast(color));

            categories_dash.update();
        }
    });
}

function updateStudentsPerBands(bands_dash, dates, classroom, subsidiary = null) {

    $.ajax({
        url: APP_URL + "/admin/academic/students-per-bands",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function (response) {

            let labels = Object.keys(response);
            let data = Object.values(response);

            let students = data.map((item) => item.students);
            let colors = data.map((item) => item.color);

            bands_dash.data.labels = labels;

            bands_dash.data.datasets.splice(0, 1);
            bands_dash.data.datasets.push({
                data: students,
                backgroundColor: colors
            });

            bands_dash.options.plugins.outlabels.color =
                colors.map((color) => getHexContrast(color));

            bands_dash.update();
        }
    });
}
$(function () {
    const financial = document.getElementById('chart-financial');
    const billing = document.getElementById('chart-expected-billing');
    const students = document.getElementById('chart-student-status');

    if(financial != null ){
        var year_dash = new Chart(financial, {
            type: 'line',
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                plugins: { datalabels: false }
            },
            data: {
                labels: lastTwelveMonths(),
                datasets: []
            }
        });
    }

    if(billing != null ){
        var billing_dash = pieChart(billing, {});
    }

    if(students != null ){
        var student_dash = pieChart(students, {});
    }

    $('#income-card').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: APP_URL + "/admin/financial/income",
            type: "GET",
            success: function(response){
                var data = {
                    label: 'Receitas',
                    data: response,
                    backgroundColor: 'rgba(16,192,37,0.2)',
                    borderColor: 'rgb(17,192,34)',
                    borderWidth: 1
                };

                year_dash.data.datasets.push(data);
                year_dash.update();
            }
        });
    });

    $('#outcome-card').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: APP_URL + "/admin/financial/outcome",
            type: "GET",
            success: function(response){
                var data = {
                    label: 'Despesas',
                    data: response,
                    backgroundColor: 'rgba(192,50,60,0.2)',
                    borderColor: 'rgb(192,41,38)',
                    borderWidth: 1
                };

                year_dash.data.datasets.push(data);
                year_dash.update();
            }
        });
    });

    $('#registration-card').on('click', function (e) {

        $.ajax({
            url: APP_URL + "/admin/financial/registrations",
            type: "GET",
            success: function(response){
                var data = {
                    label: 'Matrículas',
                    data: response,
                    backgroundColor: 'rgba(185,192,33,0.2)',
                    borderColor: 'rgb(192,169,39)',
                    borderWidth: 1
                };

                year_dash.data.datasets.push(data);
                year_dash.update();
            }
        });
    });

    $('#active-students-card').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: APP_URL + "/admin/financial/active-students",
            type: "GET",
            success: function(response){
                var data = {
                    label: 'Estudantes',
                    data: response,
                    backgroundColor: 'rgba(49,146,192,0.2)',
                    borderColor: 'rgb(60,130,192)',
                    borderWidth: 1
                };

                year_dash.data.datasets.push(data);
                year_dash.update();
            }
        });
    });

    $('#filter-billing').on('click', function() {

        let dates = $('input[name="date_range"]').val();
        let classroom = $('select[name="classroom_id[]"]').val();
        let subsidiaryElement = $('select[name="subsidiary_id"]');
        let subsidiary = subsidiaryElement === undefined ? null : subsidiaryElement.val();

        updateResume(dates, classroom, subsidiary);
        updateExpectedBilling(billing_dash, dates, classroom, subsidiary);
        updateStudentStatus(student_dash, dates, classroom, subsidiary);

    });
});

function lastTwelveMonths() {
    var curDate = new Date();
    var curMonth = curDate.getMonth();
    var curYear = curDate.getFullYear() - 2000;
    var lastYear = curYear - 1;
    var lastMonts = [];

    var months = [
        'Jan',
        'Fev',
        'Mar',
        'Abr',
        'Mai',
        'Jun',
        'Jul',
        'Ago',
        'Set',
        'Out',
        'Nov',
        'Dez'
    ];

    for (var i = curMonth; i > curMonth - 12; i--) {
        lastMonts.push(
            i >= 0 ?
                months[i] + `/${curYear}` :
                months[months.length + i] + `/${lastYear}`
        )
    }

    return lastMonts;
}

function updateResume(dates, classroom, subsidiary = null){

    $.ajax({
        url: APP_URL + "/admin/financial/billing-summary",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function(response){
            $('#expected-incoming').html("R$ " + response.expected_incoming.toFixed(2));
            $('#real-incoming').html("R$ " + response.real_incoming.toFixed(2));
            $('#conversion').html(response.conversion.toFixed(2) + " %");
            $('#medium-ticket').html("R$ " + response.medium_ticket.toFixed(2));
            $('#paid-charges').html(response.paid_charges);
            $('#total-charges').html(response.total_charges);
        }
    });
}

function updateExpectedBilling(pie_dash, dates, classroom, subsidiary = null){

    $.ajax({
        url: APP_URL + "/admin/financial/billing-per-payment-type",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function(response){
            pie_dash.data.datasets.splice(0, 1);

            pie_dash.data.labels = [
                'Boleto',
                'Dinheiro',
                'Cartão de crédito'
            ];
            pie_dash.data.datasets.push({
                data: response,
                backgroundColor: [
                    '#e3ad0b',
                    '#28a745',
                    '#dc3545',
                ]
            });
            pie_dash.update();
        }
    });
}

function updateStudentStatus(student_dash, dates, classroom, subsidiary = null){

    $.ajax({
        url: APP_URL + "/admin/financial/students-by-status",
        type: "GET",
        data: {
            date_range: dates,
            classroom_id: classroom,
            subsidiary_id: subsidiary
        },
        success: function(response){
            student_dash.data.datasets.splice(0, 1);

            student_dash.data.labels = [
                'Bolsistas',
                'Regular',
                'Pendentes',
                'Cancelados'
            ];
            student_dash.data.datasets.push({
                data: response,
                backgroundColor: [
                    '#17a2b8',
                    '#28a745',
                    '#e3ad0b',
                    '#dc3545',
                ]
            });
            student_dash.update();
        }
    });
}







var lines = $('.degrees-fields:not(:hidden)');

lines.each(function (index, line) {
    if(index < lines.length -1){
        $(line).find('.action-buttons').hide();
    }

    if(index === $('.degrees-fields').length -1){
        $(line).find('.add-degree').first().parent().hide();
    }
});

$('.add-degree').click(function() {

    var line = $(this).closest('.degrees-fields');
    var idx = parseInt(line.data('index'));
    var lines = $('.degrees-fields');
    var nextLine = $(lines[idx + 1]);

    line.find('.action-buttons').hide();

    if(lines.length === (parseInt(nextLine.data('index')) + 1)){
        nextLine.find('.add-degree').first().parent().hide();
    }

    nextLine.find('.degree-name').prop('disabled', false);
    nextLine.find('.degree-rule').prop('disabled', false);

    nextLine.show();
});

$('.remove-degree').click(function(){

    var line = $(this).closest('.degrees-fields');
    var idx = parseInt(line.data('index'));
    var lines = $('.degrees-fields');

    line.hide();
    $(lines[idx - 1]).find('.action-buttons').show();

    if(line.find('.degree-id').first().val() > 0){
        line.find('.degree-destroy').first().val('1');
    } else {
        line.find('.degree-name').prop('disabled', true);
        line.find('.degree-rule').prop('disabled', true);
    }

});





window.flatpickr.l10ns.pt.rangeSeparator = " - ";

$(".js-datepicker").flatpickr({
    allowInput: true,
    enableTime: false,
    dateFormat: 'Y-m-d',
    altInput: true,
    altFormat: 'd/m/Y',
    disableMobile: "true",
    locale: 'pt',
    onClose(dates, currentdatestring, picker){
        picker.setDate(picker.altInput.value, true, picker.config.altFormat)
    }
});

$(".js-datetimepicker").flatpickr({
    allowInput: true,
    enableTime: true,
    time_24hr: false,
    dateFormat: 'Y-m-d H:i:S',
    altInput: true,
    altFormat: 'd/m/Y H:i',
    disableMobile: "true",
    locale: 'pt'
});

$(".js-daterangepicker").flatpickr({
    dateFormat: 'Y-m-d',
    altInput: true,
    altFormat: 'd/m/Y',
    disableMobile: "true",
    mode: 'range',
    locale: 'pt'
});

$(".js-monthpicker").flatpickr({
    disableMobile: "true",
    locale: 'pt',
    altInput: true,
    plugins: [
        new monthSelectPlugin({
            shorthand: true,
            dateFormat: "Y-m",
            altFormat: "m/Y",
            theme: "light"
        })
    ]
});